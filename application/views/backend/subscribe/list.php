<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <div class="row">
      <div class="col-xl-4 col-12">
        <h5><?php echo $header_page; ?></h5>
      </div>
      <div class="col-xl-4 col-12"><?php echo $this->session->flashdata('message'); ?></div>
      <div class="col-xl-4 col-12"></div>
  </div>
  <div class="row">
    <div class="col-xl-12 col-12">
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="dataTableSubscribe" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Email</th>
              <th>Session</th>
              <th>User Agent</th>
              <th>Platform</th>
              <th>Ngày đăng ký</th>
              <th>Cập nhật</th>
              <th>Thao tác</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid-->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="" method="POST">
    <div class="modal-content">
      <div class="modal-body">
        Bạn có chắc chắn xóa?
        <input type="hidden" class="form-control" id="id_sub" name="id_sub" value="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        <button type="submit" name="btnDelete" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
      </div>
    </div>
    </form>
  </div>
</div>
<script type="text/javascript">
  function get_id(id)
  {
    $('#id_sub').val(id);
  }
</script>