<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <div class="row">
      <div class="col-xl-4 col-12">
        <h5><?php echo $header_page; ?></h5>
      </div>
      <div class="col-xl-4 col-12"><?php echo $this->session->flashdata('message'); ?></div>
      <div class="col-xl-4 col-12">
        <div class="button-crud text-right">
          <a href="<?php echo base_url('admin/contact/subscribe'); ?>" class="btn btn-secondary"><i class="fa fa-undo" aria-hidden="true"></i> Quay lại</a>
        </div>
      </div>
  </div>
  <div class="row">
    <div class="col-xl-12 col-12">
      <form action="" method="POST">
        <div class="card">
          <div class="card-header">
            Chỉnh sửa email
            <div class="pull-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="email_sub">Email</label>
              <input type="text" class="form-control"  name="email_sub" id="email_sub" autofocus maxlength="50" required="required" value="<?php echo $detail_subscribe['Email_Sub']; ?>">
            </div>
            <div class="form-group text-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- /.container-fluid-->