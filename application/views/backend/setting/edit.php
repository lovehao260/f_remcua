<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <form action="" method="POST">
    <div class="row">
        <div class="col-xl-3 col-12">
          <h5><?php echo $header_page; ?></h5>
        </div>
        <div class="col-xl-6 col-12"><?php echo $this->session->flashdata('message'); ?></div>
        <div class="col-xl-3 col-12"></div>
    </div>
    <div class="row">
      <div class="col-xl-3 col-12"></div>
      <div class="col-xl-6 col-12">
        <div class="form-group">
          <div class="button-crud text-right">
            <button type="submit" class="btn btn-success" name="btnSave" id="btnSave"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
          </div>
        </div>
        <div class="form-group">
          <label class="label" for="title_setting">Tên website</label>
          <input type="text" class="form-control" maxlength="500" name="title_setting" id="title_setting" required="required" value="<?php echo $this->setting[0]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="sloganleft_setting">Slogan trái</label>
          <input type="text" class="form-control" maxlength="500" name="sloganleft_setting" id="sloganleft_setting" value="<?php echo $this->setting[8]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="sloganright_setting">Slogan phải</label>
          <input type="text" class="form-control" maxlength="500" name="sloganright_setting" id="sloganright_setting" value="<?php echo $this->setting[18]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="keywords_setting">Từ khóa</label>
          <input type="text" class="form-control" maxlength="500" name="keywords_setting" id="keywords_setting" value="<?php echo $this->setting[1]; ?>">
        </div>
        <div class="form-group">
          <label class="" for="description_setting">Mô tả</label>
          <textarea class="form-control" name="description_setting" id="description_setting" maxlength="500" rows="2"><?php echo $this->setting[2]; ?></textarea>
        </div>
        <div class="form-group">
          <label class="label" for="author_setting">Tác giả</label>
          <input type="text" class="form-control" maxlength="500" name="author_setting" id="author_setting" value="<?php echo $this->setting[3]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="phone_setting">Điện thoại</label>
          <input type="text" class="form-control" maxlength="15" name="phone_setting" id="phone_setting" value="<?php echo $this->setting[4]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="email_setting">Email</label>
          <input type="text" class="form-control" maxlength="150" name="email_setting" id="email_setting" value="<?php echo $this->setting[5]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="address_setting">Địa chỉ</label>
          <input type="text" class="form-control" maxlength="250" name="address_setting" id="address_setting" value="<?php echo $this->setting[6]; ?>">
        </div>
        <script>
          function BrowseServer() {
            var finder = new CKFinder();
            finder.selectActionFunction = SetFileField;
            finder.popup();
          }
          function SetFileField(fileUrl) {
            var inputTag = '<input type="hidden" id="inputSThumbnail" name="logo_setting" value="">';
            $('#singleThumbnail').html('<div class="col-12"><img src="" class="img-fluid" id="Image">'+inputTag+'</div><div class="col-12"><a href="javascript:void(0)" onclick="removeImg();" id="RemoveImg">Xóa hình ảnh</a></div>');
            $('#Image').attr('src',fileUrl);
            $('#inputSThumbnail').val(fileUrl);
          }
          function removeImg()
          {
            $('#Image').attr('src','');
            $('#inputSThumbnail').val('');
            $('#RemoveImg').remove();
          }
        </script>
        <div class="card">
          <div class="card-header">
            <span class="btn">
              <i class="fa fa-picture-o"></i> Logo
            </span>
            <span class="btn btn-primary fileinput-button pull-right" onclick="BrowseServer();">
                <i class="fa fa-plus"></i>
                <span>Thêm hình ảnh...</span>
            </span>
          </div>
          <div class="card-body">
            <div id="singleThumbnail">
              <?php if($this->setting[7] != null){ ?>
              <div class="col-12">
                <img src="<?php echo base_url($this->setting[7]); ?>" class="img-fluid" id="Image" name="Image">
                <input type="hidden" id="inputSThumbnail" name="logo_setting" value="<?php echo $this->setting[7]; ?>">
              </div>
              <div class="col-12">
                <a href="javascript:void(0)" onclick="removeImg();" id="RemoveImg">Xóa hình ảnh</a>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
        <!-- /// -->
        <script>
          function BrowseServerIcon() {
            var finder = new CKFinder();
            finder.selectActionFunction = SetFileFieldIcon;
            finder.popup();
          }
          function SetFileFieldIcon(fileUrl) {
            var inputTag = '<input type="hidden" id="inputSThumbnailIcon" name="iconlogo_setting" value="">';
            $('#singleThumbnailIcon').html('<div class="col-12"><img src="" class="img-fluid" id="ImageIcon">'+inputTag+'</div><div class="col-12"><a href="javascript:void(0)" onclick="removeImgIcon();" id="RemoveImgIcon">Xóa hình ảnh</a></div>');
            $('#ImageIcon').attr('src',fileUrl);
            $('#inputSThumbnailIcon').val(fileUrl);
          }
          function removeImgIcon()
          {
            $('#ImageIcon').attr('src','');
            $('#inputSThumbnailIcon').val('');
            $('#removeImgIcon').remove();
          }
        </script>
        <div class="card">
          <div class="card-header">
            <span class="btn">
              <i class="fa fa-picture-o"></i> Icon
            </span>
            <span class="btn btn-primary fileinput-button pull-right" onclick="BrowseServerIcon();">
                <i class="fa fa-plus"></i>
                <span>Thêm hình ảnh...</span>
            </span>
          </div>
          <div class="card-body">
            <div id="singleThumbnailIcon">
              <?php if($this->setting[24] != null){ ?>
              <div class="col-12">
                <img src="<?php echo base_url($this->setting[24]); ?>" class="img-fluid" id="ImageIcon" name="ImageIcon">
                <input type="hidden" id="inputSThumbnailIcon" name="iconlogo_setting" value="<?php echo $this->setting[24]; ?>">
              </div>
              <div class="col-12">
                <a href="javascript:void(0)" onclick="removeImgIcon();" id="removeImgIcon">Xóa hình ảnh</a>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
        <!-- /// -->
        <div class="form-group">
          <label class="label" for="facebook_setting">Facebook</label>
          <input type="text" class="form-control" maxlength="250" name="facebook_setting" id="facebook_setting" value="<?php echo $this->setting[9]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="youtube_setting">Youtube</label>
          <input type="text" class="form-control" maxlength="250" name="youtube_setting" id="youtube_setting" value="<?php echo $this->setting[10]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="googleplus_setting">GooglePlus</label>
          <input type="text" class="form-control" maxlength="250" name="googleplus_setting" id="googleplus_setting" value="<?php echo $this->setting[11]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="twitter_setting">Twitter</label>
          <input type="text" class="form-control" maxlength="250" name="twitter_setting" id="twitter_setting" value="<?php echo $this->setting[12]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="unitprice_setting">Đơn vị tiền tệ</label>
          <input type="text" class="form-control" maxlength="150" name="unitprice_setting" id="unitprice_setting" value="<?php echo $this->setting[13]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="float_setting">Đơn vị tính</label>
          <input type="text" class="form-control" maxlength="150" name="float_setting" id="float_setting" value="<?php echo $this->setting[14]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="percent_setting">Phần trăm giá mét vuông (Ví dụ: 0.1 = 10%, nhập 0.1)</label>
          <input type="text" class="form-control" maxlength="150" name="percent_setting" id="percent_setting" value="<?php echo $this->setting[19]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="googleana_setting">Google Analytics</label>
          <input type="text" class="form-control" maxlength="150" name="googleana_setting" id="googleana_setting" value="<?php echo $this->setting[16]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="facebookid_setting">App Facebook ID</label>
          <input type="text" class="form-control" maxlength="150" name="facebookid_setting" id="facebookid_setting" value="<?php echo $this->setting[17]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="page_facebookid_setting">Page Facebook ID</label>
          <input type="text" class="form-control" maxlength="150" name="page_facebookid_setting" id="page_facebookid_setting" value="<?php echo $this->setting[23]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="idtawkto_setting">ID Tawk.to</label>
          <input type="text" class="form-control" maxlength="150" name="idtawkto_setting" id="idtawkto_setting" value="<?php echo $this->setting[22]; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="descriptionsub_setting">Mô tả Subscribe</label>
          <textarea class="form-control" name="descriptionsub_setting" id="descriptionsub_setting" maxlength="500" rows="2"><?php echo $this->setting[15]; ?></textarea>
        </div>
        <div class="form-group">
          <label class="label" for="map_setting">Google Map</label>
          <textarea class="form-control" name="map_setting" id="map_setting" maxlength="500" rows="2"><?php echo $this->setting[20]; ?></textarea>
        </div>
        <div class="form-group">
          <label class="label" for="texthome_setting">Text Home</label>
          <textarea class="form-control" name="texthome_setting" id="texthome_setting" maxlength="500" rows="2"><?php echo $this->setting[21]; ?></textarea>
        </div>
        <div class="form-group">
          <div class="button-crud text-right">
            <button type="submit" class="btn btn-success" name="btnSave" id="btnSave"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
          </div>
        </div>
      </div>
      <!-- /END COL -->
      <div class="col-xl-3 col-12"></div>
      <!-- /END COL -->
    </div>
    <!-- /END ROW -->
  </form>
</div>
<!-- /.container-fluid-->