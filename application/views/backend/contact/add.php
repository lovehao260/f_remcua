<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <div class="row">
      <div class="col-xl-4 col-12">
        <h5><?php echo $header_page; ?></h5>
      </div>
      <div class="col-xl-8 col-12"><?php echo $this->session->flashdata('message'); ?></div>
  </div>
  <div class="row">
    <div class="col-xl-12 col-12">
      <form action="" method="POST">
        <div class="card">
          <div class="card-header">
            <?php echo $title_card; ?>
            <div class="pull-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="name_contact">Họ và tên</label>
              <input type="text" class="form-control" name="name_contact" id="name_contact" autofocus maxlength="50" required="required">
            </div>
            <div class="form-group">
              <label for="phone_contact">Điện thoại</label>
              <input type="text" class="form-control" name="phone_contact" id="phone_contact" maxlength="50" required="required">
            </div>
            <div class="form-group">
              <label for="email_contact">Email</label>
              <input type="text" class="form-control" name="email_contact" id="email_contact" maxlength="50">
            </div>
            <div class="form-group">
              <label for="content_contact">Nội dung</label>
              <textarea class="form-control" name="content_contact" id="content_contact" required="required"></textarea>
            </div>
            <div class="form-group">
              <label for="session_contact">Session</label>
              <input type="text" class="form-control" name="session_contact" id="session_contact" maxlength="50" required="required" value="<?php echo session_id(); ?>">
            </div>
            <div class="form-group">
              <label for="useragent_contact">User Agent</label>
              <input type="text" class="form-control" name="useragent_contact" id="useragent_contact" maxlength="50" required="required" value="<?php echo $this->agent->browser().' '.$this->agent->version(); ?>">
            </div>
            <div class="form-group">
              <label for="platform_contact">Platform</label>
              <input type="text" class="form-control" name="platform_contact" id="platform_contact" maxlength="50" required="required" value="<?php echo $this->agent->platform(); ?>">
            </div>
            <div class="form-group text-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- /.container-fluid-->
