<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <div class="row">
      <div class="col-xl-4 col-12">
        <h5><?php echo $header_page; ?></h5>
      </div>
      <div class="col-xl-4 col-12"><?php echo $this->session->flashdata('message'); ?></div>
      <div class="col-xl-4 col-12">
        <div class="button-crud text-right">
          <a href="<?php echo base_url('admin/contact/add'); ?>" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Thêm liên hệ</a>
        </div>
      </div>
  </div>
  <div class="row">
    <div class="col-xl-12 col-12">
      <form action="" method="POST">
        <div class="card">
          <div class="card-header">
            <?php echo $detail_contact['Name_Contact']; ?>
            <div class="pull-right">
              <a href="<?php echo base_url('admin/contact/edit/'.$detail_contact['ID_Contact']); ?>" class="btn btn-info"><i class="fa fa-edit" aria-hidden="true"></i> Chỉnh sửa</a>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="name_contact">Họ và tên</label>
              <input type="text" class="form-control" disabled="disabled" name="name_contact" id="name_contact" autofocus maxlength="50" required="required" value="<?php echo $detail_contact['Name_Contact']; ?>">
            </div>
            <div class="form-group">
              <label for="phone_contact">Điện thoại</label>
              <input type="text" class="form-control" name="phone_contact" id="phone_contact" maxlength="50" required="required" disabled="disabled" value="<?php echo $detail_contact['Phone_Contact']; ?>">
            </div>
            <div class="form-group">
              <label for="email_contact">Email</label>
              <input type="text" class="form-control" name="email_contact" id="email_contact" maxlength="50" disabled="disabled" value="<?php echo $detail_contact['Email_Contact']; ?>">
            </div>
            <div class="form-group">
              <label for="content_contact">Nội dung</label>
              <textarea class="form-control" name="content_contact" id="content_contact" required="required" disabled="disabled"><?php echo $detail_contact['Messages_Contact']; ?></textarea>
            </div>
            <div class="form-group">
              <label for="session_contact">Session</label>
              <input type="text" class="form-control" name="session_contact" id="session_contact" maxlength="50" required="required" disabled="disabled" value="<?php echo $detail_contact['Session_Contact']; ?>">
            </div>
            <div class="form-group">
              <label for="useragent_contact">User Agent</label>
              <input type="text" class="form-control" name="useragent_contact" id="useragent_contact" maxlength="50" required="required" disabled="disabled" value="<?php echo $detail_contact['User_Agent_Contact']; ?>">
            </div>
            <div class="form-group">
              <label for="platform_contact">Platform</label>
              <input type="text" class="form-control" name="platform_contact" id="platform_contact" maxlength="50" required="required" disabled="disabled" value="<?php echo $detail_contact['Platform_Contact']; ?>">
            </div>
            <div class="form-group text-right">
              <a href="<?php echo base_url('admin/contact/edit/'.$detail_contact['ID_Contact']); ?>" class="btn btn-info"><i class="fa fa-edit" aria-hidden="true"></i> Chỉnh sửa</a>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- /.container-fluid-->