<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <div class="row">
      <div class="col-xl-4 col-12">
        <h5><?php echo $header_page; ?></h5>
      </div>
      <div class="col-xl-8 col-12"><?php echo $this->session->flashdata('message'); ?></div>
  </div>
  <div class="row">
    <div class="col-xl-2 col-12"></div>
    <div class="col-xl-8 col-12">
      <form action="" method="POST">
        <div class="card">
          <div class="card-header">
            Chỉnh sửa thành viên
            <div class="pull-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="name_user">Họ và tên</label>
              <input type="text" class="form-control" name="name_user" id="name_user" autofocus maxlength="50" required="required" value="<?php echo $detail_user['Name_U']; ?>">
            </div>
            <div class="form-group">
              <label for="username_user">Tên đăng nhập</label>
              <input type="text" class="form-control" disabled="disabled" name="username_user" id="username_user" maxlength="50" required="required" value="<?php echo $detail_user['Username_U']; ?>">
            </div>
            <div class="form-group">
              <label for="password_user">Mật khẩu</label>
              <input type="password" class="form-control" name="password_user" id="password_user" maxlength="50">
            </div>
            <div class="form-group">
              <label for="phone_user">Điện thoại</label>
              <input type="text" class="form-control" name="phone_user" id="phone_user" maxlength="50" value="<?php echo $detail_user['Phone_U']; ?>">
            </div>
            <div class="form-group">
              <label for="email_user">Email</label>
              <input type="text" class="form-control" name="email_user" id="email_user" maxlength="50" value="<?php echo $detail_user['Email_U']; ?>">
            </div>
            
            <div class="form-group">
              <label for="public_user">Hiển thị</label>
              <select class="form-control" name="public_user" id="public_user" required="required">
                <option value="Y" <?php if($detail_user['Active_U'] == 'Y'){echo "selected='selected'";} ?>>Hiển thị</option>
                <option value="N" <?php if($detail_user['Active_U'] == 'N'){echo "selected='selected'";} ?>>Ẩn</option>
              </select>
            </div>
            <div class="form-group text-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="col-xl-2 col-12">
</div>
<!-- /.container-fluid-->