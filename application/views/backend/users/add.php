<div class="col-xl-4 col-12">
  <form action="" method="POST">
    <div class="card">
      <div class="card-header">
        Thêm thành viên mới
        <div class="pull-right">
          <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
        </div>
      </div>
      <div class="card-body">
        <div class="form-group">
          <label for="name_user">Họ và tên</label>
          <input type="text" class="form-control" name="name_user" id="name_user" autofocus maxlength="50" required="required" value="<?php echo set_value('name_user'); ?>">
        </div>
        <div class="form-group">
          <label for="username_user">Tên đăng nhập</label>
          <input type="text" class="form-control" name="username_user" id="username_user" maxlength="50" required="required" value="<?php echo set_value('username_user'); ?>">
        </div>
        <div class="form-group">
          <label for="password_user">Mật khẩu</label>
          <input type="password" class="form-control" name="password_user" id="password_user" maxlength="50" required="required">
        </div>
        <div class="form-group">
          <label for="phone_user">Điện thoại</label>
          <input type="text" class="form-control" name="phone_user" id="phone_user" maxlength="50" value="<?php echo set_value('phone_user'); ?>">
        </div>
        <div class="form-group">
          <label for="email_user">Email</label>
          <input type="text" class="form-control" name="email_user" id="email_user" maxlength="50" value="<?php echo set_value('email_user'); ?>">
        </div>
        
        <div class="form-group">
          <label for="public_user">Hiển thị</label>
          <select class="form-control" name="public_user" id="public_user" required="required">
            <option value="Y">Hiển thị</option>
            <option value="N">Ẩn</option>
          </select>
        </div>
        <div class="form-group text-right">
          <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
        </div>
      </div>
    </div>
  </form>
</div>