<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <div class="row">
      <div class="col-xl-4 col-12">
        <h5><?php echo $header_page; ?></h5>
      </div>
      <div class="col-xl-8 col-12"><?php echo $this->session->flashdata('message'); ?></div>
  </div>
  <div class="row">
    <?php $this->load->view('backend/users/add'); ?>
    <div class="col-xl-8 col-12">
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Họ và tên</th>
              <th>Tên đăng nhập</th>
              <th>Điện thoại</th>
              <th>Email</th>
              <th>Kích hoạt</th>
              <th>Đăng nhập lần cuối</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($get_user as $item){ ?>
            <tr>
              <td><a href="<?php echo base_url('/admin/users/edit/'.$item['ID_U']); ?>"><?php echo $item['Name_U']; ?></a></td>
              <td><a href="<?php echo base_url('/admin/users/edit/'.$item['ID_U']); ?>"><?php echo $item['Username_U']; ?></a></td>
              <td><?php echo $item['Phone_U']; ?></td>
              <td><?php echo $item['Email_U']; ?></td>
              <td><?php echo public_item($item['Active_U']); ?></td>
              <td><?php echo $item['Lastlogin_U']; ?></td>
              <td>
                <a href="<?php echo base_url('/admin/users/edit/'.$item['ID_U']); ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
              <?php if($item['ID_U'] != '1'){ ?>
                <a href="" class="btn btn-danger btn-sm" onclick="get_id(<?php echo $item['ID_U']; ?>)" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
              <?php } ?>
              </td>
            </tr>
          <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid-->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="" method="POST">
    <div class="modal-content">
      <div class="modal-body">
        Bạn có chắc chắn xóa?
        <input type="hidden" class="form-control" id="id_user" name="id_user" value="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        <button type="submit" name="btnDelete" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
      </div>
    </div>
    </form>
  </div>
</div>
<script type="text/javascript">
  function get_id(id)
  {
    $('#id_user').val(id);
  }
</script>