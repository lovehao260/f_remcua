    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <?php echo $breadcrumb; ?>
      <!-- Icon Cards-->
      <div class="row">
        <div class="col-12">
          <div class="alert alert-primary" role="alert">
            Welcome, <strong><?php echo $user_login['Name_U']; ?></strong>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-12 col-md-12 col-12">
          <div class="card">
            <div class="card-header">
              Liên hệ mới
            </div>
            <div class="card-body">
              <table class="table">
                <thead>
                  <tr>
                    <th>Họ và tên</th>
                    <th>Điện thoại</th>
                    <th>Email</th>
                    <th>Ngày gửi</th>
                  </tr>
                </thead>
                <tbody>
                <?php if(!empty($contact)){ foreach($contact as $item){ if($item['Read_Contact'] == 'N'){ ?>
                  <tr>
                    <td><strong class="text-primary"><a href=""><?php echo $item['Name_Contact']; ?></a></strong></td>
                    <td><strong class="text-primary"><?php echo $item['Phone_Contact']; ?></strong></td>
                    <td><strong class="text-primary"><?php echo $item['Email_Contact']; ?></strong></td>
                    <td><strong class="text-primary"><?php echo $item['Createday_Contact']; ?></strong></td>
                  </tr>
                <?php }}}else{ ?>
                  <tr>
                    <td colspan="4"><p class="text-center">Không có dữ liệu trong bảng</p></td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            </div>
            <div class="card-footer text-muted">
              <a href="<?php echo base_url('admin/contact'); ?>">Xem thêm</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->