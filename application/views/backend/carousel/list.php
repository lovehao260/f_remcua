<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <div class="row">
      <div class="col-4">
        <h5><?php echo $header_page; ?></h5>
      </div>
      <div class="col-4"><?php echo $this->session->flashdata('message'); ?></div>
      <div class="col-4">
        <div class="button-crud text-right">
          <a href="<?php echo base_url('admin/carousel/add'); ?>" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Tạo trình chiếu</a>
        </div>
      </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="dataTableCarousel" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Hình ảnh</th>
              <th>Tên slide</th>
              <th width="100">Thứ tự</th>
              <th width="100">Hiển thị</th>
              <th width="150">Thao tác</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($get_carousel as $item){ ?>
            <tr>
              <td><a href="<?php echo base_url('admin/carousel/edit/'.$item['ID_Slide']); ?>"><img src="<?php echo base_url($item['Image_Slide']); ?>" class="img-fluid" width="200" height="100"></a></td>
              <td><a href="<?php echo base_url('admin/carousel/edit/'.$item['ID_Slide']); ?>"><?php echo $item['Name_Slide']; ?></a></td>
              <td><?php echo $item['Sort_Slide']; ?></td>
              <td><?php echo public_item($item['Public_Slide']); ?></td>
              <td>
                <a href="<?php echo base_url('admin/carousel/edit/'.$item['ID_Slide']); ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                <a href="" class="btn btn-danger btn-sm" onclick="get_id(<?php echo $item['ID_Slide']; ?>)" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
              </td>
            </tr>
          <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid-->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="" method="POST">
    <div class="modal-content">
      <div class="modal-body">
        Bạn có chắc chắn xóa?
        <input type="hidden" class="form-control" id="id_slide" name="id_slide" value="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        <button type="submit" name="btnDelete" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
      </div>
    </div>
    </form>
  </div>
</div>
<script type="text/javascript">
  function get_id(id)
  {
    $('#id_slide').val(id);
  }
</script>