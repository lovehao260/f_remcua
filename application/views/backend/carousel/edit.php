<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <form action="" method="POST">
    <div class="row">
        <div class="col-xl-4 col-12">
          <h5><?php echo $header_page; ?></h5>
        </div>
        <div class="col-xl-4 col-12"><?php echo $this->session->flashdata('message'); ?></div>
        <div class="col-xl-4 col-12">
          <div class="button-crud text-right">
            <button type="submit" class="btn btn-success" name="btnSave" id="btnSave"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
          </div>
        </div>
    </div>
    <div class="row">
      <div class="col-xl-9 col-12">
        <div class="form-group">
          <label class="label" for="name_slide">Tên trình chiếu</label>
          <input type="text" class="form-control" maxlength="50" name="name_slide" id="name_slide" autofocus required="required" value="<?php echo $detail_slide['Name_Slide']; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="caption_slide">Chú thích trình chiếu</label>
          <input type="text" class="form-control" maxlength="150" name="caption_slide" id="caption_slide" value="<?php echo $detail_slide['Caption_Slide']; ?>">
        </div>
        <div class="form-group">
          <label class="" for="description_slide">Nội dung trình chiếu</label>
          <textarea class="form-control" name="description_slide" id="description_slide" rows="2"><?php echo $detail_slide['Description_Slide']; ?></textarea>
        </div>
        <script>
          function BrowseServer() {
            var finder = new CKFinder();
            finder.selectActionFunction = SetFileField;
            finder.popup();
          }
          function SetFileField(fileUrl) {
            var inputTag = '<input type="hidden" id="inputSThumbnail" name="image_slide" value="">';
            $('#singleThumbnail').html('<div class="col-12"><img src="" class="img-fluid" id="Image">'+inputTag+'</div><div class="col-12"><a href="javascript:void(0)" onclick="removeImg();" id="RemoveImg">Xóa hình ảnh</a></div>');
            $('#Image').attr('src',fileUrl);
            $('#inputSThumbnail').val(fileUrl);
          }
          function removeImg()
          {
            $('#Image').attr('src','');
            $('#inputSThumbnail').val('');
            $('#RemoveImg').remove();
          }
        </script>
        <div class="card">
          <div class="card-header">
            <span class="btn">
              <i class="fa fa-picture-o"></i> Thêm hình ảnh
            </span>
            <span class="btn btn-primary fileinput-button pull-right" onclick="BrowseServer();">
                <i class="fa fa-plus"></i>
                <span>Thêm hình ảnh...</span>
            </span>
          </div>
          <div class="card-body">
            <div id="singleThumbnail">
              <?php if($detail_slide['Image_Slide'] != null){ ?>
              <div class="col-12">
                <img src="<?php echo base_url($detail_slide['Image_Slide']); ?>" class="img-fluid" id="Image" name="Image">
                <input type="hidden" id="inputSThumbnail" name="image_slide" value="<?php echo $detail_slide['Image_Slide']; ?>">
              </div>
              <div class="col-12">
                <a href="javascript:void(0)" onclick="removeImg();" id="RemoveImg">Xóa hình ảnh</a>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>   
      </div>
      <!-- /END COL -->
      <div class="col-xl-3 col-12">
        <div class="form-group">
          <label class="label" for="public_slide">Hiển thị</label>
          <select class="form-control" id="public_slide" name="public_slide">
            <option value="Y" <?php if($detail_slide['Public_Slide'] == 'Y'){echo "selected='selected'";} ?>>Hiển thị</option>
            <option value="N" <?php if($detail_slide['Public_Slide'] == 'N'){echo "selected='selected'";} ?>>Ẩn</option>
          </select>
        </div>
        <div class="form-group">
          <label class="label" for="sort_slide">Thứ tự <em>(Số thứ tự nhỏ nhất sẽ được hiển thị đầu tiên)</em></label>
          <input type="number" class="form-control" name="sort_slide" id="sort_slide" value="<?php echo $detail_slide['Sort_Slide']; ?>">
        </div>
      </div>
      <!-- /END COL -->
    </div>
    <!-- /END ROW -->
    <div class="row">
        <div class="col-8"></div>
        <div class="col-4">
          <div class="button-crud text-right">
            <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
          </div>
        </div>
    </div>
  </form>
</div>
<!-- /.container-fluid-->