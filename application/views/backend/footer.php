</div>
<footer class="sticky-footer">
  <div class="container">
    <div class="text-center">
      <small>Copyright © <?php echo $this->setting[0]; ?> 2018</small>
    </div>
  </div>
</footer>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fa fa-angle-up"></i>
</a>
<!-- Logout Modal-->
<div class="modal fade" id="modalLogout" tabindex="-1" role="dialog" aria-labelledby="modalLogoutLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">Bạn có chắc chắn đăng xuất?</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Hủy</button>
        <a class="btn btn-primary" href="<?php echo base_url('admin/login/out'); ?>">Đăng xuất</a>
      </div>
    </div>
  </div>
</div>
<!-- Bootstrap core JavaScript-->
<script type="text/javascript" src="/assets/admin/vendor/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/assets/admin/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="/assets/admin/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/assets/admin/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="/assets/admin/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/assets/admin/ckfinder/ckfinder.js"></script>

<script type="text/javascript" src="/assets/admin/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<!-- Core plugin JavaScript-->
<script type="text/javascript" src="/assets/admin/vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Page level plugin JavaScript-->
<script type="text/javascript" src="/assets/admin/vendor/chart.js/Chart.min.js"></script>
<script type="text/javascript" src="/assets/admin/vendor/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="/assets/admin/vendor/datatables/dataTables.bootstrap4.js"></script>
<!-- Custom scripts for this page-->
<script type="text/javascript" src="/assets/admin/js/sb-admin-datatables.js"></script>
<?php if($_SERVER['REQUEST_URI'] == '/admin/home'){ ?>
<script type="text/javascript" src="/assets/admin/js/sb-admin-charts.min.js"></script>
<?php } ?>
<!-- <script type="text/javascript" src="/assets/admin/js/autoNumeric.js"></script> -->
<script type="text/javascript" src="/assets/admin/js/jquery.priceformat.min.js"></script>
<script type="text/javascript" src="/assets/admin/js/typeahead.bundle.js"></script>
<script type="text/javascript" src="/assets/admin/js/sb-admin.js"></script>
<script type="text/javascript" src="/assets/admin/js/app.js"></script>
<?php 
    if (isset($detail_product['ID_P'])) {
      $id_product = $detail_product['ID_P'];
    }else{
      $id_product = '';
    }
?>
<?php if($this->uri->uri_string() == 'admin/product/add' or $this->uri->uri_string() == 'admin/product/edit/'.$id_product){ ?>
<script type="text/javascript">

  CKEDITOR.replace( 'full_product' ,{
    height: '500px'
  });
  CKEDITOR.replace( 'short_product' );
</script>
<?php } ?>
<?php
  if (isset($detail_post['ID_Post'])) {
    $id_post = $detail_post['ID_Post'];
  }else{
    $id_post = '';
  }
  if (isset($detail_page['ID_Page'])) {
    $id_page = $detail_page['ID_Page'];
  }else{
    $id_page = '';
  }
  if($this->uri->uri_string() == 'admin/posts/add' or $this->uri->uri_string() == 'admin/posts/edit/'.$id_post 
    or $this->uri->uri_string() == 'admin/page/add' or $this->uri->uri_string() == 'admin/page/edit/'.$id_page){ 
?>
<script type="text/javascript">
  CKEDITOR.replace( 'full_post' ,{
    height: '500px'
  });
  CKEDITOR.replace( 'short_post' ,{
    height: '113px'
  });
</script>
<?php } ?>
<script type="text/javascript">
  $(document).ready(function(){
    // $('#link_menu').selectpicker();
  });
  function get_val_link()
  {
    $('#link_cus_menu').val($('#link_menu').val());
    $('#name_menu').val($('#link_menu option:selected').text());
  }
</script>
<script>
var citynames = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  prefetch: {
    url: '/admin/product/tags_json',
    filter: function(list) {
      return $.map(list, function(cityname) {
        return { name: cityname }; });
    }
  }
});
citynames.initialize();

$('#tag_product').tagsinput({
  typeaheadjs: {
    name: 'citynames',
    displayKey: 'name',
    valueKey: 'name',
    source: citynames.ttAdapter()
  }
});
</script>
</body>
</html>