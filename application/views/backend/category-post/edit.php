<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <div class="row">
      <div class="col-xl-4 col-12">
        <h5><?php echo $header_page; ?></h5>
      </div>
      <div class="col-xl-8 col-12"><?php if(isset($message)){echo $message;} ?></div>
  </div>
  <div class="row">
    <div class="col-xl-2 col-12"></div>
    <div class="col-xl-8 col-12">
      <form action="" method="POST">
        <div class="card">
          <div class="card-header">
            Thêm danh mục mới
            <div class="pull-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="name_catpost">Tên danh mục</label>
              <input type="text" class="form-control" name="name_catpost" id="name_catpost" autofocus maxlength="50" required="required" value="<?php echo $detail_category['Name_CPost']; ?>">
            </div>
            <div class="form-group">
              <label for="keywords_catpost">Từ khóa SEO</label>
              <input type="text" class="form-control" name="keywords_catpost" id="keywords_catpost" maxlength="50" value="<?php echo $detail_category['Keywords_CPost']; ?>">
            </div>
            <div class="form-group">
              <label for="description_catpost">Mô tả SEO</label>
              <textarea class="form-control" name="description_catpost" id="description_catpost"><?php echo $detail_category['Description_CPost']; ?></textarea>
            </div>
            <div class="form-group">
              <label for="public_catpost">Hiển thị</label>
              <select class="form-control" name="public_catpost" id="public_catpost">
                <option value="Y" <?php if($detail_category['Public_CPost'] == 'Y'){echo "selected='selected'";} ?>>Hiển thị</option>
                <option value="N" <?php if($detail_category['Public_CPost'] == 'N'){echo "selected='selected'";} ?>>Ẩn</option>
              </select>
            </div>
            <div class="form-group">
              <label for="sort_catpost">Thứ tự</label>
              <input type="number" class="form-control" name="sort_catpost" id="sort_catpost" value="<?php echo $detail_category['Sort_CPost']; ?>">
            </div>
            <script type="text/javascript">
              function BrowseServer() {
                var finder = new CKFinder();
                finder.selectActionFunction = SetFileField;
                finder.popup();
              }
              function SetFileField(fileUrl) {
                var inputTag = '<input type="hidden" id="inputSThumbnail" name="thumbnail_catpost" value="">';
                $('#singleThumbnail').html('<div class="col-12"><img src="" class="img-fluid" id="Image" name="Image">'+inputTag+'</div><div class="col-12"><a href="javascript:void(0)" onclick="removeImg();" id="RemoveImg">Xóa hình ảnh</a></div>');
                $('#Image').attr('src',fileUrl);
                $('#inputSThumbnail').val(fileUrl);
              }
              function removeImg()
              {
                $('#Image').attr('src','');
                $('#inputSThumbnail').val('');
                $('#RemoveImg').remove();
              }
            </script>
            <div class="card">
              <div class="card-header">
                <span class="btn">
                  <i class="fa fa-picture-o"></i> Thêm hình ảnh
                </span>
                <span class="btn btn-primary fileinput-button pull-right" onclick="BrowseServer();">
                    <i class="fa fa-plus"></i>
                    <span>Thêm hình ảnh...</span>
                </span>
              </div>
              <div class="card-body">
                <div id="singleThumbnail">
                  <?php if($detail_category['Thumbnail_CPost'] != null){ ?>
                  <div class="col-12">
                    <img src="<?php echo $detail_category['Thumbnail_CPost']; ?>" class="img-fluid" id="Image" name="Image">
                    <input type="hidden" id="inputSThumbnail" name="thumbnail_catpost" value="<?php echo $detail_category['Thumbnail_CPost']; ?>">
                  </div>
                  <div class="col-12">
                    <a href="javascript:void(0)" onclick="removeImg();" id="RemoveImg">Xóa hình ảnh</a>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <div class="form-group text-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="col-xl-2 col-12"></div>
  </div>
</div>
<!-- /.container-fluid-->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="" method="POST">
    <div class="modal-content">
      <div class="modal-body">
        Bạn có chắc chắn xóa?
        <input type="hidden" class="form-control" id="id_cat" name="id_cat" value="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        <button type="submit" name="btnDelete" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
      </div>
    </div>
    </form>
  </div>
</div>
<script type="text/javascript">
  function get_id(id)
  {
    $('#id_cat').val(id);
  }
</script>