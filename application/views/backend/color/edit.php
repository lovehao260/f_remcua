<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <div class="row">
      <div class="col-xl-4 col-12">
        <h5><?php echo $header_page; ?></h5>
      </div>
      <div class="col-xl-8 col-12"><?php if(isset($message)){echo $message;} ?></div>
  </div>
  <div class="row">
    <div class="col-xl-2 col-12"></div>
    <div class="col-xl-8 col-12">
      <form action="" method="POST">
        <div class="card">
          <div class="card-header">
            Sửa màu
            <div class="pull-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="name_color">Tên màu</label>
              <input type="text" class="form-control" name="name_color" id="name_color" maxlength="50" required="required" value="<?php echo $detail_color['Name_Color']; ?>">
            </div>
            <div class="form-group">
              <label for="keywords_color">Từ khóa SEO</label>
              <input type="text" class="form-control" name="keywords_color" id="keywords_color" maxlength="50" value="<?php echo $detail_color['Keywords_Color']; ?>">
            </div>
            <div class="form-group">
              <label for="description_color">Mô tả SEO</label>
              <textarea class="form-control" name="description_color" id="description_color"><?php echo $detail_color['Description_Color']; ?></textarea>
            </div>
            <div class="form-group">
              <label for="code_color">Mã màu</label>
              <input type="text" class="form-control" name="code_color" id="code_color" required="required" value="<?php echo $detail_color['Code_Color']; ?>">
            </div>
            <div class="form-group text-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="col-xl-2 col-12"></div>
  </div>
</div>
<!-- /.container-fluid-->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="" method="POST">
    <div class="modal-content">
      <div class="modal-body">
        Bạn có chắc chắn xóa?
        <input type="hidden" class="form-control" id="id_cat" name="id_cat" value="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        <button type="submit" name="btnDelete" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
      </div>
    </div>
    </form>
  </div>
</div>
<script type="text/javascript">
  function get_id(id)
  {
    $('#id_cat').val(id);
  }
</script>