<div class="col-xl-4 col-12">
      <form action="" method="POST">
        <div class="card">
          <div class="card-header">
            Thêm màu mới
            <div class="pull-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="name_color">Tên màu</label>
              <input type="text" class="form-control" name="name_color" id="name_color" maxlength="50" required="required">
            </div>
            <div class="form-group">
              <label for="keywords_color">Từ khóa SEO</label>
              <input type="text" class="form-control" name="keywords_color" id="keywords_color" maxlength="50">
            </div>
            <div class="form-group">
              <label for="description_color">Mô tả SEO</label>
              <textarea class="form-control" name="description_color" id="description_color"></textarea>
            </div>
            <div class="form-group">
              <label for="code_color">Mã màu</label>
              <input type="text" class="form-control" name="code_color" id="code_color" required="required">
            </div>
            <div class="form-group text-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
        </div>
      </form>
    </div>