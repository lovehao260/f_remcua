<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <div class="row">
      <div class="col-xl-4 col-12">
        <h5><?php echo $header_page; ?></h5>
      </div>
      <div class="col-xl-8 col-12"><?php if(isset($message)){echo $message;} ?></div>
  </div>
  <div class="row">
    <?php $this->load->view('backend/color/add'); ?>
    <div class="col-xl-8 col-12">
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="dataTableColor" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Màu sắc</th>
              <th>Đường dẫn tĩnh</th>
              <th>Mã màu</th>
              <th>Cập nhật</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($get_list_color as $item){ ?>
            <tr>
              <td><span class="badge" style="font-size: 16px;font-weight: 100;color:#FFF;background-color:<?php echo $item['Code_Color']; ?>"><?php echo $item['Name_Color']; ?></span></td>
              <td><?php echo $item['Name_Slug_Color']; ?></td>
              <td><?php echo $item['Code_Color']; ?></td>
              <td><?php echo $item['Modify_Color']; ?></td>
              <td>
                <a href="<?php echo base_url('admin/product/editcolor/'.$item['ID_Color']); ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                <a href="" class="btn btn-danger btn-sm" onclick="get_id(<?php echo $item['ID_Color']; ?>)" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
              </td>
            </tr>
          <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid-->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="" method="POST">
    <div class="modal-content">
      <div class="modal-body">
        Bạn có chắc chắn xóa?
        <input type="hidden" class="form-control" id="id_color" name="id_color" value="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        <button type="submit" name="btnDelete" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
      </div>
    </div>
    </form>
  </div>
</div>
<script type="text/javascript">
  function get_id(id)
  {
    $('#id_color').val(id);
  }
</script>