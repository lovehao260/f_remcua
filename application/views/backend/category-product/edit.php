<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <div class="row">
      <div class="col-xl-4 col-12">
        <h5><?php echo $header_page; ?></h5>
      </div>
      <div class="col-xl-8 col-12"><?php if(isset($message)){echo $message;} ?></div>
  </div>
  <div class="row">
    <div class="col-xl-2 col-12"></div>
    <div class="col-xl-8 col-12">
      <form action="" method="POST">
        <div class="card">
          <div class="card-header">
            Sửa danh mục
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="name_catproduct">Tên danh mục</label>
              <input type="text" class="form-control" name="name_catproduct" id="name_catproduct" maxlength="50" required="required" value="<?php echo $detail_category['Name_CatP']; ?>">
            </div>
            <div class="form-group">
              <label for="keywords_catproduct">Từ khóa SEO</label>
              <input type="text" class="form-control" name="keywords_catproduct" id="keywords_catproduct" maxlength="50" value="<?php echo $detail_category['Keywords_CatP']; ?>">
            </div>
            <div class="form-group">
              <label for="description_catproduct">Mô tả SEO</label>
              <textarea class="form-control" name="description_catproduct" id="description_catproduct"><?php echo $detail_category['Description_CatP']; ?></textarea>
            </div>
            <div class="form-group">
              <label for="position_catproduct">Vị trí</label>
              <select class="form-control" name="position_catproduct" id="position_catproduct">
              <?php foreach($position as $key => $value){ ?>
                <option value="<?php echo $key; ?>" <?php if($detail_category['Position_CatP'] == $key){echo "selected='selected'";} ?>><?php echo $value; ?></option>
              <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label for="sort_catproduct">Thứ tự</label>
              <input type="number" class="form-control" name="sort_catproduct" id="sort_catproduct" value="<?php echo $detail_category['Sort_CatP']; ?>">
            </div>
            <script type="text/javascript">
              function BrowseServer() {
                var finder = new CKFinder();
                finder.selectActionFunction = SetFileField;
                finder.popup();
              }
              function SetFileField(fileUrl) {
                var inputTag = '<input type="hidden" id="inputSThumbnail" name="singleThumbnail" value="">';
                $('#singleThumbnail').html('<div class="col-12"><img src="" class="img-fluid" id="Image" name="Image">'+inputTag+'</div><div class="col-12"><a href="javascript:void(0)" onclick="removeImg();" id="RemoveImg">Xóa hình ảnh</a></div>');
                $('#Image').attr('src',fileUrl);
                $('#inputSThumbnail').val(fileUrl);
              }
              function removeImg()
              {
                $('#Image').attr('src','');
                $('#inputSThumbnail').val('');
                $('#RemoveImg').remove();
              }
            </script>
            <div class="card">
              <div class="card-header">
                <span class="btn">
                  <i class="fa fa-picture-o"></i> Thêm hình ảnh
                </span>
                <span class="btn btn-primary fileinput-button pull-right" onclick="BrowseServer();">
                    <i class="fa fa-plus"></i>
                    <span>Thêm hình ảnh...</span>
                </span>
              </div>
              <div class="card-body">
                <div id="singleThumbnail">
                  <?php if($detail_category['Thumbnail_CatP'] != null){ ?>
                  <div class="col-12">
                    <img src="<?php echo $detail_category['Thumbnail_CatP']; ?>" class="img-fluid" id="Image" name="Image">
                    <input type="hidden" id="inputSThumbnail" name="singleThumbnail" value="<?php echo $detail_category['Thumbnail_CatP']; ?>">
                  </div>
                  <div class="col-12">
                    <a href="javascript:void(0)" onclick="removeImg();" id="RemoveImg">Xóa hình ảnh</a>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <div class="form-group text-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="col-xl-2 col-12"></div>
  </div>
</div>
<!-- /.container-fluid-->