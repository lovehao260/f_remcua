<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <div class="row">
      <div class="col-xl-4 col-12">
        <h5><?php echo $header_page; ?></h5>
      </div>
      <div class="col-xl-4 col-12"><?php echo $this->session->flashdata('message'); ?></div>
      <div class="col-xl-4 col-12">
        <div class="button-crud text-right">
          <a href="<?php echo base_url('admin/menu'); ?>" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Thêm menu</a>
        </div>
      </div>
  </div>
  <div class="row">
    <div class="col-xl-2 col-12"></div>
    <div class="col-xl-8 col-12">
      <form action="" method="POST">
        <div class="card">
          <div class="card-header">
            <?php echo $title_card; ?>
            <div class="pull-right">
              <?php if(isset($detail_menu)){ ?>
              <a href="<?php echo base_url('admin/menu'); ?>" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Thêm menu</a>
              <?php } ?>
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="name_menu">Tên menu</label>
              <input type="text" class="form-control" name="name_menu" id="name_menu" autofocus maxlength="50" required="required" value="<?php if(isset($detail_menu)){echo $detail_menu['Name_Menu'];} ?>">
            </div>
            <div class="form-group">
              <label for="cat_menu">Liên kết có sẵn</label>
              <select class="form-control" id="link_menu" name="link_menu" data-live-search="true" onchange="get_val_link()">
                <option value="">--- Không chọn ---</option>
                <optgroup label="Sản phẩm">
                  <option value="/tat-ca-san-pham">Sản phẩm</option>
                <?php foreach($get_cat_product as $item){ ?>
                  <option value="/danh-muc-san-pham/<?php echo $item['Name_Slug_CatP'].'-'.$item['ID_CatP']; ?>" <?php if(isset($detail_menu)){if($detail_menu['Link_Menu'] == '/danh-muc-san-pham/'.$item['Name_Slug_CatP'].'-'.$item['ID_CatP']){echo "selected='selected'";}} ?>><?php echo $item['Name_CatP']; ?></option>
                <?php } ?>
                </optgroup>
                <optgroup label="Bài viết">
                  <option value="/posts">Tin tức</option>
                <?php foreach($get_cat_post as $item){ ?>
                  <option value="/danh-muc-bai-viet/<?php echo $item['Name_Slug_CPost'].'-'.$item['ID_CPost']; ?>" <?php if(isset($detail_menu)){if($detail_menu['Link_Menu'] == '/danh-muc-bai-viet/'.$item['Name_Slug_CPost'].'-'.$item['ID_CPost']){echo "selected='selected'";}} ?>><?php echo $item['Name_CPost']; ?></option>
                <?php } ?>
                </optgroup>
                <optgroup label="Trang">
                <?php foreach($get_page as $item){ ?>
                  <option value="/page/<?php echo $item['Title_Slug_Page'].'-'.$item['ID_Page']; ?>" <?php if(isset($detail_menu)){if($detail_menu['Link_Menu'] == '/page/'.$item['Title_Slug_Page'].'-'.$item['ID_Page']){echo "selected='selected'";}} ?>><?php echo $item['Title_Page']; ?></option>
                <?php } ?>
                </optgroup>
                <optgroup label="Trang khác">
                  <option value="/" <?php if(isset($detail_menu)){if($detail_menu['Link_Menu'] == '/'){echo "selected='selected'";}} ?>>Trang chủ</option>
                  <option value="/lien-he" <?php if(isset($detail_menu)){if($detail_menu['Link_Menu'] == '/lien-he'){echo "selected='selected'";}} ?>>Liên hệ</option>
                </optgroup>
              </select>
            </div>
            <div class="form-group">
              <label for="link_cus_menu">Liên kết tĩnh</label>
              <input type="text" class="form-control" name="link_cus_menu" id="link_cus_menu" maxlength="50" required="required" value="<?php if(isset($detail_menu)){echo $detail_menu['Link_Menu'];} ?>">
            </div>
            <?php if($menu_dropdown != null){ ?>
            <div class="form-group">
              <label for="parent_menu">Menu cha</label>
              <select class="form-control" name="parent_menu" id="parent_menu">
                <option value="">--- Không chọn ---</option>
              <?php foreach($menu_dropdown as $key => $item){ ?>
                <option value="<?php echo $key; ?>" <?php if(isset($detail_menu)){if($detail_menu['Parent_Menu'] == $key){echo "selected='selected'";}} ?>><?php echo $item; ?></option>
              <?php } ?>
              </select>
            </div>
            <?php } ?>
            <div class="form-group">
              <label for="icon_menu">Icon</label>
              <input type="text" class="form-control" name="icon_menu" id="icon_menu" maxlength="50" value="<?php if(isset($detail_menu)){echo $detail_menu['Icon_Menu'];} ?>">
            </div>
            <div class="form-group">
              <label for="sort_menu">Thứ tự</label>
              <input type="number" class="form-control" name="sort_menu" id="sort_menu" maxlength="50" value="<?php if(isset($detail_menu)){echo $detail_menu['Sort_Menu'];}else{echo "0";} ?>">
            </div>
            <div class="form-group">
              <label for="public_menu">Hiển thị</label>
              <select class="form-control" name="public_menu" id="public_menu">
                <option value="Y" <?php if(isset($detail_menu)){if($detail_menu['Public_Menu'] == 'Y'){echo "selected='selected'";}} ?>>Hiển thị</option>
                <option value="N" <?php if(isset($detail_menu)){if($detail_menu['Public_Menu'] == 'N'){echo "selected='selected'";}} ?>>Ẩn</option>
              </select>
            </div>
            <div class="form-group text-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="col-xl-2 col-12"></div>
  </div>
</div>
<!-- /.container-fluid-->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="" method="POST">
    <div class="modal-content">
      <div class="modal-body">
        Bạn có chắc chắn xóa?
        <input type="hidden" class="form-control" id="id_catpost" name="id_catpost" value="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        <button type="submit" name="btnDelete" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
      </div>
    </div>
    </form>
  </div>
</div>
<script type="text/javascript">
  function get_id(id)
  {
    $('#id_catpost').val(id);
  }
</script>