<div class="col-xl-4 col-12">
  <form action="" method="POST">
    <div class="card">
      <div class="card-header">
        <?php echo $title_card; ?>
        <div class="pull-right">
          <?php if(isset($detail_menu)){ ?>
          <a href="<?php echo base_url('admin/menu'); ?>" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Thêm menu</a>
          <?php } ?>
          <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
        </div>
      </div>
      <div class="card-body">
        <div class="form-group">
          <label for="name_menu">Tên menu</label>
          <input type="text" class="form-control" name="name_menu" id="name_menu" autofocus maxlength="50" required="required" value="">
        </div>
        <div class="form-group">
          <label for="cat_menu">Liên kết có sẵn</label>
          <select class="form-control" id="link_menu" name="link_menu" data-live-search="true" onchange="get_val_link()">
            <option value="">--- Không chọn ---</option>
            <optgroup label="Danh mục sản phẩm">
              <option value="/tat-ca-san-pham">Sản phẩm</option>
            <?php foreach($get_cat_product as $item){ ?>
              <option value="/danh-muc-san-pham/<?php echo $item['Name_Slug_CatP'].'-'.$item['ID_CatP']; ?>"><?php echo $item['Name_CatP']; ?></option>
            <?php } ?>
            </optgroup>
            <optgroup label="Danh mục bài viết">
              <option value="/posts">Tin tức</option>
            <?php foreach($get_cat_post as $item){ ?>
              <option value="/danh-muc-bai-viet/<?php echo $item['Name_Slug_CPost'].'-'.$item['ID_CPost']; ?>"><?php echo $item['Name_CPost']; ?></option>
            <?php } ?>
            </optgroup>
            <optgroup label="Trang">
            <?php foreach($get_page as $item){ ?>
              <option value="/page/<?php echo $item['Title_Slug_Page'].'-'.$item['ID_Page']; ?>"><?php echo $item['Title_Page']; ?></option>
            <?php } ?>
            </optgroup>
            <optgroup label="Trang khác">
              <option value="/">Trang chủ</option>
              <option value="/lien-he">Liên hệ</option>
            </optgroup>
          </select>
        </div>
        <div class="form-group">
          <label for="link_cus_menu">Liên kết tĩnh</label>
          <input type="text" class="form-control" name="link_cus_menu" id="link_cus_menu" maxlength="50" required="required" value="<?php if(isset($detail_menu)){echo $detail_menu['Link_Menu'];} ?>">
        </div>
        <?php if($menu_dropdown != null){ ?>
        <div class="form-group">
          <label for="parent_menu">Menu cha</label>
          <select class="form-control" name="parent_menu" id="parent_menu">
            <option value="">--- Không chọn ---</option>
          <?php foreach($menu_dropdown as $key => $item){ ?>
            <option value="<?php echo $key; ?>"><?php echo $item; ?></option>
          <?php } ?>
          </select>
        </div>
        <?php } ?>
        <div class="form-group">
          <label for="icon_menu">Icon</label>
          <input type="text" class="form-control" name="icon_menu" id="icon_menu" maxlength="50" value="">
        </div>
        <div class="form-group">
          <label for="sort_menu">Thứ tự</label>
          <input type="number" class="form-control" name="sort_menu" id="sort_menu" maxlength="50" value="">
        </div>
        <div class="form-group">
          <label for="public_menu">Hiển thị</label>
          <select class="form-control" name="public_menu" id="public_menu">
            <option value="Y">Hiển thị</option>
            <option value="N">Ẩn</option>
          </select>
        </div>
        <div class="form-group text-right">
          <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
        </div>
      </div>
    </div>
  </form>
</div>