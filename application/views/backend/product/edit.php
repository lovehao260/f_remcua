<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <form action="" method="POST" id="formProduct">
    <div class="row">
        <div class="col-xl-4 col-12">
          <h5><?php echo $header_page; ?></h5>
        </div>
        <div class="col-xl-4 col-12"><?php echo validation_errors(); ?><?php echo $this->session->flashdata('message'); ?></div>
        <div class="col-xl-4 col-12">
          <div class="button-crud text-right">
            <button type="submit" class="btn btn-success" name="btnSave" id="btnSave"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            <a href="<?php echo base_url('admin/product/add'); ?>" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Thêm sản phẩm</a>
          </div>
        </div>
    </div>
    <div class="row">
      <div class="col-xl-9 col-12">
        <div class="form-group">
          <label class="label" for="name_product">Tên sản phẩm</label>
          <input type="text" class="form-control" maxlength="150" name="name_product" id="name_product" value="<?php echo $detail_product['Name_P']; ?>">
        </div>
        <div class="form-group">
          <label class="" for="short_product">Mô tả ngắn</label>
          <textarea class="form-control" maxlength="500" name="short_product" id="short_product"><?php echo $detail_product['Short_Content_P']; ?></textarea>
        </div>
        <div class="form-group">
          <label class="" for="full_product">Mô tả sản phẩm</label>
          <textarea class="form-control" name="full_product" id="full_product" rows="5"><?php echo $detail_product['Full_Content_P']; ?></textarea>
        </div>
        <div class="form-group">
          <label class="" for="tag_product">Tags</label>
          <input type="text" class="form-control" name="tag_product" id="tag_product" value="<?php echo $data_name_tag; ?>">
        </div>
        <div class="form-group">
          <label class="" for="keyword_product">Từ khóa SEO</label>
          <input type="text" class="form-control" maxlength="100" name="keyword_product" id="keyword_product" value="<?php echo $detail_product['Keywords_P']; ?>">
        </div>
        <div class="form-group">
          <label class="" for="description_product">Mô tả SEO</label>
          <textarea class="form-control" maxlength="150" name="description_product" id="description_product"><?php echo $detail_product['Description_P']; ?></textarea>
        </div>

        
      </div>
      <!-- /END COL -->
      <div class="col-xl-3 col-12">
        <div class="form-group">
          <label class="label" for="code_product">Mã sản phẩm</label>
          <input type="text" class="form-control" maxlength="15" name="code_product" id="code_product" value="<?php echo $detail_product['Code_P']; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="price_02_product">Giá mét tới</label>
          <input type="text" class="form-control price_product" maxlength="15" onkeyup="tinhgiametvuong()" name="price_02_product" id="price_02_product" value="<?php echo $detail_product['Price_2_P']; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="price_product">Giá mét vuông</label>
          <input type="text" class="form-control price_product" maxlength="15" name="price_product" id="price_product" value="<?php echo $detail_product['Price_P']; ?>">
        </div>
        <div class="form-group">
          <label class="label" for="status_product">Trạng thái</label>
          <select class="form-control" id="status_product" name="status_product">
            <option value="Y" <?php if($detail_product['Status_P'] == 'Y'){echo "selected='selected'";} ?>>Còn hàng</option>
            <option value="N" <?php if($detail_product['Status_P'] == 'N'){echo "selected='selected'";} ?>>Hết hàng</option>
          </select>
        </div>
        <div class="form-group">
          <label class="label" for="public_product">Hiển thị</label>
          <select class="form-control" id="public_product" name="public_product">
            <option value="Y" <?php if($detail_product['Public_P'] == 'Y'){echo "selected='selected'";} ?>>Hiển thị</option>
            <option value="N" <?php if($detail_product['Public_P'] == 'N'){echo "selected='selected'";} ?>>Ẩn</option>
          </select>
        </div>
        <div class="form-group">
          <label class="label" for="category_product">Danh mục</label>
          <select class="form-control" id="category_product" name="category_product">
            <option value="">---Chọn danh mục---</option>
            <?php foreach($get_list_category as $item){ ?>
            <option value="<?php echo $item['ID_CatP']; ?>" <?php if($detail_product['Category_P'] == $item['ID_CatP']){echo "selected='selected'";} ?>><?php echo $item['Name_CatP']; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label class="label" for="color_product">Màu sắc :</label>
          <?php 
            foreach($get_list_color as $item){ 
          ?>
            <label for="default_<?php echo $item['ID_Color']; ?>" class="btn btn-primary" style="background-color:<?php echo $item['Code_Color']; ?>;border-color:<?php echo $item['Code_Color']; ?>"><?php echo $item['Name_Color']; ?> 
              <input type="checkbox" id="default_<?php echo $item['ID_Color']; ?>" <?php if(in_array($item['ID_Color'], $color_check) == true){echo "checked='checked'";} ?> name="color_product[]" class="badgebox" value="<?php echo $item['ID_Color']; ?>">
              <span class="badge">&check;</span>
            </label>
          <?php } ?>
        </div>
        <script>
          function BrowseServer() {
            var finder = new CKFinder();
            finder.selectActionFunction = SetFileField;
            finder.popup();
          }
          function SetFileField(fileUrl) {
            var inputTag = '<input type="hidden" id="inputSThumbnail" name="thumbnail_product" value="">';
            $('#singleThumbnail').html('<div class="col-12"><img src="" class="img-fluid" id="Image">'+inputTag+'</div><div class="col-12"><a href="javascript:void(0)" onclick="removeImg();" id="RemoveImg">Xóa hình ảnh</a></div>');
            $('#Image').attr('src',fileUrl);
            $('#inputSThumbnail').val(fileUrl);
          }
          function removeImg()
          {
            $('#Image').attr('src','');
            $('#inputSThumbnail').val('');
            $('#RemoveImg').remove();
          }
          function removeImgS(item)
          {
            $('#item_img_'+item+'').remove();
            $.ajax({
              type: 'POST',
              url: '/admin/product/remove_thumbnail/<?php echo $detail_product["ID_P"]; ?>/'+item,
              cache: false
            });
          }
          function BrowseServer_s() {
            var finder = new CKFinder();
            finder.selectActionFunction = SetFileField_s;
            finder.popup();
          }
          function SetFileField_s(fileUrl) {
              var itemCountInput = ($("[id^='inputSThumbnail-']").length + 1);
              var itemCountImage = ($("[id^='Image-']").length + 1);
              var itemCountGroup = ($("[id^='item_img_']").length + 1);
              var inputTag = '<input type="hidden" id="inputSThumbnail-'+itemCountInput+'" name="albumThumbnail[]" value="'+fileUrl+'">';
              var remove = '<a href="javascript:void(0)" onclick="removeImgS('+itemCountGroup+');">Xóa hình ảnh</a>';
              $('#showThumbnail').append('<div class="col-6" id="item_img_'+itemCountGroup+'"><img src="'+fileUrl+'" class="img-fluid" id="Image-'+itemCountImage+'" name="Image">'+inputTag+remove+'</div>');
          }
        </script>
        <div class="card">
          <div class="card-header">
            <span class="btn">
              <i class="fa fa-picture-o"></i> Thêm hình ảnh
            </span>
            <span class="btn btn-primary fileinput-button pull-right" onclick="BrowseServer();">
                <i class="fa fa-plus"></i>
                <span>Thêm hình ảnh...</span>
            </span>
          </div>
          <div class="card-body">
            <div id="singleThumbnail">
              <?php if($detail_product['Thumbnail_P'] != null){ ?>
              <div class="col-12">
                <img src="<?php echo $detail_product['Thumbnail_P']; ?>" class="img-fluid" id="Image" name="Image">
                <input type="hidden" id="inputSThumbnail" name="thumbnail_product" value="<?php echo thumbnail_product($detail_product['Thumbnail_P']); ?>">
              </div>
              <div class="col-12">
                <a href="javascript:void(0)" onclick="removeImg();" id="RemoveImg">Xóa hình ảnh</a>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header">
            <span class="btn">
              <i class="fa fa-picture-o"></i> Thêm album ảnh
            </span>
            <span class="btn btn-primary fileinput-button pull-right" onclick="BrowseServer_s();">
                <i class="fa fa-plus"></i>
                <span>Thêm album hình ảnh...</span>
            </span>
          </div>
          <div class="card-body">
            <div class="row" id="showThumbnail">
            <?php if(!empty($get_thumbnail)){foreach($get_thumbnail as $item){ ?>
              <div class="col-6" id="item_img_<?php echo $item['ID_Tn']; ?>">
                <img src="<?php echo $item['Thumbnail_link_Tn']; ?>" class="img-fluid" id="Image-<?php echo $item['ID_Tn']; ?>" name="Image">
                <input type="hidden" id="inputSThumbnail-<?php echo $item['ID_Tn']; ?>" name="albumThumbnail[]" value="<?php echo $item['Thumbnail_link_Tn']; ?>">
                <a href="javascript:void(0)" onclick="removeImgS(<?php echo $item['ID_Tn']; ?>);">Xóa hình ảnh</a>
              </div>
            <?php }} ?>
            </div>
          </div>
        </div>
      </div>
      <!-- /END COL -->
    </div>
    <!-- /END ROW -->
    <div class="row">
        <div class="col-8"></div>
        <div class="col-4">
          <div class="button-crud text-right">
            <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
          </div>
        </div>
    </div>
  </form>
</div>
<!-- /.container-fluid-->
<script type="text/javascript">
  function tinhgiametvuong()
  {
      var mettoi = $('#price_02_product').unmask();
      var tinhtoan = mettoi * <?php echo $this->setting[19]; ?>;
      $('#price_product').val(tinhtoan);
  }
</script>