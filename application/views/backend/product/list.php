<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <div class="row">
      <div class="col-4">
        <h5><?php echo $header_page; ?></h5>
      </div>
      <div class="col-4"><?php echo $this->session->flashdata('message'); ?></div>
      <div class="col-4">
        <div class="button-crud text-right">
          <a href="<?php echo base_url('admin/product/add'); ?>" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Thêm sản phẩm</a>
        </div>
      </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="dataTableProduct" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>#ID</th>
              <th>Hình ảnh</th>
              <th>Tên sản phẩm</th>
              <th>Danh mục</th>
              <th>Giá</th>
              <th>Mã sản phẩm</th>
              <th>Trạng thái</th>
              <th>Hiển thị</th>
              <th>Lượt xem</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <!-- Ajax -->
        </table>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid-->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="" method="POST">
    <div class="modal-content">
      <div class="modal-body">
        Bạn có chắc chắn xóa?
        <input type="hidden" class="form-control" id="id_product" name="id_product" value="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        <button type="submit" name="btnDelete" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
      </div>
    </div>
    </form>
  </div>
</div>
<script type="text/javascript">
  function get_id(id)
  {
    $('#id_product').val(id);
  }
</script>