<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <div class="row">
      <div class="col-xl-4 col-12">
        <h5><?php echo $header_page; ?></h5>
      </div>
      <div class="col-xl-4 col-12"><?php echo $this->session->flashdata('message'); ?></div>
      <div class="col-xl-4 col-12">
        <div class="button-crud text-right">
          <a href="<?php echo base_url('admin/ads/add'); ?>" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Thêm quảng cáo</a>
        </div>
      </div>
  </div>
  <div class="row">
    <div class="col-xl-12 col-12">
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th width="200">Hình ảnh</th>
              <th>Tên quảng cáo</th>
              <th width="150">Vị trí</th>
              <th width="150">Lượt click</th>
              <th width="150">Thứ tự</th>
              <th width="150">Hiển thị</th>
              <th width="150">Thao tác</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($get_ads as $item){ ?>
            <tr>
              <td><a href="<?php echo base_url('admin/ads/edit/'.$item['ID_Ads']); ?>"><img src="<?php echo base_url($item['Image_Ads']); ?>" width="200"></a></td>
              <td><a href="<?php echo base_url('admin/ads/edit/'.$item['ID_Ads']); ?>"><?php echo $item['Name_Ads']; ?></a></td>
              <td><?php echo $item['Position_Ads']; ?></td>
              <td><?php echo $item['Click_Ads']; ?></td>
              <td><?php echo $item['Sort_Ads']; ?></td>
              <td><?php echo public_item($item['Public_Ads']); ?></td>
              <td>
                <a href="<?php echo base_url('/admin/ads/edit/'.$item['ID_Ads']); ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                <a href="" class="btn btn-danger btn-sm" onclick="get_id(<?php echo $item['ID_Ads']; ?>)" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid-->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="" method="POST">
    <div class="modal-content">
      <div class="modal-body">
        Bạn có chắc chắn xóa?
        <input type="hidden" class="form-control" id="id_ads" name="id_ads" value="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        <button type="submit" name="btnDelete" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
      </div>
    </div>
    </form>
  </div>
</div>
<script type="text/javascript">
  function get_id(id)
  {
    $('#id_ads').val(id);
  }
</script>