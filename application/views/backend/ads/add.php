<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <form action="" method="POST">
    <div class="row">
        <div class="col-xl-4 col-12">
          <h5><?php echo $header_page; ?></h5>
        </div>
        <div class="col-xl-4 col-12"><?php echo $this->session->flashdata('message'); ?></div>
        <div class="col-xl-4 col-12">
          <div class="button-crud text-right">
            <button type="submit" class="btn btn-success" name="btnSave" id="btnSave"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
          </div>
        </div>
    </div>
    <div class="row">
      <div class="col-xl-9 col-12">
        <div class="form-group">
          <label class="label" for="name_ads">Tên quảng cáo</label>
          <input type="text" class="form-control" maxlength="150" name="name_ads" id="name_ads" autofocus required="required" value="<?php echo set_value('name_ads'); ?>">
        </div>
        <div class="form-group">
          <label class="label" for="linkads_ads">Liên kết tĩnh</label>
          <input type="text" class="form-control" maxlength="250" name="linkads_ads" id="linkads_ads" required="required" value="<?php echo set_value('linkads_ads'); ?>">
        </div>
        <script>
          function BrowseServer() {
            var finder = new CKFinder();
            finder.selectActionFunction = SetFileField;
            finder.popup();
          }
          function SetFileField(fileUrl) {
            var inputTag = '<input type="hidden" id="inputSThumbnail" name="image_ads" value="">';
            $('#singleThumbnail').html('<div class="col-12"><img src="" class="img-fluid" id="Image">'+inputTag+'</div><div class="col-12"><a href="javascript:void(0)" onclick="removeImg();" id="RemoveImg">Xóa hình ảnh</a></div>');
            $('#Image').attr('src',fileUrl);
            $('#inputSThumbnail').val(fileUrl);
          }
          function removeImg()
          {
            $('#Image').attr('src','');
            $('#inputSThumbnail').val('');
            $('#RemoveImg').remove();
          }
        </script>
        <div class="card">
          <div class="card-header">
            <span class="btn">
              <i class="fa fa-picture-o"></i> Thêm hình ảnh
            </span>
            <span class="btn btn-primary fileinput-button pull-right" onclick="BrowseServer();">
                <i class="fa fa-plus"></i>
                <span>Thêm hình ảnh...</span>
            </span>
          </div>
          <div class="card-body">
            <div id="singleThumbnail"></div>
          </div>
        </div>   
      </div>
      <!-- /END COL -->
      <div class="col-xl-3 col-12">
        <div class="form-group">
          <label class="label" for="position_ads">Vị trí</label>
          <select class="form-control" id="position_ads" name="position_ads">
            <option value="">--- Không có vị trí ---</option>
          <?php foreach($this->position as $key => $item){ ?>
            <option value="<?php echo $key; ?>"><?php echo $item; ?></option>
          <?php } ?> 
          </select>
        </div>
        <div class="form-group">
          <label class="label" for="public_ads">Hiển thị</label>
          <select class="form-control" id="public_ads" name="public_ads">
            <option value="Y">Hiển thị</option>
            <option value="N">Ẩn</option>
          </select>
        </div>
        <div class="form-group">
          <label class="label" for="sort_ads">Thứ tự <em>(Số thứ tự nhỏ nhất sẽ được hiển thị đầu tiên)</em></label>
          <input type="number" class="form-control" name="sort_ads" id="sort_ads" value="0">
        </div>
      </div>
      <!-- /END COL -->
    </div>
    <!-- /END ROW -->
    <div class="row">
        <div class="col-8"></div>
        <div class="col-4">
          <div class="button-crud text-right">
            <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
          </div>
        </div>
    </div>
  </form>
</div>
<!-- /.container-fluid-->