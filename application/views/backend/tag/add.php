<div class="col-xl-4 col-12">
      <form action="" method="POST">
        <div class="card">
          <div class="card-header">
            Tạo tag mới
            <div class="pull-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="name_tag">Tag</label>
              <input type="text" class="form-control" name="name_tag" id="name_tag" maxlength="50" required="required" autofocus>
            </div>
            <div class="form-group text-right">
              <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
            </div>
          </div>
        </div>
      </form>
    </div>