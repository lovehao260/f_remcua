<!DOCTYPE html>
<html lang="vi">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="<?php echo $meta_description; ?>">
  <meta name="author" content="<?php echo $meta_author; ?>">
  <title><?php echo $meta_title; ?></title>
  <!-- Bootstrap core CSS-->
  <link href="/assets/admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <!-- Custom fonts for this template-->
  <link href="/assets/admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="/assets/admin/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link rel="stylesheet" href="/assets/admin/css/bootstrap-select.min.css">
  <link rel="stylesheet" type="text/css" href="/assets/admin/css/bootstrap-colorpicker.css">
  <link rel="stylesheet" type="text/css" href="/assets/admin/bootstrap-tagsinput/bootstrap-tagsinput.css">
  <link rel="stylesheet" href="/assets/admin/css/jquery.fileupload.css">
  <link rel="stylesheet" href="/assets/admin/css/jquery.fileupload-ui.css">
  <!-- Custom styles for this template-->
  <link href="/assets/admin/css/sb-admin.css" rel="stylesheet" type="text/css">
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="<?php echo base_url('admin/home'); ?>"><?php echo $this->setting[0]; ?></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item <?php echo active_menu('home'); ?>" data-toggle="tooltip" data-placement="right" title="Bảng điều khiển">
          <a class="nav-link" href="<?php echo base_url('admin/home'); ?>">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Bảng điều khiển</span>
          </a>
        </li>
        <li class="nav-item <?php echo active_menu('product');echo active_menu('tags'); ?>" data-toggle="tooltip" data-placement="right" title="Sản phẩm">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#navProduct" data-parent="#exampleAccordion">
            <i class="fa fa-product-hunt" aria-hidden="true"></i>
            <span class="nav-link-text">Sản phẩm</span>
          </a>
          <ul class="sidenav-second-level collapse" id="navProduct">
            <li>
              <a href="<?php echo base_url('admin/product'); ?>">Tất cả sản phẩm</a>
            </li>
            <li>
              <a href="<?php echo base_url('admin/product/add'); ?>">Thêm sản phẩm</a>
            </li>
            <li>
              <a href="<?php echo base_url('admin/product/category'); ?>">Danh mục</a>
            </li>
            <li>
              <a href="<?php echo base_url('admin/product/color'); ?>">Màu sắc</a>
            </li>
            <li>
              <a href="<?php echo base_url('admin/tags'); ?>">Tags</a>
            </li>
          </ul>
        </li>
        <li class="nav-item <?php echo active_menu('posts'); ?>" data-toggle="tooltip" data-placement="right" title="Bài viết">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#navPost" data-parent="#exampleAccordion">
            <i class="fa fa-thumb-tack" aria-hidden="true"></i>
            <span class="nav-link-text">Bài viết</span>
          </a>
          <ul class="sidenav-second-level collapse" id="navPost">
            <li>
              <a href="<?php echo base_url('admin/posts'); ?>">Tất cả bài viết</a>
            </li>
            <li>
              <a href="<?php echo base_url('admin/posts/add'); ?>">Viết bài mới</a>
            </li>
            <li>
              <a href="<?php echo base_url('admin/posts/category'); ?>">Danh mục</a>
            </li>
          </ul>
        </li>
        <li class="nav-item <?php echo active_menu('page'); ?>" data-toggle="tooltip" data-placement="right" title="Trang">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#navPage" data-parent="#exampleAccordion">
            <i class="fa fa-file" aria-hidden="true"></i>
            <span class="nav-link-text">Trang</span>
          </a>
          <ul class="sidenav-second-level collapse" id="navPage">
            <li>
              <a href="<?php echo base_url('admin/page'); ?>">Tất cả các trang</a>
            </li>
            <li>
              <a href="<?php echo base_url('admin/page/add'); ?>">Thêm trang mới</a>
            </li>
          </ul>
        </li>
        <li class="nav-item <?php echo active_menu('contact'); ?>" data-toggle="tooltip" data-placement="right" title="Liên hệ">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#navContact" data-parent="#exampleAccordion">
            <i class="fa fa-address-book" aria-hidden="true"></i>
            <?php 
              $count_contact =  $this->LP_Contact_BE->count_contact_new(); 
            ?>
            <span class="nav-link-text">Liên hệ <?php if($count_contact != 0){ ?><span class="badge badge-success"><?php echo $count_contact; ?></span><?php } ?></span>
          </a>
          <ul class="sidenav-second-level collapse" id="navContact">
            <li>
              <a href="<?php echo base_url('admin/contact'); ?>">Tất cả liên hệ <?php if($count_contact != 0){ ?><span class="badge badge-success"><?php echo $count_contact; ?></span><?php } ?></a>
            </li>
            <li>
              <a href="<?php echo base_url('admin/contact/add'); ?>">Thêm liên hệ mới</a>
            </li>
            <li>
              <a href="<?php echo base_url('admin/contact/subscribe'); ?>">Subscribe</a>
            </li>
          </ul>
        </li>
        <li class="nav-item <?php echo active_menu('menu'); ?>" data-toggle="tooltip" data-placement="right" title="Menu">
          <a class="nav-link" href="<?php echo base_url('admin/menu'); ?>">
            <i class="fa fa-bars" aria-hidden="true"></i>
            <span class="nav-link-text">Menu</span>
          </a>
        </li>
        <li class="nav-item <?php echo active_menu('carousel'); ?>" data-toggle="tooltip" data-placement="right" title="Trình chiếu ảnh">
          <a class="nav-link" href="<?php echo base_url('admin/carousel'); ?>">
            <i class="fa fa-object-group" aria-hidden="true"></i>
            <span class="nav-link-text">Trình chiếu ảnh</span>
          </a>
        </li>
        <li class="nav-item <?php echo active_menu('ads'); ?>" data-toggle="tooltip" data-placement="right" title="Quảng cáo">
          <a class="nav-link" href="<?php echo base_url('admin/ads'); ?>">
            <i class="fa fa-bullhorn" aria-hidden="true"></i>
            <span class="nav-link-text">Quảng cáo</span>
          </a>
        </li>
        <li class="nav-item <?php echo active_menu('users'); ?>" data-toggle="tooltip" data-placement="right" title="Thành viên">
          <a class="nav-link" href="<?php echo base_url('admin/users'); ?>">
            <i class="fa fa-users" aria-hidden="true"></i>
            <span class="nav-link-text">Thành viên</span>
          </a>
        </li>
        <li class="nav-item <?php echo active_menu('setting'); ?>" data-toggle="tooltip" data-placement="right" title="Cài đặt">
          <a class="nav-link" href="<?php echo base_url('admin/setting'); ?>">
            <i class="fa fa-cogs" aria-hidden="true"></i>
            <span class="nav-link-text">Cài đặt</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-envelope"></i>
            <span class="d-lg-none">Liên hệ
              <?php if($count_contact != 0){ ?><span class="badge badge-pill badge-primary"><?php echo $count_contact; ?></span><?php } ?>
            </span>
            <?php if($count_contact > 0){ ?>
            <span class="indicator text-primary d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
            <?php } ?>
          </a>
          <div class="dropdown-menu" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">Liên hệ mới: <?php echo $count_contact; ?></h6>
            <?php 
            $contact = $this->LP_Contact_BE->get_contact_notification();
              foreach($contact as $item){ 
                if($item['Read_Contact'] == 'N'){
            ?>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?php echo base_url('/admin/contact/view/'.$item['ID_Contact']); ?>">
              <strong><?php echo $item['Name_Contact']; ?></strong>
              <span class="small float-right text-muted"><?php echo datetime_item($item['Createday_Contact']); ?></span>
              <div class="dropdown-message small"><?php echo word_limiter($item['Messages_Contact'],12); ?></div>
            </a>
            <?php }} ?>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item small" href="<?php echo base_url('admin/contact'); ?>">Xem tất cả liên hệ</a>
          </div>
        </li>
        <!-- <li class="nav-item">
          <form class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
              <input class="form-control" type="text" placeholder="Tìm kiếm...">
              <span class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </li> -->
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#modalLogout">
            <i class="fa fa-fw fa-sign-out"></i>Đăng xuất</a>
        </li>
      </ul>
    </div>
  </nav>
<div class="content-wrapper">