<div class="container-fluid">
  <!-- Breadcrumbs-->
  <?php echo $breadcrumb; ?>
  <form action="" method="POST">
    <div class="row">
        <div class="col-xl-4 col-12">
          <h5><?php echo $header_page; ?></h5>
        </div>
        <div class="col-xl-4 col-12"><?php echo $this->session->flashdata('message'); ?></div>
        <div class="col-xl-4 col-12">
          <div class="button-crud text-right">
            <button type="submit" class="btn btn-success" name="btnSave" id="btnSave"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
          </div>
        </div>
    </div>
    <div class="row">
      <div class="col-xl-9 col-12">
        <div class="form-group">
          <label class="label" for="title_page">Tiêu đề</label>
          <input type="text" class="form-control" maxlength="150" name="title_page" id="title_page" autofocus required="required" value="<?php echo set_value('title_page'); ?>">
        </div>
        <div class="form-group">
          <label class="" for="short_page">Mô tả ngắn</label>
          <textarea class="form-control" maxlength="250" name="short_page" id="short_post"><?php echo set_value('short_page'); ?></textarea>
        </div>
        <div class="form-group">
          <label class="" for="full_page">Nội dung trang</label>
          <textarea class="form-control" name="full_page" id="full_post" rows="5"><?php echo set_value('full_page'); ?></textarea>
        </div>
        <div class="form-group">
          <label class="" for="keyword_page">Từ khóa SEO</label>
          <input type="text" class="form-control" maxlength="100" name="keyword_page" id="keyword_page" value="<?php echo set_value('keyword_page'); ?>">
        </div>
        <div class="form-group">
          <label class="" for="description_page">Mô tả SEO</label>
          <textarea class="form-control" maxlength="150" name="description_page" id="description_page"><?php echo set_value('description_page'); ?></textarea>
        </div>    
      </div>
      <!-- /END COL -->
      <div class="col-xl-3 col-12">
        <div class="form-group">
          <label class="label" for="public_page">Hiển thị</label>
          <select class="form-control" id="public_page" name="public_page">
            <option value="Y">Hiển thị</option>
            <option value="N">Ẩn</option>
          </select>
        </div>
        <div class="form-group">
          <label class="label" for="position_page">Vị trí</label>
          <select class="form-control" id="position_page" name="position_page">
            <option value="">Không có vị trí</option>
            <?php foreach($this->position as $key => $value){ ?>
            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label class="" for="sort_page">Thứ tự</label>
          <input type="number" class="form-control" name="sort_page" id="sort_page" value="0<?php echo set_value('sort_page'); ?>">
        </div>
        <script>
          function BrowseServer() {
            var finder = new CKFinder();
            finder.selectActionFunction = SetFileField;
            finder.popup();
          }
          function SetFileField(fileUrl) {
            var inputTag = '<input type="hidden" id="inputSThumbnail" name="thumbnail_page" value="">';
            $('#singleThumbnail').html('<div class="col-12"><img src="" class="img-fluid" id="Image">'+inputTag+'</div><div class="col-12"><a href="javascript:void(0)" onclick="removeImg();" id="RemoveImg">Xóa hình ảnh</a></div>');
            $('#Image').attr('src',fileUrl);
            $('#inputSThumbnail').val(fileUrl);
          }
          function removeImg()
          {
            $('#Image').attr('src','');
            $('#inputSThumbnail').val('');
            $('#RemoveImg').remove();
          }
        </script>
        <div class="card">
          <div class="card-header">
            <span class="btn">
              <i class="fa fa-picture-o"></i> Thêm hình ảnh
            </span>
            <span class="btn btn-primary fileinput-button pull-right" onclick="BrowseServer();">
                <i class="fa fa-plus"></i>
                <span>Thêm hình ảnh...</span>
            </span>
          </div>
          <div class="card-body">
            <div id="singleThumbnail"></div>
          </div>
        </div>
      </div>
      <!-- /END COL -->
    </div>
    <!-- /END ROW -->
    <div class="row">
        <div class="col-8"></div>
        <div class="col-4">
          <div class="button-crud text-right">
            <button type="submit" name="btnSave" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
          </div>
        </div>
    </div>
  </form>
</div>
<!-- /.container-fluid-->