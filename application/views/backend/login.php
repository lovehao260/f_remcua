<!DOCTYPE html>
<html lang="vi">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Admin - <?php echo $this->setting[0]; ?></title>
  <!-- Bootstrap core CSS-->
  <link href="/assets/admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <!-- Custom fonts for this template-->
  <link href="/assets/admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="/assets/admin/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="/assets/admin/css/sb-admin.css" rel="stylesheet" type="text/css">
</head>
<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Đăng nhập</div>
      <div class="card-body">
        <form action="<?php echo base_url('admin/login'); ?>" method="POST" id="formLogin" class="">
          <div class="form-group">
            <label for="exampleInputEmail1">Tên đăng nhập</label>
            <input class="form-control" id="tendangnhap" name="tendangnhap" type="text" >
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Mật khẩu</label>
            <input class="form-control" id="matkhau" name="matkhau" type="password">
          </div>
          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox"> Nhớ mật khẩu</label>
            </div>
          </div>
          <button type="submit" name="submit" class="btn btn-primary btn-block" onclick="login();">Đăng Nhập</button>
        </form>
        <div class="text-center">
          <a class="d-block small" href="">Quên mật khẩu?</a>
        </div>
      </div>
    </div>
    <div class="text-center text-primary">
      <?php echo validation_errors(); if(isset($message)){echo $message;} ?>
    </div>
  </div>
<!-- Bootstrap core JavaScript-->
<script type="text/javascript" src="/assets/admin/vendor/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/assets/admin/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assets/admin/js/sb-admin.js"></script>
</body>
</html>
