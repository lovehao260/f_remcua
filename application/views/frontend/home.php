<div class="container non-padding wrapper1 bg-grey">
	<?php $this->load->view('frontend/carousel'); ?>
	<div class="row block1">
		<div class="col-lg-12">
			<h3 class="text-uppercase text-center text-orange"><strong>Dự án mới nhất</strong></h3>
			<p class="text-center slogan2"><?php echo $this->setting[21]; ?></p>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 a1">
		<?php foreach($get_page_position_big as $item){ ?>
			<div class="project1 hvr-underline-from-center">
				<a href="<?php echo url_page($item['Title_Slug_Page'],$item['ID_Page']); ?>" class="">
					<img src="<?php echo thumbnail_post($item['Thumbnail_Page']); ?>" class="img-responsive">
					<div class="caption_project1">
						<p class="text-uppercase"><?php echo word_limiter($item['Title_Page'],15); ?></p>
					</div>
				</a>
			</div>
		<?php } ?>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 a2">
		<?php foreach($get_page_position as $item){ ?>
			<div class="col-sm-6 col-xs-6">
				<div class="project2 project3 hvr-underline-from-left">
					<a href="<?php echo url_page($item['Title_Slug_Page'],$item['ID_Page']); ?>">
						<img src="<?php echo thumbnail_post($item['Thumbnail_Page']); ?>" class="img-responsive">
						<div class="caption_project1">
							<p><?php echo word_limiter($item['Title_Page'],9); ?></p>
						</div>
					</a>
				</div>
			</div>
		<?php } ?>
		</div>
	</div>
</div>
<!-- /END CONTAINER Grey -->
<div class="container warapper2 bg-white">
	<div class="row block2">
		<div class="col-lg-12">
			<h3 class="text-center text-uppercase text-orange"><strong>Sản phẩm thông dụng</strong></h3>
		</div>
	</div>
    <div class="row">
        <div class="row block2-1">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <div class="controls controls-edit b1">
					<a class="left btn btn-default btn-radius-o" href="#carousel-products" data-slide="prev">
						<span class="glyphicon glyphicon-menu-left"></span>
					</a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            	<div class="text-center">
					<a href="<?php echo base_url('tat-ca-san-pham'); ?>" class="btn btn-default btn-radius text-uppercase">Xem tất cả</a>
				</div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <div class="controls controls-edit b2 text-right">
                    <a class="right btn btn-default btn-radius-o" href="#carousel-products" data-slide="next">
                    	<span class="glyphicon glyphicon-menu-right"></span>
                    </a>
                </div>
            </div>
        </div>
        <?php 
        	$product_01 = array_slice($get_product, 0,4);
        	$product_02 = array_slice($get_product, -4,4);
        ?>
        <div id="carousel-products" class="carousel carousel-edit slide" data-ride="carousel" data-interval="false">
            <div class="carousel-inner">
                <div class="item active">
                    <div class="row">
                    <?php
						foreach($product_01 as $item){
					?>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="col-item col-item-1">
                            	<a href="<?php echo url_product($item['Name_Slug_P'],$item['ID_P']); ?>">
	                                <div class="photo">
	                                    <img src="<?php echo thumbnail_product($item['Thumbnail_P']); ?>" class="img-responsive" alt="a" />
	                                </div>
	                                <div class="info">
	                                    <div class="">
	                                        <div class="title-text-color">
	                                            <h5><?php echo name_product($item['Name_P']); ?></h5>
	                                            <h5 class="price"> <?php echo price($item['Price_P'],$this->setting[13],$this->setting[14]); ?></h5>
	                                        </div>
	                                    </div>
	                                    <div class="clearfix">
	                                    </div>
	                                </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>    
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                    <?php
						foreach($product_02 as $item){
					?>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="col-item col-item-1">
                            	<a href="<?php echo url_product($item['Name_Slug_P'],$item['ID_P']); ?>">
	                                <div class="photo">
	                                    <img src="<?php echo thumbnail_product($item['Thumbnail_P']); ?>" class="img-responsive" alt="a" />
	                                </div>
	                                <div class="info">
	                                    <div class="">
	                                        <div class="title-text-color">
	                                            <h5><?php echo name_product($item['Name_P']); ?></h5>
	                                            <h5 class="price"> <?php echo price($item['Price_P'],$this->setting[13],$this->setting[14]); ?></h5>
	                                        </div>
	                                    </div>
	                                    <div class="clearfix">
	                                    </div>
	                                </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
</div>
<!-- /END CONTAINER White -->
<div class="container warapper2 bg-white">
	<div class="row block2">
		<div class="col-lg-12">
			<h3 class="text-center text-uppercase text-orange"><strong>Ý tưởng độc - lạ với giấy dán tường - </strong><strong class="text-primary">Năm <?php echo date('Y'); ?></strong></h3>
		</div>
	</div>
    <div class="row">
        <div class="row block2-1">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <div class="controls controls-edit b1">
					<a class="left btn btn-default btn-radius-o" href="#carousel-creative" data-slide="prev">
						<span class="glyphicon glyphicon-menu-left"></span>
					</a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            	<div class="text-center">
					<a href="<?php echo base_url('posts'); ?>" class="btn btn-default btn-radius text-uppercase">Xem tất cả</a>
				</div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <div class="controls controls-edit b2 text-right">
                    <a class="right btn btn-default btn-radius-o" href="#carousel-creative" data-slide="next">
                    	<span class="glyphicon glyphicon-menu-right"></span>
                    </a>
                </div>
            </div>
        </div>
        <?php
        	$get_post_home_01 = array_slice($get_post_home, 0,3);
        	$get_post_home_02 = array_slice($get_post_home, -3,3);
        ?>
        <div id="carousel-creative" class="carousel carousel-edit slide" data-ride="carousel" data-interval="false">
            <div class="carousel-inner">
                <div class="item active">
                    <div class="row">
                    <?php
						foreach($get_post_home_01 as $item){
					?>
                        <div class="col-sm-4">
                            <div class="col-item col-item-1 page-class">
                            	<a href="<?php echo url_post($item['Title_Slug_Post'],$item['ID_Post']); ?>" title="<?php echo $item['Title_Post']; ?>">
	                                <div class="photo">
	                                    <img src="<?php echo thumbnail_post($item['Thumbnail_Post']); ?>" class="img-responsive" alt="<?php echo $item['Title_Post']; ?>" />
	                                    <div class="content-hover">
                                            <h3><?php echo word_limiter($item['Title_Post'],15); ?></h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>    
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                    <?php
						foreach($get_post_home_02 as $item){
					?>
                        <div class="col-sm-4">
                            <div class="col-item col-item-1 page-class">
                            	<a href="<?php echo url_post($item['Title_Slug_Post'],$item['ID_Post']); ?>" title="<?php echo $item['Title_Post']; ?>">
	                                <div class="photo">
	                                    <img src="<?php echo thumbnail_post($item['Thumbnail_Post']); ?>" class="img-responsive" alt="<?php echo $item['Title_Post']; ?>" />
	                                    <div class="content-hover">
                                            <h3><?php echo word_limiter($item['Title_Post'],15); ?></h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
</div>
<!-- /END CONTAINER White -->
