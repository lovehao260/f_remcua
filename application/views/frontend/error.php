<div class="container wrapper1">
	<div class="row">
		<ol class="breadcrumb text-uppercase">
			<li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
			<li class="active">Trang lỗi</li>
		</ol>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="error-content">
				<h1>Opps...Không tìm thấy trang...</h1>
			</div>
		</div>
	</div>
</div>
<!-- /END CONTAINER Grey -->