<div class="container wrapper1">
	<div class="row">
		<ol class="breadcrumb text-uppercase">
			<li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
			<li class="active">Tìm kiếm</li>
		</ol>
	</div>
	<div class="row">
		<div class="categorys">
			<h3 class="title text-uppercase">Tìm kiếm sản phẩm với từ khóa: <?php if(isset($key)){echo $key;} ?></h3>
			<?php if($get_product != null){ foreach($get_product as $item){ ?>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <div class="col-item col-item-1">
                	<a href="<?php echo url_product($item['Name_Slug_P'],$item['ID_P']);?>" title="<?php echo $item['Name_P']; ?>">
                        <div class="photo">
                            <img src="<?php echo thumbnail_product($item['Thumbnail_P']); ?>" class="img-responsive" alt="<?php echo $item['Name_P']; ?>" />
                        </div>
                        <div class="info">
                            <div class="title-text-color">
                                <h5><?php echo name_product($item['Name_P']); ?></h5>
                                <h5 class="price"><?php echo price($item['Price_P'],$this->setting[13],$this->setting[14]); ?></h5>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <?php }}else{ ?>
            	<h3 class="text-center">Không tìm thấy dữ liệu</h3>
            <?php } ?>    
		</div>
		<div class="clearfix"></div>
		<nav aria-label="Page navigation" class="text-center">
			  	<?php if(isset($pagination)){echo $pagination;} ?>
		</nav>
	</div>
</div>