<div class="container wrapper1">
	<div class="row">
		<ol class="breadcrumb text-uppercase">
			<li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
			<li class=""><a href="<?php echo base_url('danh-muc-bai-viet/'.$detail_category['Name_Slug_CPost'].'-'.$detail_category['ID_CPost']); ?>"><?php echo $detail_category['Name_CPost']; ?></a></li>
			<li class="active"><?php echo $detail_post['Title_Post']; ?></li>
		</ol>
	</div>
	<div class="row">
		<div class="col-lg-3">
			<div class="panel panel-transparent">
				<div class="panel-heading" data-toggle="collapse" data-target="#listCategory">
					<h3 class="panel-title"><i class="fas fa-align-justify"></i> Danh mục bài viết</h3>
				</div>
				<div class="panel-body non-padding collapse in" id="listCategory">
				<?php foreach($category as $item){ ?>
					<a href="<?php echo base_url('danh-muc-bai-viet/'.$item['Name_Slug_CPost'].'-'.$item['ID_CPost']); ?>" title="<?php echo $item['Name_CPost']; ?>" class="list-group-item <?php if($item['ID_CPost'] == $detail_post['ID_Cat_Post']){echo "active";} ?>"><i class="fas fa-caret-right"></i> <?php echo $item['Name_CPost']; ?></a>
				<?php } ?>
				</div>
			</div>
			<?php $this->load->view('frontend/recent-posts'); ?>
			<?php $this->load->view('frontend/ads-post'); ?>
		</div>
		<div class="col-lg-9">
			<div class="title-page">
				<h3 class="text-black"><?php echo $detail_post['Title_Post']; ?></h3>
				<p class="text-author"><i class="fas fa-calendar-alt"></i> <?php echo date_post($detail_post['Createday_Post']); ?></p>
			</div>
			<div class="page-content">
			<!-- start content -->
				<?php echo $detail_post['Full_Content_Post']; ?>
			<!-- end content -->
			</div>
		</div>
	</div>
	<?php $this->load->view('frontend/related-post'); ?>
</div>