<div class="container wrapper1">
	<div class="row">
		<ol class="breadcrumb text-uppercase">
			<li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
			<li class="active">Liên hệ</li>
		</ol>
	</div>
	<div class="row">
		<div class="col-lg-12 text-center">
			<h3 class="text-uppercase text-orange title-contact">Liên hệ</h3>
		</div>
	</div>
	<div class="row">
		<!-- <div class="col-lg-12 text-center">
			<p class="content-contact">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel volutpat felis, eu condimentum massa. Pellentesque mollis eros vel mattis tempor.</p>
		</div> -->
	</div>
	<div class="row">
		<div class="col-lg-4 col-md-4">
			<div class="contact-info-area">
                <ul>
                    <li>
                        <div class="contact-icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="contact-address">
                            <h5>Điện thoại</h5>
                            <span> <?php echo $this->setting[4]; ?></span>
                        </div>
                    </li>
                    <li>
                        <div class="contact-icon">
                           <i class="far fa-envelope"></i>
                        </div>
                        <div class="contact-address">
                            <h5>Email</h5>
                            <span><a href="#"><?php echo $this->setting[5]; ?></a></span>
                        </div>
                    </li>
                    <li>
                        <div class="contact-icon">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="contact-address">
                            <h5>Địa chỉ</h5>
                            <span><?php echo $this->setting[6]; ?></span>
                        </div>
                    </li>
                </ul>
            </div>
		</div>
		<div class="col-lg-8 col-md-8">
			<div>
				<form action="<?php echo base_url('lien-he'); ?>" method="POST" id="formContact">
					<div class="form-group">
						<input type="text" class="form-control" name="hovaten" maxlength="40" placeholder="Họ và tên *">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="dienthoai" maxlength="13" placeholder="Điện thoại *">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="email" placeholder="Email">
					</div>
					<div class="form-group">
						<textarea class="form-control" rows="3" name="noidung" maxlength="500" placeholder="Nội dung *"></textarea>
					</div> 
					<div class="form-group">
						<button type="submit" name="send" class="btn btn-primary btn-lg">Gửi</button>
					</div>
					<div class="form-group text-center">
						<p class=""><?php if(isset($result)){echo $result;} ?></p>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="map">
		<div class="embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="<?php echo $this->setting[20]; ?>" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>
</div>