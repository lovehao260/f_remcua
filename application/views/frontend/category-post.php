<div class="container wrapper1">
	<div class="row">
		<ol class="breadcrumb text-uppercase">
			<li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
			<li class="active"><?php echo $detail_category['Name_CPost']; ?></li>
		</ol>
	</div>
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
			<div class="panel panel-transparent">
				<div class="panel-heading" data-toggle="collapse" data-target="#listCategory">
					<h3 class="panel-title"><i class="fas fa-align-justify"></i> Danh mục bài viết</h3>
				</div>
				<div class="panel-body non-padding collapse in" id="listCategory">
				<?php foreach($category as $item){ ?>
					<a href="<?php echo base_url('danh-muc-bai-viet/'.$item['Name_Slug_CPost'].'-'.$item['ID_CPost']); ?>" class="list-group-item <?php if($this->uri->segment(2) == $item['Name_Slug_CPost'].'-'.$item['ID_CPost']){echo "active";} ?>"><i class="fas fa-caret-right"></i> <?php echo $item['Name_CPost']; ?></a>
				<?php } ?>
				</div>
			</div>
			<?php $this->load->view('frontend/recent-posts'); ?>
			<?php $this->load->view('frontend/ads-post'); ?>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
			<div class="post-items">
				<h3 class="title text-uppercase text-orange"><?php echo $detail_category['Name_CPost']; ?></h3>
				<?php foreach($category_post as $item){ ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    <div class="col-item col-item-1 col-item-post">
                    	<a href="<?php echo url_post($item['Title_Slug_Post'],$item['ID_Post']); ?>" title="<?php echo $item['Title_Post']; ?>">
                            <div class="photo">
                                <img src="<?php echo thumbnail_post($item['Thumbnail_Post']); ?>" class="img-responsive" alt="<?php echo $item['Title_Post']; ?>">
                            </div>
                            <div class="info">
                                <div class="title-text-color text-center">
                                    <h4><?php echo $item['Title_Post']; ?></h4>
                                    <p class="text-author"><i class="fas fa-calendar-alt"></i> <?php echo date_post($item['Createday_Post']); ?> </p>
									<p class="text-short"><?php echo word_limiter($item['Short_Content_Post'],30); ?></p>
									<p><strong><a href="<?php echo url_post($item['Title_Slug_Post'],$item['ID_Post']); ?>" class="">Xem thêm <i class="fa fa-chevron-circle-right hvr-icon"></i></a></strong></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php } ?>    
			</div>
			<div class="clearfix"></div>
			<div class="pagination-01">
				<nav aria-label="Page navigation" class="text-center">
				  	<?php echo $pagination; ?>
				</nav>
			</div>
		</div>
	</div>
</div>