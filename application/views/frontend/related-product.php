<?php if(!empty($related_product)){ ?>
<div class="row">
	<div class="col-lg-12">
		<h3 class="title text-uppercase text-orange">Sản phẩm liên quan</h3>
		<div class="control-slide-product">
			<a class="left" href="#carousel-products" data-slide="prev">
				<span class="glyphicon glyphicon-menu-left"></span>
			</a>
            <a class="right" href="#carousel-products" data-slide="next">
            	<span class="glyphicon glyphicon-menu-right"></span>
            </a>
		</div>
	</div>
</div>
<div class="row">
	<div id="carousel-products" class="carousel carousel-edit slide" data-ride="carousel" data-interval="false">
        <div class="carousel-inner">
            <div class="item active">
                <div class="row">
                <?php
                if(!empty($related_product)){
                    $related_01 = array_slice($related_product, 0,4);
                    $related_02 = array_slice($related_product, -4,4, true);
                }else{
                    $related_01 = null;
                    $related_02 = null;
                }
					if(!empty($related_01)){ foreach($related_01 as $item){
				?>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="col-item col-item-1">
                        	<a href="<?php echo url_product($item['Name_Slug_P'],$item['ID_P']);?>" title="<?php echo $item['Name_P']; ?>">
                                <div class="photo">
                                    <img src="<?php echo thumbnail_product($item['Thumbnail_P']); ?>" class="img-responsive" alt="<?php echo $item['Name_P']; ?>" />
                                </div>
                                <div class="info">
                                    <div class="">
                                        <div class="title-text-color">
                                            <h5><?php echo $item['Name_P']; ?></h5>
                                            <h5 class="price"> <?php echo price($item['Price_P'],$this->setting[13],$this->setting[14]); ?></h5>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php }} ?>    
                </div>
            </div>
            <div class="item">
                <div class="row">
                <?php
					if(!empty($related_02)){ foreach($related_02 as $item){
				?>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="col-item col-item-1">
                            <a href="<?php echo url_product($item['Name_Slug_P'],$item['ID_P']);?>" title="<?php echo $item['Name_P']; ?>">
                                <div class="photo">
                                    <img src="<?php echo thumbnail_product($item['Thumbnail_P']); ?>" class="img-responsive" alt="<?php echo $item['Name_P']; ?>" />
                                </div>
                                <div class="info">
                                    <div class="">
                                        <div class="title-text-color">
                                            <h5><?php echo name_product($item['Name_P']); ?></h5>
                                            <h5 class="price"> <?php echo price($item['Price_P'],$this->setting[13],$this->setting[14]); ?></h5>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php }} ?>    
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>