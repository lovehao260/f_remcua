<div class="container warapper2 bg-grey">


    <div class="row block2">


        <div class="col-lg-12 text-center ">


            <h3 class="title-footer text-uppercase text-orange"><strong>Rèm Sài Gòn</strong></h3>


            <div class="flag">


                <img src="<?php echo base_url(); ?>assets/img/flag.png" width="45">


            </div>


        </div>


    </div>


    <div class="row">


        <div class="bottom-site">


            <div class="col-lg-121 a2">


                <?php $get_category_position = $this->LP_Product_FE->get_category_position('home_footer'); ?>



                <?php foreach ($get_category_position as $item) { ?>


                    <div class="col-sm-4 col-xs-6">


                        <div class="poro-item">


                            <a href="<?php echo url_category($item['Name_Slug_CatP'], $item['ID_CatP']); ?>">


                                <img src="<?php echo thumbnail_product($item['Thumbnail_CatP']); ?>"
                                     class="img-responsive">


                                <div class="poro-caption">


                                    <p class="text-center text-uppercase"><?php echo $item['Name_CatP']; ?></p>


                                </div>


                            </a>


                        </div>


                    </div>


                <?php } ?>


            </div>


        </div>


    </div>


    <div class="row bg-white">


        <!-- <div class="col-lg-3"></div> -->


        <div class="col-lg-12">


            <div class="subscribe text-center">


                <div class="subscribe-title">


                    <h3 class="text-uppercase">Đăng ký dịch vụ</h3>


                </div>


                <div class="content-subscribe">


                    <?php echo $this->setting[15]; ?>


                </div>


                <div class="form-subscribe">


                    <form action="#" method="POST" class="formSub" id="formSub">


                        <div class="form-group">


                            <div class="input-group">


                                <input type="email" name="email-sub" id="email-sub" required="required"
                                       class="form-control input-lg" placeholder="Email của bạn">


                                <span class="input-group-btn">



							        <button type="submit" id="btn-sub" onclick="add_sub();" name="btn-sub"
                                            class="btn btn-primary text-uppercase btn-lg">Đăng ký</button>



							    </span>


                            </div>


                        </div>


                        <div class="form-group">


                            <div class="text-center" id="result"></div>


                        </div>


                    </form>


                </div>


            </div>


        </div>


        <!-- <div class="col-lg-3"></div> -->


    </div>


</div>


<!-- /END CONTAINER White -->


<div class="container warapper3 bg-grey">


    <div class="row block3">


        <div class="footer-contact">


            <div class="col-lg-4 col-md-4 col-xs-12">


                <h3 class="text-uppercase text-orange title-contact">Thông tin liên hệ</h3>


                <h3 class=""><?php echo $this->setting[0]; ?></h3>


                <p><?php echo check_item('Địa chỉ: ', $this->setting[6]); ?></p>


                <p><?php echo check_item('Điện thoại: ', '<a href="tel:' . $this->setting[4] . '">' . $this->setting[4] . '</a>'); ?></p>


                <p><?php echo check_item('Email: ', '<a href="mailto:' . $this->setting[5] . '">' . $this->setting[5]) . '</a>'; ?></p>


                <div class="social-list">


                    <a title="Facebook" href="<?php echo $this->setting[9]; ?>" style="color:#3b579c" target="_blank"><i
                                class="fab fa-facebook fa-3x"></i></a>


                    <a title="Google Plus" href="<?php echo $this->setting[11]; ?>" style="color:#ce4439"
                       target="_blank"><i class="fab fa-google-plus-square fa-3x"></i></a>


                    <a title="Twitter" href="<?php echo $this->setting[12]; ?>" style="color:#00c4ff" target="_blank"><i
                                class="fab fa-twitter-square fa-3x"></i></a>


                    <a title="Youtube" href="<?php echo $this->setting[10]; ?>" style="color:#f00" target="_blank"><i
                                class="fab fa-youtube fa-3x"></i></a>


                </div>


                <div class="certification-social">


                    <div class="row">


                        <div class="col-xs-6">


                            <a href="">


                                <img src="<?php echo base_url(); ?>assets/img/20150827110756-dathongbao.png" width="150"
                                     class="img-responsive">


                            </a>


                        </div>


                        <div class="col-xs-6">


                            <a href="">


                                <img src="<?php echo base_url(); ?>assets/img/20150827110756-dadangky.png" width="150"
                                     class="img-responsive">


                            </a>


                        </div>


                    </div>


                </div>


            </div>


            <div class="col-lg-4 col-md-4 col-xs-12">


                <div class="title-galley">


                    <h3 class="text-uppercase text-orange title-galley-text">Facebook</h3>


                </div>


                <div class="conten-galley">

                    <div id="fb-root"></div>


                    <script>

                        window.fbAsyncInit = function () {

                            FB.init({

                                xfbml: true,

                                version: 'v3.2'

                            });

                        };


                        (function (d, s, id) {

                            var js, fjs = d.getElementsByTagName(s)[0];

                            if (d.getElementById(id)) return;

                            js = d.createElement(s);
                            js.id = id;

                            js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';

                            fjs.parentNode.insertBefore(js, fjs);

                        }(document, 'script', 'facebook-jssdk'));</script>


                    <div class="fb-page" data-href="<?php echo $this->setting[9]; ?>" data-tabs=""
                         data-small-header="false" data-adapt-container-width="true" data-hide-cover="false"
                         data-show-facepile="true">


                        <blockquote cite="<?php echo $this->setting[9]; ?>" class="fb-xfbml-parse-ignore">


                            <a href="<?php echo $this->setting[9]; ?>"><?php echo $this->setting[0]; ?></a>


                        </blockquote>


                    </div>


                </div>


            </div>


            <div class="col-lg-4 col-md-4 col-xs-12">


                <div class="title-galley">


                    <h3 class="text-uppercase text-orange title-galley-text">Bản đồ dẫn đường</h3>


                </div>


                <div class="conten-galley">


                    <div class="embed-responsive embed-responsive-16by9">


                        <iframe class="embed-responsive-item" src="<?php echo $this->setting[20]; ?>" width="600"
                                height="450" frameborder="0" style="border:0" allowfullscreen></iframe>


                    </div>


                </div>


            </div>


        </div>


    </div>


</div>


<!-- /END CONTAINER White -->


<div class="modal fade" id="formSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">


    <div class="modal-dialog modal-lg" role="document">


        <div class="modal-content">


            <div class="modal-body">


                <form class="formSearch" action="<?php echo base_url('tim-kiem'); ?>" method="GET"
                      onsubmit="this.submit();" id="formSearch">


                    <div class="form-group">


                        <input type="text" name="tu-khoa" class="form-control input-lg" required="required"
                               placeholder="Tìm kiếm">


                    </div>


                    <button type="button" id="sendSearch" onclick="submit();" class="btn btn-danger">Tìm kiếm</button>


                </form>


            </div>


        </div>


    </div>


</div>


<div class="modal fade" id="showLagreThumb" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">


    <div class="modal-dialog modal-lg" role="document">


        <div class="modal-content">


            <div class="modal-header">


                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>


                <h4 class="modal-title" id="title-p-modal">Modal title</h4>


            </div>


            <div class="modal-body text-center">


                <img src="" class="img-responsive" id="modal_image_thumb">


            </div>


        </div>


    </div>


</div>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>


<script type="text/javascript" src="/assets/validation/jquery.validate.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pace.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.elevateZoom-3.0.8.min.js"></script>


<script type="text/javascript" src="/assets/js/jquery.priceformat.min.js"></script>


<script type="text/javascript" src="/assets/js/site_responsive.js"></script>


<script type="text/javascript" src="/assets/js/site.js"></script>


<!--Start of Tawk.to Script-->


<script type="text/javascript">


    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();


    (function () {


        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];


        s1.async = true;


        s1.src = 'https://embed.tawk.to/<?php echo $this->setting[22]; ?>/default';


        s1.charset = 'UTF-8';


        s1.setAttribute('crossorigin', '*');


        s0.parentNode.insertBefore(s1, s0);


    })();


</script>


<!--End of Tawk.to Script-->


<!-- Load Facebook SDK for JavaScript -->


<div id="fb-root"></div>

<!-- Your customer chat code -->


<div class="fb-customerchat"

     attribution=setup_tool

     page_id="239265983291829">

</div>


<!-- Global site tag (gtag.js) - Google Analytics -->


<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $this->setting[16]; ?>"></script>


<script>


    window.dataLayer = window.dataLayer || [];


    function gtag() {
        dataLayer.push(arguments);
    }


    gtag('js', new Date());


    gtag('config', '<?php echo $this->setting[16]; ?>');


</script>
<script type="text/javascript">
    // $(document).ready(function() {
    // 	$('#tinhtoan').on('click',function(event){
    // 		event.preventDefault();
    // 		var chieucao = $('#chieucao').val();
    // 		var chieurong = $('#chieurong').val();
    // 		var dongia = $('#don_gia_met_vuong').val();
    // 		var tinhtoan = (chieucao * chieurong);
    // 		$('#ketquaTinhMetVuong').html(dongia);
    // 	});
    // });
</script>


</body>


</html>
