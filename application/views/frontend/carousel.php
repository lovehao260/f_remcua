<div class="row">
	<div class="col-lg-12">
		<div id="carousel-remcuathanhtung" class="carousel slide" data-ride="carousel">
		  	<ol class="carousel-indicators">
		  	<?php $i = 0; foreach($carousel as $item){ ?>
			    <li data-target="#carousel-remcuathanhtung" data-slide-to="<?php echo $i++; ?>" class="<?php if($min_carousel['Sort_Slide'] == $item['Sort_Slide']){echo 'active';} ?>"></li>
			<?php } ?>
		  	</ol>

		  	<div class="carousel-inner" role="listbox">
		  	<?php foreach($carousel as $item){  ?>
			    <div class="item <?php if($min_carousel['Sort_Slide'] == $item['Sort_Slide']){echo 'active';} ?>">
			      	<img src="<?php echo $item['Image_Slide']; ?>" alt="<?php echo $item['Caption_Slide']; ?>">
			      	<div class="carousel-caption">
			      	<?php if($item['Caption_Slide'] != null){ ?>
			      		<h3><?php echo $item['Caption_Slide']; ?></h3>
			      	<?php } if($item['Description_Slide'] != null){ ?>
			      		<p><?php echo $item['Description_Slide']; ?></p>
			      	<?php } ?>
			      	</div>
			    </div>
			<?php }?>    
		  	</div>
		  	<style type="text/css">
		  		.jssora051 {display:block;position:absolute;cursor:pointer;}
				.jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
				.jssora051:hover {opacity:.8;}
				.jssora051.jssora051dn {opacity:.5;}
				.jssora051.jssora051ds {opacity:.3;pointer-events:none;}
		  	</style>
		  	<a class="left carousel-control" href="#carousel-remcuathanhtung" role="button" data-slide="prev">
		  	<div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:40%;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                </svg>
            </div>
		    <span class="sr-only">Previous</span>
		  	</a>
		  	<a class="right carousel-control" href="#carousel-remcuathanhtung" role="button" data-slide="next">
            <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:40%;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                </svg>
            </div>
		    <span class="sr-only">Next</span>
		  	</a>
		</div>
	</div>
</div>
<!-- /END CAROUSEL -->