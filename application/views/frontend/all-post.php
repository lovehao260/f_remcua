<div class="container wrapper1">
	<div class="row">
		<ol class="breadcrumb text-uppercase">
			<li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
			<li class="active">Danh mục bài viết</li>
		</ol>
	</div>
	<div class="row">
		<div class="col-lg-12 a1 a2">
			<div class="categorys">
				<h3 class="title text-uppercase text-orange">Tất cả bài viết</h3>
				<?php foreach($get_post as $item){ ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="col-item col-item-1 col-item-post">
                    	<a href="<?php echo url_post($item['Title_Slug_Post'],$item['ID_Post']); ?>" title="<?php echo $item['Title_Post']; ?>">
                            <div class="photo">
                                <img src="<?php echo thumbnail_post($item['Thumbnail_Post']); ?>" class="img-responsive" alt="<?php echo $item['Title_Post']; ?>">
                            </div>
                            <div class="info">
                                <div class="title-text-color text-center">
                                    <h4><?php echo $item['Title_Post']; ?></h4>
                                    <p class="text-author"><i class="fas fa-calendar-alt"></i> <?php echo date_post($item['Createday_Post']); ?></p>
									<p class="text-short"><?php echo word_limiter($item['Short_Content_Post'],30); ?></p>
									<p><strong><a href="<?php echo url_post($item['Title_Slug_Post'],$item['ID_Post']); ?>" class="">Xem thêm <i class="fa fa-chevron-circle-right hvr-icon"></i></a></strong></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php } ?>    
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<nav aria-label="Page navigation" class="text-center">
			  	<?php echo $pagination; ?>
			</nav>
		</div>
	</div>
</div>