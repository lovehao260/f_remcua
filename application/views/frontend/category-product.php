<div class="container wrapper1">
	<div class="row">
	<ol class="breadcrumb text-uppercase">
		<li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
		<?php if($this->uri->segment(1) != 'tat-ca-san-pham'){ ?>
		<li><a href="<?php echo base_url('tat-ca-san-pham'); ?>">Tất cả sản phẩm</a></li>
		<?php }?>
		<li class="active">
			<?php
				if(isset($name_category['Name_CatP'])){
					echo $name_category['Name_CatP'];
				}elseif(isset($name_color['Name_Color'])){
					echo "Bộ lọc màu: ".$name_color['Name_Color'];
				}elseif(!isset($name_category['Name_CatP'])){
					echo "Tất cả sản phẩm";
				} 
			?>
		</li>
	</ol>
	</div>
	<div class="row">
		<div class="col-lg-3 col-md-3 col-xs-12 a2">
			<form action="<?php echo base_url('tim-kiem'); ?>" method="GET" onsubmit="this.submit();">
				<div class="form-group">
					<div class="input-group">
						<input type="text" class="form-control" name="tu-khoa" placeholder="Tìm kiếm sản phẩm" required="required" autofocus>
						<span class="input-group-btn">
							<button type="button" onclick="submit();" class="btn btn-default"><i class="fa fa-search"></i></button>
						</span>
					</div>
				</div>
			</form>
			<div class="panel panel-transparent">
				<div class="panel-heading cursor" data-toggle="collapse" data-target="#listCategory">
					<h3 class="panel-title"><i class="fas fa-align-justify"></i> Danh mục sản phẩm</h3>
				</div>
				<div class="panel-body non-padding collapse in" id="listCategory">
				<?php if($category != null){ foreach($category as $item){ ?>
					<a href="<?php echo url_category($item['Name_Slug_CatP'],$item['ID_CatP']); ?>" class="list-group-item <?php if($this->uri->segment(2) == $item['Name_Slug_CatP'].'-'.$item['ID_CatP']){echo 'active';} ?>" title="<?php echo $item['Name_CatP']; ?>" title="<?php echo $item['Name_CatP']; ?>"><i class="fas fa-caret-right"></i> <?php echo $item['Name_CatP']; ?></a>
				<?php }}else{ ?>
					<a href="#" class="list-group-item text-center">Không có dữ liệu</a>
				<?php } ?>
				</div>
			</div>
			<div class="panel panel-transparent">
				<div class="panel-heading cursor" data-toggle="collapse" data-target="#listColor">
					<h3 class="panel-title"><i class="fas fa-palette"></i> Màu sắc</h3>
				</div>
				<div class="panel-body non-padding collapse in" id="listColor">
				<?php if($color != null){ foreach($color as $item){ ?>
					<a href="<?php echo url_color($item['Name_Slug_Color'],$item['ID_Color']); ?>" title="<?php echo $item['Name_Color']; ?>" class="list-group-item <?php if($this->uri->segment(3) == $item['Name_Slug_Color'].'-'.$item['ID_Color']){echo 'active';} ?>" title="<?php echo $item['Name_Color']; ?>"><span class="label" style="background-color:<?php echo $item['Code_Color']; ?>">&nbsp;&nbsp;</span> <?php echo $item['Name_Color']; ?> </a>
				<?php }}else{ ?>
					<a href="#" class="list-group-item text-center">Không có dữ liệu</a>
				<?php } ?>
				</div>
			</div>
			<?php $this->load->view('frontend/ads-cat-product'); ?>
		</div>
		<div class="col-lg-9 col-md-9 col-xs-12 a1">
			<?php $this->load->view('frontend/ads-cat-product-top'); ?>
			<div class="categorys">
				<h3 class="title text-uppercase">
					<?php
						if(isset($name_category['Name_CatP'])){
							echo $name_category['Name_CatP'];
						}elseif(isset($name_color['Name_Color'])){
							echo "Bộ lọc màu: ".$name_color['Name_Color'];
						}elseif(!isset($name_category['Name_CatP'])){
							echo "Tất cả sản phẩm";
						} 
					?>
				</h3>
				<?php if($get_product != null){ foreach($get_product as $item){ ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="col-item col-item-1">
                    	<a href="<?php echo url_product($item['Name_Slug_P'],$item['ID_P']);?>" title="<?php echo $item['Name_P']; ?>">
                            <div class="photo">
                                <img src="<?php echo thumbnail_product($item['Thumbnail_P']); ?>" class="img-responsive" alt="<?php echo $item['Name_P']; ?>" />
                            </div>
                            <div class="info">
                                <div class="title-text-color">
                                    <h5><?php echo name_product($item['Name_P']); ?></h5>
                                    <h5 class="price"><?php echo price($item['Price_P'],$this->setting[13],$this->setting[14]); ?></h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php }}else{ ?>
                <div class="col-xs-12">
	                <h3 class="text-center">Không có sản phẩm</h3>
                </div>
                <?php } ?>    
			</div>
			<div class="clearfix"></div>
			<nav aria-label="Page navigation" class="text-center">
			  	<?php echo $pagination; ?>
			</nav>
		</div>
	</div>
</div>
<!-- /END CONTAINER Grey -->