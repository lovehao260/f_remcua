<div class="container wrapper1">



	<div class="row">

		<ol class="breadcrumb text-uppercase">

			<li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>

			<?php if($this->uri->segment(1) != 'tat-ca-san-pham'){ ?>

			<li><a href="<?php echo base_url('tat-ca-san-pham'); ?>">Tất cả sản phẩm</a></li>

			<?php }?>

			<li class=""><a href="<?php echo url_category($name_category['Name_Slug_CatP'],$name_category['ID_CatP']); ?>"><?php echo $name_category['Name_CatP']; ?></a></li>

			<li class="active"><?php echo $detail_product['Name_P']; ?></li>

		</ol>

	</div>

	<div class="detail-product-image">

	<?php

		if (isset($detail_product)) {

	?>

		<div class="row">

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 a2">

				<div class="image-product" id="#image-product">

					<img src="<?php echo thumbnail_product($detail_product['Thumbnail_P']); ?>" class="img-responsive" id="showThumbnail" alt="<?php echo $detail_product['Name_P']; ?>">

				</div>

				<?php if(isset($get_thumbnail) && $get_thumbnail != null){ ?>

				<div class="list-thumbnail-product">

					<div class="carousel slide text-center" id="thumbnail-product">

						<div class="carousel-inner">

						<?php

							$thumbnail_01 = array_slice($get_thumbnail, 0,4);

							$thumbnail_02 = array_slice($get_thumbnail, -1,4,true);	

						?>

							<div class="item active">

								<?php foreach($thumbnail_01 as $item){ ?>

									<div class="thumbnail-product-item" data-src="<?php echo thumbnail_product($item['Thumbnail_link_Tn']); ?>">

										<img src="<?php echo thumbnail_product($item['Thumbnail_link_Tn']); ?>" width="133" height="133" alt="<?php echo $detail_product['Name_P']; ?>">

									</div>

								<?php } ?>

							</div>

							<div class="item">

								<?php foreach($thumbnail_02 as $item){ ?>

									<div class="thumbnail-product-item" data-src="<?php echo thumbnail_product($item['Thumbnail_link_Tn']); ?>">

										<img src="<?php echo thumbnail_product($item['Thumbnail_link_Tn']); ?>" width="133" height="133" alt="<?php echo $detail_product['Name_P']; ?>">

									</div>

								<?php } ?>

							</div>

						</div>

						<a class="left carousel-control carousel-detail-product" href="#thumbnail-product" data-slide="prev">

							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>

						</a>



		                <a class="right carousel-control carousel-detail-product" href="#thumbnail-product" data-slide="next">

		                	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>

		                </a>

					</div>

				</div>

				<?php } ?>

			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 a1">

				<div class="title-product">

					<h1 class="title-p" id="title-p"><?php echo $detail_product['Name_P']; ?></h1>

					<h3 class="price-detail"><?php echo price($detail_product['Price_P'],$this->setting[13],$this->setting[14]); ?></h3>

					<div class="short-content">

						<?php echo $detail_product['Short_Content_P']; ?>

					</div>

				</div>

				<div class="info-product">

					<p class="code-product"><?php echo check_item('Mã sản phẩm: ',$detail_product['Code_P']); ?> </p>

					<p><?php echo check_item('Tình trạng: ',check_product($detail_product['Status_P'])); ?></p>

					<?php if($get_color != null){ ?>

					<div class="list-color">

						<p class="color-title">Màu sắc:

						<?php foreach($get_color as $item){ ?>

							<a href="<?php echo url_color($item['Name_Slug_Color'],$item['ID_Color']); ?>" title="<?php echo $item['Name_Color']; ?>"><span class="label" style="background-color:<?php echo $item['Code_Color']; ?>">&nbsp;&nbsp;</span></a>

						<?php } ?>

						</p>

					</div>

					<?php } ?>

					<div class="clearfix"></div>

					<p>Danh mục: <a href="<?php echo url_category($name_category['Name_Slug_CatP'],$name_category['ID_CatP']); ?>" title="<?php echo $name_category['Name_CatP'];?>"><?php echo $name_category['Name_CatP'];?></a></p>

					<?php if($get_tag != null){ ?>

					<p>Tags: 

						<?php foreach($get_tag as $item){ ?>

							<a href="<?php echo base_url('tag/'.$item['Name_Slug_Tag'].'-'.$item['ID_Tag']); ?>" class="tags-product" title="<?php echo $item['Name_Tag']; ?>"><span class="label label-primary"><?php echo $item['Name_Tag']; ?></span></a> 

						<?php } ?>

					</p>

					<?php } ?>

				</div>

				<!-- <div class="contact-product">

					<h3><?php echo check_item('Liên hệ: ', $this->setting[4]); ?></h3>

				</div> -->

				<div class="calculator-product">

					<button type="button" class="btn btn-danger btn-lg" data-toggle="collapse" data-target="#tinhtoangia"><i class="fa fa-calculator" aria-hidden="true"></i> Tính giá rèm</button>

					<a href="tel:<?php echo $this->setting[4]; ?>" class="btn btn-default btn-lg"><?php echo check_item('Liên hệ ', $this->setting[4]); ?></a>

				</div>

				<div class="calculator-product well collapse" id="tinhtoangia">

					<ul class="nav nav-tabs" role="tablist">

					    <li role="presentation" class="active"><a href="#metvuong" aria-controls="metvuong" role="tab" data-toggle="tab">Tính mét vuông</a></li>

					    <li role="presentation"><a href="#mettoi" aria-controls="mettoi" role="tab" data-toggle="tab">Tính mét tới</a></li>

				  	</ul>



				  <!-- Tab panes -->

				  	<div class="tab-content">

					    <div role="tabpanel" class="tab-pane active" id="metvuong">

						    <form action="" method="POST" id="formMetVuong" autocomplete="off">

						    	<div class="form-group">

						    		<label for="chieurong">Chiều rộng (mét)</label>

						    		<input type="number" class="form-control" name="chieurong" id="chieurong" placeholder="Nhập chiều rộng (mét)...">

						    	</div>

						    	<div class="form-group">

						    		<label for="chieucao">Chiều cao (mét)</label>

						    		<input type="number" class="form-control" name="chieucao" id="chieucao" placeholder="Nhập chiều cao (mét)...">

						    	</div>

						    	<div class="form-group">

						    		<label for="don_gia_met_vuong">Đơn giá mét vuông (đ)</label>

						    		<input type="text" class="form-control" name="dongia" id="don_gia_met_vuong" placeholder="Nhập đơn giá" value="<?php echo $detail_product['Price_P']; ?>">

						    		<input type="hidden" name="gia_thuc" id="gia_thuc" value="<?php echo $detail_product['Price_P']; ?>">

						    	</div>

						    	<div class="form-group">

						    		<button type="button" id="tinhtoan" onclick="tinh_met_vuong();" class="btn btn-info">Tính toán</button>

						    	</div>

						    	<div class="form-group" id="ketquaTinhMetVuong">

						    	</div>
						    	
						    </form>

					    </div>

					    <div role="tabpanel" class="tab-pane" id="mettoi">

					    	<form action="" method="POST" id="formMetToi" autocomplete="off">

						    	<div class="form-group">

						    		<label for="chieurong_toi">Chiều rộng (mét)</label>

						    		<input type="number" class="form-control" name="chieurong" id="chieurong_toi" placeholder="Nhập chiều rộng (mét)...">

						    	</div>

						    	<div class="form-group">

						    		<label for="dongia_toi">Đơn giá mét tới (đ)</label>

						    		<input type="text" class="form-control" name="dongia" id="dongia_toi" placeholder="Nhập đơn giá" value="<?php echo $detail_product['Price_2_P']; ?>">

						    		<input type="hidden" name="gia_thuc" id="gia_thuc_toi" value="<?php echo $detail_product['Price_2_P']; ?>">

						    	</div>

						    	<div class="form-group">

						    		<button type="button" onclick="tinh_met_toi();" class="btn btn-info">Tính toán</button>

						    	</div>

						    	<div class="form-group" id="ketquaTinhMetToi">

						    	</div>

						    </form>

					    </div>

				  	</div>

				</div>

			</div>

		</div>

		<div class="row">

			<div class="col-lg-12 a1 a2">

				<div class="full-content">

				  	<ul class="nav nav-tabs" role="tablist">

				    <li role="presentation" class="active"><a href="#mota" aria-controls="mota" role="tab" data-toggle="tab">Mô tả</a></li>

				    <li role="presentation"><a href="#binhluan" aria-controls="binhluan" role="tab" data-toggle="tab">Bình luận (<span class="fb-comments-count" data-href="<?php echo base_url('/san-pham/'.$detail_product['Name_Slug_P'].'-'.$detail_product['ID_P']); ?>"></span>)</a></li>

				  	</ul>



				  	<div class="tab-content">

					    <div role="tabpanel" class="tab-pane active" id="mota">

					    	<?php echo $detail_product['Full_Content_P'];?>

					    </div>

					    <div role="tabpanel" class="tab-pane" id="binhluan">

						    <div class="fb-comments" data-href="<?php echo base_url('/san-pham/'.$detail_product['Name_Slug_P'].'-'.$detail_product['ID_P']); ?>" data-numposts="5" data-width="100%"></div>

					    </div>

				  	</div>

				</div>

			</div>

		</div>

	<?php }else{ echo "<h3 class='text-center'>Sản phẩm không tồn tại</h3>";} ?>

		<?php $this->load->view('frontend/related-product'); ?>

	</div>

</div>

<!-- /END CONTAINER Grey -->