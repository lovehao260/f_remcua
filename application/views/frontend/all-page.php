<div class="container wrapper1">
	<div class="row">
		<ol class="breadcrumb text-uppercase">
			<li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
			<li class="active">Tất cả các trang</li>
		</ol>
	</div>
	<div class="row">
		<div class="col-lg-12 a1 a2">
			<div class="categorys">
				<h3 class="title text-uppercase text-orange">Tất cả các trang</h3>
				<?php foreach($get_page as $item){ ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    <div class="col-item col-item-1 col-item-page">
                    	<a href="<?php echo url_page($item['Title_Slug_Page'],$item['ID_Page']); ?>" title="<?php echo $item['Title_Page']; ?>">
                            <div class="photo">
                                <img src="<?php echo thumbnail_post($item['Thumbnail_Page']); ?>" class="img-responsive" alt="<?php echo $item['Title_Page']; ?>">
                            </div>
                            <div class="info">
                                <div class="title-text-color text-center">
                                    <h4><?php echo $item['Title_Page']; ?></h4>
                                    
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php } ?>    
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<nav aria-label="Page navigation" class="text-center">
			  	<?php echo $pagination; ?>
			</nav>
		</div>
	</div>
</div>