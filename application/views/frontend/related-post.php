<div class="row">
	<div class="col-lg-12">
		<h3 class="title text-uppercase text-orange">Bài viết liên quan</h3>
		<div class="control-slide-product">
			<a class="left" href="#carousel-post" data-slide="prev">
				<span class="glyphicon glyphicon-menu-left"></span>
			</a>
            <a class="right" href="#carousel-post" data-slide="next">
            	<span class="glyphicon glyphicon-menu-right"></span>
            </a>
		</div>
	</div>
</div>
<div class="row">
	<div id="carousel-post" class="carousel carousel-edit slide" data-ride="carousel" data-interval="false">
        <div class="carousel-inner">
        <?php  
            $related_01 = array_slice($related,0,4);
            $related_02 = array_slice($related,-4,4,true);
        ?>
            <div class="item active">
                <div class="row">
                    <?php foreach($related_01 as $item){ ?>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <div class="col-item col-item-1 col-item-post">
                        <a href="<?php echo url_post($item['Title_Slug_Post'],$item['ID_Post']); ?>" title="<?php echo $item['Title_Post']; ?>">
                            <div class="photo">
                                <img src="<?php echo thumbnail_post($item['Thumbnail_Post']); ?>" class="img-responsive" alt="<?php echo $item['Title_Post']; ?>">
                            </div>
                            <div class="info">
                                <div class="title-text-color text-center">
                                    <h4><?php echo $item['Title_Post']; ?></h4>
                                    <p class="text-author"><i class="fas fa-calendar-alt"></i> <?php echo date_post($item['Createday_Post']); ?></p>
                                    <p class="text-short"><?php echo word_limiter($item['Short_Content_Post'],30); ?></p>
                                    <p><strong><a href="<?php echo url_post($item['Title_Slug_Post'],$item['ID_Post']); ?>" class="">Xem thêm <i class="fa fa-chevron-circle-right hvr-icon"></i></a></strong></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    </div>
                    <?php } ?>       
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <?php foreach($related_02 as $item){  ?>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <div class="col-item col-item-1 col-item-post">
                        <a href="<?php echo url_post($item['Title_Slug_Post'],$item['ID_Post']); ?>" title="<?php echo $item['Title_Post']; ?>">
                            <div class="photo">
                                <img src="<?php echo thumbnail_post($item['Thumbnail_Post']); ?>" class="img-responsive" alt="<?php echo $item['Title_Post']; ?>">
                            </div>
                            <div class="info">
                                <div class="title-text-color text-center">
                                    <h4><?php echo $item['Title_Post']; ?></h4>
                                    <p class="text-author"><i class="fas fa-calendar-alt"></i> <?php echo date_post($item['Createday_Post']); ?></p>
                                    <p class="text-short"><?php echo word_limiter($item['Short_Content_Post'],30); ?></p>
                                    <p><strong><a href="<?php echo url_post($item['Title_Slug_Post'],$item['ID_Post']); ?>" class="">Xem thêm <i class="fa fa-chevron-circle-right hvr-icon"></i></a></strong></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    </div>
                    <?php } ?>       
                </div>
            </div>
        </div>
    </div>
</div>