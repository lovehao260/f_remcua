<div class="container wrapper1">
	<div class="row">
		<ol class="breadcrumb text-uppercase">
			<li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
			<li class="active"><?php echo $detail_page['Title_Page']; ?></li>
		</ol>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="title-page">
				<h3 class="text-uppercase"><?php echo $detail_page['Title_Page']; ?></h3>
				<p class="text-author"><i class="fas fa-calendar-alt"></i> <?php echo date_post($detail_page['Modify_Page']); ?></p>
			</div>
			<div class="page-content">
				<?php echo $detail_page['Full_Content_Page']; ?>
			</div>
		</div>
	</div>
</div>
<!-- /END CONTAINER Grey -->