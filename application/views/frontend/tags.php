<div class="container wrapper1">
	<div class="row">
		<ol class="breadcrumb text-uppercase">
			<li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
			<li class="active">Tag: <?php echo $get_detail_tags['Name_Tag']; ?></li>
		</ol>
	</div>
	<div class="row">
		<div class="col-lg-3 a2">
			<div class="panel panel-transparent">
				<div class="panel-heading" data-toggle="collapse" data-target="#listTags">
					<h3 class="panel-title"><i class="fas fa-tags"></i> Tags</h3>
				</div>
				<div class="panel-body collapse in" id="listTags">
				<?php foreach($get_tags as $item){ ?>
					<a href="<?php echo base_url('tag/'.$item['Name_Slug_Tag'].'-'.$item['ID_Tag']); ?>" style="font-size: <?php echo(rand(13,30)); ?>px"><?php echo $item['Name_Tag']; ?>,</a>
				<?php } ?>
				</div>
			</div>
		</div>
		<div class="col-lg-9 a1">
			<div class="categorys">
				<h3 class="title text-uppercase text-orange">Tag: <?php echo $get_detail_tags['Name_Tag']; ?></h3>
				<?php foreach($get_product_tag as $item){ ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="col-item col-item-1">
                    	<a href="<?php echo url_product($item['Name_Slug_P'],$item['ID_P']);?>" title="<?php echo $item['Name_P']; ?>">
                            <div class="photo">
                                <img src="<?php echo thumbnail_product($item['Thumbnail_P']); ?>" class="img-responsive" alt="<?php echo $item['Name_P']; ?>" />
                            </div>
                            <div class="info">
                                <div class="title-text-color">
                                    <h5><?php echo name_product($item['Name_P']); ?></h5>
                                    <h5 class="price"><?php echo price($item['Price_P'],$this->setting[13],$this->setting[14]); ?></h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php } ?>   
			</div>
			<div class="clearfix"></div>
			<nav aria-label="Page navigation" class="text-center">
				<?php echo $pagination; ?>
			</nav>
		</div>
	</div>
</div>
<!-- /END CONTAINER Grey -->