<!DOCTYPE html>
<html lang="vi">
<head>
	<meta charset="utf-8">
	<meta name="keyword" content="<?php echo $meta_keywords; ?>">
	<meta name="description" content="<?php echo $meta_description; ?>">
	<meta name="author" content="<?php echo $meta_author; ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="alternate" href="<?php echo base_url(); ?>" hreflang="vi-vn" />
	<title><?php echo $meta_title; ?></title>
	<link rel="icon" href="<?php echo base_url($this->setting[24]); ?>" type="image/x-icon"/>
	<link rel="shortcut icon" href="<?php echo base_url($this->setting[24]); ?>" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css?ver=<?php echo time(); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/flatit.css?ver=<?php echo time(); ?>" />
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fontawesome/css/font-awesome.min.css?ver=<?php echo time(); ?>" /> -->
	<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css?ver=1537367291" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/hover-min.css?ver=<?php echo time(); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style_backup_cam.css?ver=<?php echo time(); ?>" />
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="logo text-center">
				<a class="" href="<?php echo base_url(); ?>" title="<?php echo $this->setting[0]; ?>">
			      	<img src="<?php echo base_url($this->setting[7]); ?>" class="img-responsive" >
			    </a>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="search-top">
				<form class="formSearch" action="<?php echo base_url('tim-kiem'); ?>" method="GET" onsubmit="this.submit();" id="formSearch">
			        <div class="input-group">
				      	<input type="text" name="tu-khoa" class="form-control" required="required" placeholder="Nhập từ khóa tìm kiếm...">
				      	<span class="input-group-btn">
				        	<button type="button" id="sendSearch" onclick="submit();" class="btn btn-orange"><i class="fa fa-search"></i> Tìm kiếm</button>
				      	</span>
				    </div>
			    </form>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="hotline text-center">
				<p class="text-hotline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hỗ trợ trực tuyến</p>
				<h3 class="phone-hotline text-red"><i class="fas fa-phone"></i> <?php echo $this->setting[4]; ?></h3>
			</div>
		</div>
	</div>
	<div class="row">
		<nav class="navbar navbar-default" data-spy="affix" data-offset-top="190">
		  	<div class="container-fluid navbar-nonpadding">
			    <div class="navbar-header">
			      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      	</button>
			    </div>
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				    <?php echo $this->multi_menu->render(array(
				        'nav_tag_open'        => '<ul class="nav navbar-nav">',            
				        'parentl1_tag_open'   => '<li class="dropdown">',
				        'parentl1_anchor'     => '<a href="%s" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">%s<span class="caret"></span></a>',
				        'parent_tag_open'     => '<li class="dropdown-submenu">',
				        'parent_anchor'       => '<a href="%s">%s</a>',
				        'children_tag_open'   => '<ul class="dropdown-menu">'
				    )); ?>
			    </div>
		  	</div>
		</nav>
	</div>
</div>
<!-- /END NAVBAR -->