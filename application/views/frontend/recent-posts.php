<div class="panel panel-transparent">
	<div class="panel-heading" data-toggle="collapse" data-target="#listRecentPost">
		<h3 class="panel-title"><i class="fas fa-edit"></i> Bài đăng gần đây</h3>
	</div>
	<div class="panel-body collapse in" id="listRecentPost">
	<?php foreach($post_limit as $item){ ?>
		<div class="media">
		  	<div class="media-left">
			    <a href="<?php echo url_post($item['Title_Slug_Post'],$item['ID_Post']); ?>" title="<?php echo $item['Title_Post']; ?>">
			      <img class="media-object" width="85" src="<?php echo thumbnail_post($item['Thumbnail_Post']); ?>" alt="<?php echo $item['Title_Post']; ?>">
			    </a>
		  	</div>
		  	<div class="media-body">
			    <h4 class="media-heading"><a href="<?php echo url_post($item['Title_Slug_Post'],$item['ID_Post']); ?>" title="<?php echo $item['Title_Post']; ?>"><?php echo word_limiter($item['Title_Post'],6); ?></a></h4>
			    <p class="text-author"><i class="fas fa-calendar-alt"></i> <?php echo date_post($item['Createday_Post']); ?></p>
		  	</div>
		</div>
	<?php } ?>
	</div>
</div>