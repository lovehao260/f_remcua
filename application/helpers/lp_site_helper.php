<?php defined('BASEPATH') OR exit('No direct script access allowed');

function meta_title($setting = null,$page = null)
{
	if (isset($setting)) {
		if ($setting != null && $page != null) {
			return $page.' - '.$setting;
		}else{
			return $setting;
		}
	}
	return null;
}
function meta_keywords($keywords)
{
	if(isset($keywords))
	{
		return $keywords;
	}
	return null;
}
function meta_author($author)
{
	if(isset($author))
	{
		return $author;
	}
	return null;
}
function meta_description($description)
{
	if (isset($description)) {
		return $description;
	}
	return null;
}
function class_active($link,$slug,$id)
{
	if($_SERVER['REQUEST_URI'] == '/'.$link.'/'.$slug.'-'.$id){
		return "active";
	}
}
function active_menu($link)
{
	$ci =& get_instance();
	if (isset($link)) {
		if($ci->uri->segment(2) == $link){
			return "active";
		}
	}
}
function thumbnail_post($link)
{
	if (isset($link)) {
		return $link;
	}else{
		return '//placehold.it/480x324&text=No Image';
	}
}
function thumbnail_product($link)
{
	if (isset($link)) {
		return $link;
	}else{
		return '//placehold.it/800x600&text=No Image';
	}
}
function url_post($slug,$id)
{
	if (isset($slug)) {
		$link = base_url('post/'.$slug.'-'.$id);
		return $link;
	}
	return null;
}
function url_page($slug,$id)
{
	if (isset($slug)) {
		$link = base_url('page/'.$slug.'-'.$id);
		return $link;
	}
	return null;
}
function url_color($slug,$id)
{
	if (isset($slug)) {
		$link = base_url('san-pham/mau-sac/'.$slug.'-'.$id);
		return $link;
	}
	return null;
}
function url_product($slug,$id)
{
	if (isset($slug)) {
		$link = base_url('san-pham/'.$slug.'-'.$id);
		return $link;
	}
	return null;
}
function url_category($slug,$id)
{
	if (isset($slug)) {
		$link = base_url('danh-muc-san-pham/'.$slug.'-'.$id);
		return $link;
	}
	return null;
}
function price($price,$a = null,$b = null)
{
	if (isset($price)) {
		if ($b != null) {
			$b = '/'.$b;
		}
		return number_format($price).$a.$b;
	}
	return null;
}
function check_item($item,$echo = null)
{
	if (isset($item)) {
		return $item.$echo;
	}else{
		return $echo;
	}
	return null;
}
function check_product($product)
{
	if (isset($product)) {
		if ($product == 'Y') {
			return '<span class="text-success"><strong>Còn hàng</strong></span>';
		}else{
			return '<span class="text-danger"><strong>Hết hàng</strong></span>';
		}
	}
	return null;
}
function public_item($item)
{
	if (isset($item)) {
		if ($item == 'Y') {
			return '<span class="badge badge-success">Hiển thị</span>';
		}else{
			return '<span class="badge badge-secondary">Ẩn</span>';
		}
	}
}
function title_category($category,$color)
{
	if(isset($category) && !isset($color)){
		return $category;
	}elseif(isset($color) && !isset($category)){
		return "Bộ lọc màu: ".$color;
	}elseif(!isset($category)){
		return "Tất cả sản phẩm";
	} 
}
function explode_item($id)
{
	if (isset($id)) {
		$kytu = ',';
		if ($kytu != null) {
			return explode($kytu, $id);
		}else{
			return explode(' ', $id);
		}
	}
	return null;
}
function name_product($name)
{
	if (isset($name)) {
		return word_limiter($name,14,'...');
	}
}
function date_post($date)
{
	$month = array(
		'01' => 'Tháng 1',
		'02' => 'Tháng 2',
		'03' => 'Tháng 3',
		'04' => 'Tháng 4',
		'05' => 'Tháng 5',
		'06' => 'Tháng 6',
		'07' => 'Tháng 7',
		'08' => 'Tháng 8',
		'09' => 'Tháng 9',
		'10' => 'Tháng 10',
		'11' => 'Tháng 11',
		'12' => 'Tháng 12'
	);
	$substr_year = substr($date, 0,4);
	$substr_month = substr($date, 5,2);
	$substr_day = substr($date, 8,2);
	foreach ($month as $key => $value) {
		if ($substr_month == $key) {
			return $substr_day.' '.$value.' '.$substr_year;
		}
	}
}
function datetime_item($date)
{
	$month = array(
		'01' => 'Tháng 1',
		'02' => 'Tháng 2',
		'03' => 'Tháng 3',
		'04' => 'Tháng 4',
		'05' => 'Tháng 5',
		'06' => 'Tháng 6',
		'07' => 'Tháng 7',
		'08' => 'Tháng 8',
		'09' => 'Tháng 9',
		'10' => 'Tháng 10',
		'11' => 'Tháng 11',
		'12' => 'Tháng 12'
	);
	$substr_year = substr($date, 0,4);
	$substr_month = substr($date, 5,2);
	$substr_day = substr($date, 8,2);
	$substr_hours = substr($date, 11,2);
	$substr_min = substr($date, 14,2);
	$substr_sec = substr($date, 17,2);
	foreach ($month as $key => $value) {
		if ($substr_month == $key) {
			return $substr_hours.':'.$substr_min.' | '.$substr_day.' '.$value.' '.$substr_year;
		}
	}
}
/*Mã hóa mật khẩu*/
if (!function_exists('hash_password')) {
	function hash_password($password) {
		return do_hash(md5(sha1(do_hash($password))));
	}
}

function check_item_array($item,$array = array())
{
	if (array_key_exists($item, $array) == true) {
		
	}
}
function result_message($result)
{
	if (isset($result)) {
		return $result;
	}else{
		return null;	
	}
	return false;
}
function to_slug($str) {
    $str = trim(mb_strtolower($str));
    $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
    $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
    $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
    $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
    $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
    $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
    $str = preg_replace('/(đ)/', 'd', $str);
    $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
    $str = preg_replace('/([\s]+)/', '-', $str);
    return $str;
}
function congthuc($chieucao,$chieurong,$gia,$pheptinh)
{
	switch ($pheptinh) {
		case 'metvuong':
			return $chieucao * $chieurong * $gia;
			break;
		case 'mettoi':
			return $chieurong * $gia;
			break;
		default:
			return null;
			break;
	}
}
