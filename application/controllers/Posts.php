<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Posts extends CI_Controller
{
	
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		/*Menu*/
		$items = $this->LP_Menu_FE->get_menu();
		$this->multi_menu->set_items($items);
	}
	public function index()
	{
		/*Pagination*/
		$config['base_url'] = base_url('posts');
		$config['total_rows'] = $this->LP_Post_FE->total_rows();
		$config['per_page'] = 6;
		$config['num_links'] = 2;
		$config['uri_segment'] = 2;
		$offset = $this->uri->segment(2);
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		$data['get_post'] = $this->LP_Post_FE->get_post($config['per_page'],$offset);

		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],'Bài viết');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		$this->load->template('frontend','all-post',isset($data)?$data:null);
	}
	public function category($id)
	{
		
		$data['category'] = $this->LP_Post_FE->get_category();
		$data['detail_category'] = $this->LP_Post_FE->detail_category($id);
		$data['detail_post'] = $this->LP_Post_FE->detail_post($id);
		$data['post_limit'] = $this->LP_Post_FE->get_post_limit(5);
		$data['ads'] = $this->LP_Ads_FE->get_ads('category_post');

		/*Pagination*/
		$config['base_url'] = base_url('danh-muc-bai-viet/'.$data['detail_category']['Name_Slug_CPost'].'-'.$data['detail_category']['ID_CPost']);
		$config['total_rows'] = $this->LP_Post_FE->total_rows_category($id);
		$config['per_page'] = 6;
		$config['num_links'] = 2;
		$config['uri_segment'] = 3;
		$offset = $this->uri->segment(3);
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['category_post'] = $this->LP_Post_FE->category_post($id,$config['per_page'],$offset);

		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['detail_category']['Name_CPost']);
		$data['meta_keywords'] = meta_keywords($data['detail_category']['Keywords_CPost']);
		$data['meta_description'] = meta_description($data['detail_category']['Description_CPost']);
		$data['meta_author'] = meta_author($this->setting[3]);

		$this->load->template('frontend','category-post',isset($data)?$data:null);
	}
	public function view($id)
	{
		$data['category'] = $this->LP_Post_FE->get_category();
		$data['detail_post'] = $this->LP_Post_FE->detail_post($id);
		$data['detail_category'] = $this->LP_Post_FE->detail_category($data['detail_post']['ID_Cat_Post']);
		$data['post_limit'] = $this->LP_Post_FE->get_post_limit(5);
		$data['related'] = $this->LP_Post_FE->related_post($data['detail_post']['ID_Cat_Post']);
		$data['ads'] = $this->LP_Ads_FE->get_ads('category_post');
		$this->LP_Post_FE->set_view($id);
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['detail_post']['Title_Post']);
		$data['meta_keywords'] = meta_keywords($data['detail_post']['Keywords_Post']);
		$data['meta_description'] = meta_description($data['detail_post']['Description_Post']);
		$data['meta_author'] = meta_author($this->setting[3]);
		
		$this->load->template('frontend','detail-post',isset($data)?$data:null);
	}
}

