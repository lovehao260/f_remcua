<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Product extends CI_Controller
{
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		/*Menu*/
		$items = $this->LP_Menu_FE->get_menu();
		$this->multi_menu->set_items($items);
	}
	public function index()
	{
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],'Sản phẩm');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		$data['color'] = $this->LP_Product_FE->get_color();
		$data['category'] = $this->LP_Product_FE->get_category();

		/*Pagination*/
		$config['base_url'] = base_url('tat-ca-san-pham');
		$config['total_rows'] = $this->LP_Product_FE->total_rows();
		$config['per_page'] = 21;
		$config['num_links'] = 2;
		$config['uri_segment'] = 2;
		$offset = $this->uri->segment(2);
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		$data['get_product'] = $this->LP_Product_FE->get_product(null,null,$config['per_page'],$offset);

		/*Load template*/
		$this->load->template('frontend','category-product',isset($data)?$data:null);
	}
	public function view($id)
	{
		$data['color'] = $this->LP_Product_FE->get_color();
		$data['category'] = $this->LP_Product_FE->get_category();
		$data['detail_product'] = $this->LP_Product_FE->detail_product($id);
		$data['name_category'] = $this->LP_Product_FE->get_name_category($data['detail_product']['Category_P']);
		$data['get_thumbnail'] = $this->LP_Product_FE->get_thumbnail($id);
		$color = explode_item($data['detail_product']['Color_P']);
		$data['get_color'] = $this->LP_Product_FE->get_color_detail($color);

		$tags = explode_item($data['detail_product']['Tags_P']);
		$data['get_tag'] = $this->LP_Product_FE->get_tag_detail($tags);

		$data['related_product'] = $this->LP_Product_FE->related_product($data['detail_product']['Category_P']);
		$this->LP_Product_FE->set_view($id);
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['detail_product']['Name_P']);
		$data['meta_keywords'] = meta_keywords($data['detail_product']['Keywords_P']);
		$data['meta_description'] = meta_description($data['detail_product']['Description_P']);
		$data['meta_author'] = meta_author($this->setting[3]);

		$this->load->template('frontend','detail-product',isset($data)?$data:null);
	}
	public function category($id_category)
	{
		$data['color'] = $this->LP_Product_FE->get_color();
		$data['category'] = $this->LP_Product_FE->get_category();
		$data['name_category'] = $this->LP_Product_FE->get_name_category($id_category);

		/*Pagination*/
		$config['base_url'] = base_url('danh-muc-san-pham/'.$data['name_category']['Name_Slug_CatP'].'-'.$data['name_category']['ID_CatP']);
		$config['total_rows'] = $this->LP_Product_FE->total_rows_category($id_category);
		$config['per_page'] = 21;
		$config['num_links'] = 2;
		$config['uri_segment'] = 3;
		$offset = $this->uri->segment(3);
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['get_product'] = $this->LP_Product_FE->get_product($id_category,null, $config['per_page'],$offset);
		
		$data['ads'] = $this->LP_Ads_FE->get_ads('category_product');
		$data['ads_top'] = $this->LP_Ads_FE->get_ads('category_product_top',1);

		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['name_category']['Name_CatP']);
		$data['meta_keywords'] = meta_keywords($data['name_category']['Keywords_CatP']);
		$data['meta_description'] = meta_description($data['name_category']['Description_CatP']);
		$data['meta_author'] = meta_author($this->setting[3]);

		$this->load->template('frontend','category-product',isset($data)?$data:null);
	}
	public function color($id_color)
	{
		$data['color'] = $this->LP_Product_FE->get_color();
		$data['category'] = $this->LP_Product_FE->get_category();
		$data['name_color'] = $this->LP_Product_FE->get_name_color($id_color);

		/*Pagination*/
		$config['base_url'] = base_url('san-pham/mau-sac/'.$data['name_color']['Name_Slug_Color'].'-'.$data['name_color']['ID_Color']);
		$config['total_rows'] = $this->LP_Product_FE->total_rows_color($id_color);
		$config['per_page'] = 21;
		$config['num_links'] = 2;
		$config['uri_segment'] = 4;
		$offset = $this->uri->segment(4);
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		$data['get_product'] = $this->LP_Product_FE->get_product(null,$id_color,$config['per_page'],$offset);
		$data['ads'] = $this->LP_Ads_FE->get_ads('category_product');

		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['name_color']['Name_Color']);
		$data['meta_keywords'] = meta_keywords($data['name_color']['Keywords_Color']);
		$data['meta_description'] = meta_description($data['name_color']['Description_Color']);
		$data['meta_author'] = meta_author($this->setting[3]);

		$this->load->template('frontend','category-product',isset($data)?$data:null);
	}
}

