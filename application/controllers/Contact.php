<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Contact extends CI_Controller
{
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		/*Menu*/
		$items = $this->LP_Menu_FE->get_menu();
		$this->multi_menu->set_items($items);
	}
	public function index()
	{
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],'Liên hệ');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		$data['carousel'] = $this->LP_Carousel_FE->get_list();
		$data['min_carousel'] = $this->LP_Carousel_FE->min_sort_col();
		$data['get_product'] = $this->LP_Product_FE->get_product_home(8);
		$data['get_post_home'] = $this->LP_Post_FE->get_post_home('home',9);
		$data['get_category_position'] = $this->LP_Product_FE->get_category_position('home_bottom');
		$data['get_page_position'] = $this->LP_Page_FE->get_page_position('home',4);
		$data['get_page_position_big'] = $this->LP_Page_FE->get_page_position('home_big',1);

		if (isset($_POST['send'])) {
			$param = array(
				'Name_Contact'		=> $this->input->post('hovaten'),
				'Phone_Contact'		=> $this->input->post('dienthoai'),
				'Email_Contact'		=> $this->input->post('email'),
				'Messages_Contact'	=> $this->input->post('noidung'),
				'Session_Contact'	=> session_id(),
				'User_Agent_Contact'=> $this->agent->browser().' '.$this->agent->version(),
				'Platform_Contact'	=> $this->agent->platform(),
				'Createday_Contact'	=> date('Y-m-d H:i:s'),
				'Modify_Contact'	=> date('Y-m-d H:i:s')
			);
			$query = $this->LP_Contact_FE->add_contact($param);
			if ($query == true) {
				$data['result'] = 'Gửi liên hệ thành công!';
			}else{
				$data['result'] = 'Thất bại, vui lòng liên hệ: '.$this->setting[4];
			}
		}

		$this->load->template('frontend','contact',isset($data)?$data:null);
	}
}

