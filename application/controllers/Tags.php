<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Tags extends CI_Controller
{
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		/*Menu*/
		$items = $this->LP_Menu_FE->get_menu();
		$this->multi_menu->set_items($items);
	}
	public function index($id = null)
	{
		$data['get_detail_tags'] = $this->LP_Product_FE->get_detail_tags($id);
		$data['get_tags'] = $this->LP_Product_FE->get_tags();
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],'Tag: '.$data['get_detail_tags']['Name_Tag']);
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Pagination*/
		$config['base_url'] = base_url('tag/'.$data['get_detail_tags']['Name_Slug_Tag'].'-'.$data['get_detail_tags']['ID_Tag']);
		$config['total_rows'] = $this->LP_Product_FE->count_product_tag($id);
		$config['per_page'] = 9;
		$config['num_links'] = 2;
		$config['uri_segment'] = 3;
		$offset = $this->uri->segment(3);
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		
		$data['get_product_tag'] = $this->LP_Product_FE->get_product_tag($id,$config['per_page'],$offset);
		$this->load->template('frontend','tags',isset($data)?$data:null);
	}
}

