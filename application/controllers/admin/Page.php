<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Page extends CI_Controller
{
	
	public $setting;
	public $position;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		$check_login = $this->session->userdata('LoginSuccess');
		if (!isset($check_login)) {
			redirect(base_url('/admin/login'));
		}
		$this->position = array(
			'home' 		=> 'home',
			'home_big' 	=> 'home_big'
		);
	}
	public function index()
	{
		$data['header_page'] = 'Tất cả các trang';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả các trang', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Xóa bài viết*/
		if (isset($_POST['btnDelete'])) {
			$this->form_validation->set_rules('id_page','id trang','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$id_page = $this->input->post('id_page');
				$query = $this->LP_Page_BE->delete_page($id_page);
				if ($query == true) {
					$message = '<p class="text-success text-center">Xóa trang thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning text-center">Xóa trang thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}	
		}

		$data['get_page'] = $this->LP_Page_BE->get_page();
		$this->load->template('backend','page/list',isset($data)?$data:null);
	}
	public function add()
	{
		$data['header_page'] = 'Thêm trang mới';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả các trang', 'admin/page');
		$this->breadcrumbs->push('Thêm trang mới', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Viết bài mới*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('title_page','tiêu đề trang','trim|required');
			if ($this->form_validation->run() == true) {
				$param = array(
					'Title_Page'			=> $this->input->post('title_page'),
					'Title_Slug_Page'		=> to_slug($this->input->post('title_page')),
					'Short_Content_Page'	=> $this->input->post('short_page'),
					'Full_Content_Page'		=> $this->input->post('full_page'),
					'Thumbnail_Page'		=> $this->input->post('thumbnail_page'),
					'Position_Page'			=> $this->input->post('position_page'),
					'Public_Page'			=> $this->input->post('public_page'),
					'Sort_Page'				=> $this->input->post('sort_page'),
					'Views_Page'			=> 0,
					'Keywords_Page'			=> $this->input->post('keyword_page'),
					'Description_Page'		=> $this->input->post('description_page'),
					'Createday_Page'		=> date('Y-m-d H:i:s'),
					'Modify_Page'			=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Page_BE->add_page($param);
				if ($query == true) {
					$message = '<p class="text-success">Thêm trang mới thành công</p>';
					$this->session->set_flashdata('message',$message);
					header('Location: '.base_url('/admin/page/edit/'.$query));
				}else{
					$message = '<p class="text-warning">Thêm trang mới thất bại</p>';
					$this->session->set_flashdata('message',$message);
					header('Location: '.base_url('/admin/page/add'));
				}
			}
		}

		$data['category'] = $this->LP_Post_BE->get_list_category();
		$this->load->template('backend','page/add',isset($data)?$data:null);
	}
	public function edit($id)
	{
		$data['header_page'] = 'Chỉnh sửa trang';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả các trang', 'admin/page');
		$this->breadcrumbs->push('Chỉnh sửa trang', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Chỉnh sửa trang*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('title_page','tiêu đề trang','trim|required');
			if ($this->form_validation->run() == true) {
				$param = array(
					'Title_Page'			=> $this->input->post('title_page'),
					'Title_Slug_Page'		=> to_slug($this->input->post('title_page')),
					'Short_Content_Page'	=> $this->input->post('short_page'),
					'Full_Content_Page'		=> $this->input->post('full_page'),
					'Thumbnail_Page'		=> $this->input->post('thumbnail_page'),
					'Position_Page'			=> $this->input->post('position_page'),
					'Public_Page'			=> $this->input->post('public_page'),
					'Sort_Page'				=> $this->input->post('sort_page'),
					'Keywords_Page'			=> $this->input->post('keyword_page'),
					'Description_Page'		=> $this->input->post('description_page'),
					'Modify_Page'			=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Page_BE->update_page($id,$param);
				if ($query == true) {
					$message = '<p class="text-success">Chỉnh sửa trang thành công</p>';
					$this->session->set_flashdata('message',$message);
					header('Location: '.base_url('/admin/page/edit/'.$id));
				}else{
					$message = '<p class="text-warning">Chỉnh sửa trang thất bại</p>';
					$this->session->set_flashdata('message',$message);
					header('Location: '.base_url('/admin/page/edit/'.$id));
				}
			}
		}

		$data['detail_page'] = $this->LP_Page_BE->get_detail_page($id);
		$this->load->template('backend','page/edit',isset($data)?$data:null);
	}
	public function ajax_page()
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
			$draw = intval($this->input->get("draw"));
	      	$start = intval($this->input->get("start"));
	      	$length = intval($this->input->get("length"));

	      	$query = $this->db->get("Pages");

	      	$data = [];
	      	foreach($query->result() as $row) {
	      		$result_position = array_search($row->Position_Page,$this->position);
	      		if ($result_position == false) {
	      			$result_position = '';
	      		}
	           	$data[] = array(
	           		'<img src="'.$row->Thumbnail_Page.'" class="img-fluid" width="100" height="100">',
	                '<a href="'.base_url('/admin/page/edit/'.$row->ID_Page).'">'.$row->Title_Page.'</a>',
	                $result_position,
	                public_item($row->Public_Page), 
	                $row->Sort_Page,  
	                $row->Views_Page,
	                $row->Modify_Page,       
	                '<a href="'.base_url('/admin/page/edit/'.$row->ID_Page).'" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
	                <a href="" class="btn btn-danger btn-sm" onclick="get_id('.$row->ID_Page.')" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
	           	);
	      	}

	      	$result = array(
	           	"draw" => $draw,
	            "recordsTotal" => $query->num_rows(),
	            "recordsFiltered" => $query->num_rows(),
	            "data" => $data
	        );

	      	echo json_encode($result);
	      	exit();
	    }else{
	    	echo "Trang không tồn tại";
	    }
	}
}