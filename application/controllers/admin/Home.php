<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Home extends CI_Controller
{
	
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		$check_login = $this->session->userdata('LoginSuccess');
		if (!isset($check_login)) {
			redirect(base_url('/admin/login'));
		}
	}
	public function index()
	{
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],'Home - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);
		$data['user_login'] = $this->session->userdata('LoginSuccess');
		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		$data['contact'] = $this->LP_Contact_BE->get_contact_home(10);

		$this->load->template('backend','home',isset($data)?$data:null);
	}
	public function error()
	{
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],'Home - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		// echo $this->uri->segment(2);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		$this->load->template('backend','home',isset($data)?$data:null);
	}
}