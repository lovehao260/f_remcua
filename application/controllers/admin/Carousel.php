<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Carousel extends CI_Controller
{
	
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		$check_login = $this->session->userdata('LoginSuccess');
		if (!isset($check_login)) {
			redirect(base_url('/admin/login'));
		}
	}
	public function index()
	{
		$data['header_page'] = 'Trình chiếu ảnh';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Trình chiếu ảnh', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Xóa menu*/
		if (isset($_POST['btnDelete'])) {
			$this->form_validation->set_rules('id_slide','id slide','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$id_slide = $this->input->post('id_slide');
				$query = $this->LP_Carousel_BE->delete_slide($id_slide);
				if ($query == true) {
					$message = '<p class="text-success">Xóa trình chiếu thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning">Xóa trình chiếu thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}	
		}
		$data['get_carousel'] = $this->LP_Carousel_BE->get_carousel();
		$this->load->template('backend','carousel/list',isset($data)?$data:null);
	}
	public function add()
	{
		$data['header_page'] = 'Tạo trình chiếu';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Trình chiếu ảnh', 'admin/carousel');
		$this->breadcrumbs->push('Tạo trình chiếu', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Tạo trình chiếu*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_slide','tên trình chiếu','trim|required');
			if ($this->form_validation->run() == true) {
				$param = array(
					'Name_Slide'			=> $this->input->post('name_slide'),
					'Image_Slide'			=> $this->input->post('image_slide'),
					'Caption_Slide'			=> $this->input->post('caption_slide'),
					'Description_Slide'		=> $this->input->post('description_slide'),
					'Sort_Slide'			=> $this->input->post('sort_slide'),
					'Public_Slide'			=> $this->input->post('public_slide'),
					'Createday_Slide'		=> date('Y-m-d H:i:s'),
					'Modify_Slide'			=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Carousel_BE->add_slide($param);
				if ($query == true) {
					$message = '<p class="text-success">Tạo trình chiếu thành công</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/carousel'));
				}else{
					$message = '<p class="text-warning">Tạo trình chiếu thất bại</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/carousel'));
				}
			}
		}
		
		$data['get_carousel'] = $this->LP_Carousel_BE->get_carousel();
		$this->load->template('backend','carousel/add',isset($data)?$data:null);
	}
	public function edit($id)
	{
		$data['header_page'] = 'Chỉnh sửa trình chiếu';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Trình chiếu ảnh', 'admin/carousel');
		$this->breadcrumbs->push('Chỉnh sửa trình chiếu', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Chỉnh sửa trình chiếu*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_slide','tên trình chiếu','trim|required');
			if ($this->form_validation->run() == true) {
				$param = array(
					'Name_Slide'			=> $this->input->post('name_slide'),
					'Image_Slide'			=> $this->input->post('image_slide'),
					'Caption_Slide'			=> $this->input->post('caption_slide'),
					'Description_Slide'		=> $this->input->post('description_slide'),
					'Sort_Slide'			=> $this->input->post('sort_slide'),
					'Public_Slide'			=> $this->input->post('public_slide'),
					'Modify_Slide'			=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Carousel_BE->update_slide($id,$param);
				if ($query == true) {
					$message = '<p class="text-success">Chỉnh sửa trình chiếu thành công</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/carousel'));
				}else{
					$message = '<p class="text-warning">Chỉnh sửa trình chiếu thất bại</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/carousel/edit/'.$id));
				}
			}
		}
		$data['detail_slide'] = $this->LP_Carousel_BE->get_detail_slide($id);
		$this->load->template('backend','carousel/edit',isset($data)?$data:null);
	}
	
}