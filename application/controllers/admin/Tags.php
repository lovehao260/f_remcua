<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Tags extends CI_Controller
{
	
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		$check_login = $this->session->userdata('LoginSuccess');
		if (!isset($check_login)) {
			redirect(base_url('/admin/login'));
		}
	}
	public function index()
	{
		$data['header_page'] = 'Tags';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả sản phẩm', 'admin/product');
		$this->breadcrumbs->push('Tags', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Thêm tag*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_tag','tên tag','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$name_tag = $this->input->post('name_tag');
				$param = array(
					'Name_Tag'		=> $name_tag,
					'Name_Slug_Tag'	=> to_slug($name_tag),
					'Createday_Tag'	=> date('Y-m-d H:i:s'),
					'Modify_Tag'	=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Tag_BE->add_tag($param);
				if ($query == true) {
					$message = '<p class="text-success">Tạo tag thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning">Tạo tag thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}	
		}

		/*Xóa tag*/
		if (isset($_POST['btnDelete'])) {
			$this->form_validation->set_rules('id_tag','id tag','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$id_tag = $this->input->post('id_tag');
				$query = $this->LP_Tag_BE->delete_tag($id_tag);
				if ($query == true) {
					$message = '<p class="text-success">Xóa tag thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning">Xóa tag thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}	
		}

		$data['get_contact'] = $this->LP_Contact_BE->get_contact();
		$this->load->template('backend','tag/list',isset($data)?$data:null);
	}
	public function edit($id)
	{
		$data['header_page'] = 'Chỉnh sửa tag';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả sản phẩm', 'admin/product');
		$this->breadcrumbs->push('Tags', 'admin/tags');
		$this->breadcrumbs->push('Chỉnh sửa tag', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Sửa tag*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_tag','tên tag','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$name_tag = $this->input->post('name_tag');
				$param = array(
					'Name_Tag'		=> $name_tag,
					'Name_Slug_Tag'	=> to_slug($name_tag),
					'Modify_Tag'	=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Tag_BE->update_tag($id,$param);
				if ($query == true) {
					$message = '<p class="text-success">Sửa tag thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning">Sửa tag thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}	
		}

		$data['detail_tag'] = $this->LP_Tag_BE->get_detail_tag($id);

		$this->load->template('backend','tag/edit',isset($data)?$data:null);
	}
	
	public function ajax_tag()
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
			$draw = intval($this->input->get("draw"));
	      	$start = intval($this->input->get("start"));
	      	$length = intval($this->input->get("length"));

	      	$query = $this->db->get("Tags");

	      	$data = [];

	      	foreach($query->result() as $row) {
	           	$data[] = array(
	                '<a href="'.base_url('admin/tags/edit/'.$row->ID_Tag).'">'.$row->Name_Tag.'</a>',
	                $row->Name_Slug_Tag,
	                $row->Createday_Tag,
	                '<a href="'.base_url('/admin/tags/edit/'.$row->ID_Tag).'" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
	                <a href="" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="get_id('.$row->ID_Tag.')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
	           	);
	      	}

	      	$result = array(
	           	"draw" => $draw,
	            "recordsTotal" => $query->num_rows(),
	            "recordsFiltered" => $query->num_rows(),
	            "data" => $data
	        );

	      	echo json_encode($result);
	      	exit();
	    }else{
	    	echo "Trang không tồn tại";
	    }
	}
	
}