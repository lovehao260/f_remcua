<?php
/**
* 
*/
class Ckfinder extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		$check_login = $this->session->userdata('LoginSuccess');
		if (!isset($check_login)) {
			redirect(base_url('/admin/login'));
		}
	}
	public function index()
	{
		echo "Trang không tồn tại";
	}
	public function connector()
	{
		$check_login = $this->session->userdata('LoginSuccess');
		if (!isset($check_login)) {
			$auth = false;
		}else{
			$auth = true;
		}
		
		define('CKFINDER_CAN_USE', $auth);
		require_once "./assets/admin/ckfinder/core/connector/php/connector.php";
	}
	public function html()
	{
		$this->load->view('backend/ckfinder');
	}
}