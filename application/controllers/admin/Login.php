<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Login extends CI_Controller
{
	
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
	}
	public function index()
	{

		$check_login = $this->session->userdata('LoginSuccess');
		if (isset($check_login)) {
			redirect(base_url('/admin/home'));
		}
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0]);
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		if (isset($_POST['submit'])) {
			/*Kiểm tra dữ liệu nhập vào từ form*/
			$this->form_validation->set_rules('tendangnhap','tên đăng nhập','trim|required');
			$this->form_validation->set_rules('matkhau','mật khẩu','trim|callback__auth');
			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
			/*Khởi động*/
			if ($this->form_validation->run() == TRUE) {
				/*Chuyển hướng*/
				redirect(base_url('/admin/home'));
			}
		}

		/*Thông báo*/
		$data['message'] = $this->session->flashdata('message');

		$this->load->view('backend/login',isset($data)?$data:null);
	}
	public function _auth($password = '', $username = '')
	{
		$username = $this->input->post('tendangnhap');
		$count = $this->LP_Login_BE->total(array('Username_U' => $username));
		if ($count == 0) {
			$this->form_validation->set_message('_auth','Tài khoản '.$username.' không tồn tại.');
			return false;
		}
		$hash = hash_password($password);
		$user = $this->LP_Login_BE->get('*',array('Username_U' => $username));
		if ($user['Password_U'] != $hash) {
			$this->form_validation->set_message('_auth','Mật khẩu không đúng.');
			return false;
		}else if ($user['Active_U'] == 'N') {
			$this->form_validation->set_message('_auth','Tài khoản đã bị vô hiệu hóa.');
			return false;
		}else {
			$param = array('Lastlogin_U' => date('Y-m-d H:i:s'));
			$this->LP_Login_BE->update_lastlogin($user['ID_U'],$param);
			$this->session->set_userdata('LoginSuccess',$user);
			// setcookie('auth',hash_password('LamPhan'), time()+7200);
			$_SESSION['Login_User'] = $user;
		}
	}
	public function out()
	{
		/*Xóa dữ liệu trong session LoginSuccess*/
		$this->session->unset_userdata('LoginSuccess');
		setcookie('auth',null,-1,'/');
		/*Tạo dữ liệu trong session Logout*/
		$this->session->set_flashdata('message','Đăng xuất thành công!');

		/*Chuyển hướng*/
		redirect(base_url('admin/login'));
	}
}