<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Setting extends CI_Controller
{
	
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		$check_login = $this->session->userdata('LoginSuccess');
		if (!isset($check_login)) {
			redirect(base_url('/admin/login'));
		}
	}
	public function index()
	{
		$data['header_page'] = 'Cài đặt';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Cài đặt', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		if (isset($_POST['btnSave'])) {
			$param = array(
				'Site_Title' 		=> $this->input->post('title_setting'),
				'Slogan'			=> $this->input->post('sloganleft_setting'),
				'Slogan_Right'		=> $this->input->post('sloganright_setting'),
				'Keywords'			=> $this->input->post('keywords_setting'),
				'Description'		=> $this->input->post('description_setting'),
				'Author'			=> $this->input->post('author_setting'),
				'Phone'				=> $this->input->post('phone_setting'),
				'Email'				=> $this->input->post('email_setting'),
				'Address'			=> $this->input->post('address_setting'),
				'Logo'				=> $this->input->post('logo_setting'),
				'Facebook'			=> $this->input->post('facebook_setting'),
				'Youtube'			=> $this->input->post('youtube_setting'),
				'GooglePlus'		=> $this->input->post('googleplus_setting'),
				'Twitter'			=> $this->input->post('twitter_setting'),
				'Unit_Price'		=> $this->input->post('unitprice_setting'),
				'Float_Price'		=> $this->input->post('float_setting'),
				'Google_Analytics'	=> $this->input->post('googleana_setting'),
				'Facebook_ID'		=> $this->input->post('facebookid_setting'),
				'Desc_Sub'			=> $this->input->post('descriptionsub_setting'),
				'Percent_MetVuong'	=> $this->input->post('percent_setting'),
				'Google_Map'		=> $this->input->post('map_setting'),	
				'Text_Home'			=> $this->input->post('texthome_setting'),
				'ID_TawkTo'			=> $this->input->post('idtawkto_setting'),
				'Page_Facebook_ID'	=> $this->input->post('page_facebookid_setting'),
				'Favicon'			=> $this->input->post('iconlogo_setting')
			);
			foreach ($param as $key => $value) {
				$query = $this->LP_Setting_BE->update_setting($key,$value);
			}
			if ($query == true) {
				$message = '<p class="text-success text-center">Lưu cài đặt thành công</p>';
				$this->session->set_flashdata('message',$message);
			}else{
				$message = '<p class="text-warning text-center">Lưu cài đặt thất bại</p>';
				$this->session->set_flashdata('message',$message);
			}
		}
		$this->setting = $this->LP_Setting_FE->get_setting();
		$this->load->template('backend','setting/edit',isset($data)?$data:null);
	}
	
}