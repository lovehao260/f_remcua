<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Contact extends CI_Controller
{
	
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		$check_login = $this->session->userdata('LoginSuccess');
		if (!isset($check_login)) {
			redirect(base_url('/admin/login'));
		}
	}
	public function index()
	{
		$data['header_page'] = 'Tất cả liên hệ';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả liên hệ', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Xóa liên hệ*/
		if (isset($_POST['btnDelete'])) {
			$this->form_validation->set_rules('id_contact','id liên hệ','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$id_contact = $this->input->post('id_contact');
				$query = $this->LP_Contact_BE->delete_contact($id_contact);
				if ($query == true) {
					$message = '<p class="text-success">Xóa liên hệ thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning">Xóa liên hệ thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}	
		}

		$data['get_contact'] = $this->LP_Contact_BE->get_contact();
		$this->load->template('backend','contact/list',isset($data)?$data:null);
	}
	public function add()
	{
		$data['header_page'] = 'Thêm liên hệ';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả liên hệ', 'admin/contact');
		$this->breadcrumbs->push('Thêm liên hệ', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		$data['title_card'] = 'Thêm liên hệ';

		/*Thêm liên hệ*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_contact','họ và tên','trim|required');
			$this->form_validation->set_rules('phone_contact','số điện thoại','trim|required');
			$this->form_validation->set_rules('content_contact','nội dung','trim|required');
			if ($this->form_validation->run() == true) {
				$param = array(
					'Name_Contact'			=> $this->input->post('name_contact'),
					'Phone_Contact'			=> $this->input->post('phone_contact'),
					'Email_Contact'			=> $this->input->post('email_contact'),
					'Messages_Contact'		=> $this->input->post('content_contact'),
					'Session_Contact'		=> $this->input->post('session_contact'),
					'User_Agent_Contact'	=> $this->input->post('useragent_contact'),
					'Platform_Contact'		=> $this->input->post('platform_contact'),
					'Read_Contact'			=> 'N',
					'Createday_Contact'		=> date('Y-m-d H:i:s'),
					'Modify_Contact'		=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Contact_BE->add_contact($param);
				if ($query == true) {
					$message = '<p class="text-success">Thêm liên hệ thành công</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/contact/view/'.$query));
				}else{
					$message = '<p class="text-warning">Thêm liên hệ thất bại</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/contact/add'));
				}
			}
		}

		$this->load->template('backend','contact/add',isset($data)?$data:null);
	}
	public function view($id)
	{
		$data['detail_contact'] = $this->LP_Contact_BE->get_detail_contact($id);
		$data['header_page'] = $data['detail_contact']['Name_Contact'];
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả liên hệ', 'admin/contact');
		$this->breadcrumbs->push('Xem: '.$data['detail_contact']['Name_Contact'], '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Đã xem*/
		$this->LP_Contact_BE->set_read_contact($id);

		$this->load->template('backend','contact/view',isset($data)?$data:null);
	}
	public function edit($id)
	{
		$data['header_page'] = 'Chỉnh sửa liên hệ';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả liên hệ', 'admin/contact');
		$this->breadcrumbs->push('Chỉnh sửa liên hệ', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		$data['title_card'] = 'Chỉnh sửa liên hệ';

		/*Thêm liên hệ*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_contact','họ và tên','trim|required');
			$this->form_validation->set_rules('phone_contact','số điện thoại','trim|required');
			$this->form_validation->set_rules('content_contact','nội dung','trim|required');
			if ($this->form_validation->run() == true) {
				$param = array(
					'Name_Contact'			=> $this->input->post('name_contact'),
					'Phone_Contact'			=> $this->input->post('phone_contact'),
					'Email_Contact'			=> $this->input->post('email_contact'),
					'Messages_Contact'		=> $this->input->post('content_contact'),
					'Session_Contact'		=> $this->input->post('session_contact'),
					'User_Agent_Contact'	=> $this->input->post('useragent_contact'),
					'Platform_Contact'		=> $this->input->post('platform_contact'),
					'Modify_Contact'		=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Contact_BE->update_contact($id,$param);
				if ($query == true) {
					$message = '<p class="text-success">Chỉnh sửa liên hệ thành công</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/contact/view/'.$id));
				}else{
					$message = '<p class="text-warning">Chỉnh sửa liên hệ thất bại</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/contact/view/'.$id));
				}
			}
		}

		$data['detail_contact'] = $this->LP_Contact_BE->get_detail_contact($id);

		$this->load->template('backend','contact/edit',isset($data)?$data:null);
	}
	public function subscribe()
	{
		$data['header_page'] = 'Subscribe';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Subscribe', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Xóa liên hệ*/
		if (isset($_POST['btnDelete'])) {
			$this->form_validation->set_rules('id_sub','id sub','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$id_sub = $this->input->post('id_sub');
				$query = $this->LP_Contact_BE->delete_subscribe($id_sub);
				if ($query == true) {
					$message = '<p class="text-success">Xóa email thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning">Xóa email thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}	
		}

		$this->load->template('backend','subscribe/list',isset($data)?$data:null);
	}
	public function editsub($id)
	{
		$data['header_page'] = 'Subscribe';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Subscribe', 'admin/contact/subscribe');
		$this->breadcrumbs->push('Chỉnh sửa email', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Chỉnh sửa email*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('email_sub','email','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$param = array(
					'Email_Sub'		=> $this->input->post('email_sub'),
					'Modify_Sub'	=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Contact_BE->update_subscribe($id,$param);
				if ($query == true) {
					$message = '<p class="text-success">Cập nhật email thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning">Cập nhật email thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}	
		}
		$data['detail_subscribe'] = $this->LP_Contact_BE->get_detail_subscribe($id);

		$this->load->template('backend','subscribe/edit',isset($data)?$data:null);
	}
	public function ajax_contact()
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
			$draw = intval($this->input->get("draw"));
	      	$start = intval($this->input->get("start"));
	      	$length = intval($this->input->get("length"));

	      	$query = $this->db->get("Contacts");

	      	$data = [];

	      	foreach($query->result() as $row) {
	      		if ($row->Read_Contact == 'N') {
	      			$namecontact = '<a href="'.base_url('/admin/contact/view/'.$row->ID_Contact).'"><strong>'.$row->Name_Contact.' <span class="badge badge-success">Mới</span></strong></a>';
	      			$phonecontact = '<a href="tel:'.$row->Phone_Contact.'"><strong>'.$row->Phone_Contact.'</strong></a>';
	      			$emailcontact = '<a href="mailto:'.$row->Email_Contact.'"><strong>'.$row->Email_Contact.'</strong></a>';
	      			$createcontact = '<strong><span class="text-primary">'.$row->Createday_Contact.'</span></strong>';
	      			$modifycontact = '<strong><span class="text-primary">'.$row->Modify_Contact.'</span></strong>';
	      		}else{
	      			$namecontact = '<a href="'.base_url('/admin/contact/view/'.$row->ID_Contact).'" class="text-secondary">'.$row->Name_Contact.'</a>';
	      			$phonecontact = '<a href="tel:'.$row->Phone_Contact.'" class="text-secondary">'.$row->Phone_Contact.'</a>';
	      			$emailcontact = '<a href="mailto:'.$row->Email_Contact.'" class="text-secondary">'.$row->Email_Contact.'</a>';
	      			$createcontact = '<span class="text-secondary">'.$row->Createday_Contact.'</span>';
	      			$modifycontact = '<span class="text-secondary">'.$row->Modify_Contact.'</span>';
	      		}
	           	$data[] = array(
	                $namecontact,
	                $phonecontact,
	                $emailcontact,  
	                $createcontact,
	                $modifycontact,
	                '<a href="'.base_url('/admin/contact/view/'.$row->ID_Contact).'" class="btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="Xem"><i class="fa fa-eye" aria-hidden="true"></i></a>
	                <a href="'.base_url('/admin/contact/edit/'.$row->ID_Contact).'" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
	                <a href="" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="get_id('.$row->ID_Contact.')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
	           	);
	      	}

	      	$result = array(
	           	"draw" => $draw,
	            "recordsTotal" => $query->num_rows(),
	            "recordsFiltered" => $query->num_rows(),
	            "data" => $data
	        );

	      	echo json_encode($result);
	      	exit();
	    }else{
	    	echo "Trang không tồn tại";
	    }
	}
	public function ajax_subscribe()
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
			$draw = intval($this->input->get("draw"));
	      	$start = intval($this->input->get("start"));
	      	$length = intval($this->input->get("length"));

	      	$query = $this->db->get("Subscribers");

	      	$data = [];

	      	foreach($query->result() as $row) {
	           	$data[] = array(
	                '<a href="mailto:'.$row->Email_Sub.'">'.$row->Email_Sub.'</a>',
	                $row->Session_Sub,
	                $row->UserAgent_Sub,  
	                $row->Platform_Sub,
	                $row->Createday_Sub,
	                $row->Modify_Sub,
	                '<a href="'.base_url('/admin/contact/editsub/'.$row->ID_Sub).'" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
	                <a href="" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="get_id('.$row->ID_Sub.')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
	           	);
	      	}

	      	$result = array(
	           	"draw" => $draw,
	            "recordsTotal" => $query->num_rows(),
	            "recordsFiltered" => $query->num_rows(),
	            "data" => $data
	        );

	      	echo json_encode($result);
	      	exit();
	    }else{
	    	echo "Trang không tồn tại";
	    }
	}
}