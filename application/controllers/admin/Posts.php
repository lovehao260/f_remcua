<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Posts extends CI_Controller
{
	
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		$check_login = $this->session->userdata('LoginSuccess');
		if (!isset($check_login)) {
			redirect(base_url('/admin/login'));
		}
	}
	public function index()
	{
		$data['header_page'] = 'Tất cả bài viết';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả bài viết', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Xóa bài viết*/
		if (isset($_POST['btnDelete'])) {
			$this->form_validation->set_rules('id_post','id bài viết','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$id_post = $this->input->post('id_post');
				$query = $this->LP_Post_BE->delete_post($id_post);
				if ($query == true) {
					$message = '<p class="text-success text-center">Xóa bài viết thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning text-center">Xóa bài viết thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}	
		}

		$data['get_post'] = $this->LP_Post_BE->get_post();
		$this->load->template('backend','post/list',isset($data)?$data:null);
	}
	public function add()
	{
		$data['header_page'] = 'Viết bài mới';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả bài viết', 'admin/posts');
		$this->breadcrumbs->push('Viết bài mới', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		$data['position'] = array(
			'' 		=> 'Không có vị trí',
			'home' 	=> 'Trang chủ'
		);

		/*Viết bài mới*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('title_post','tên sản phẩm','trim|required');
			if ($this->form_validation->run() == true) {
				$param = array(
					'ID_Cat_Post'			=> $this->input->post('category_post'),
					'Title_Post'			=> $this->input->post('title_post'),
					'Title_Slug_Post'		=> to_slug($this->input->post('title_post')),
					'Short_Content_Post'	=> $this->input->post('short_post'),
					'Full_Content_Post'		=> $this->input->post('full_post'),
					'Thumbnail_Post'		=> $this->input->post('thumbnail_post'),
					'Position_Post'			=> $this->input->post('position_post'),
					'Public_Post'			=> $this->input->post('public_post'),
					'Views_Post'			=> 0,
					'Keywords_Post'			=> $this->input->post('keyword_post'),
					'Description_Post'		=> $this->input->post('description_post'),
					'Createday_Post'		=> date('Y-m-d H:i:s'),
					'Modify_Post'			=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Post_BE->add_post($param);
				if ($query == true) {
					$message = '<p class="text-success text-center">Thêm bài viết mới thành công</p>';
					$this->session->set_flashdata('message',$message);
					header('Location: '.base_url('/admin/posts/edit/'.$query));
				}else{
					$message = '<p class="text-warning text-center">Thêm bài viết mới thất bại</p>';
					$this->session->set_flashdata('message',$message);
					header('Location: '.base_url('/admin/posts/add'));
				}
			}
		}

		$data['category'] = $this->LP_Post_BE->get_list_category();
		$this->load->template('backend','post/add',isset($data)?$data:null);
	}
	public function edit($id)
	{
		$data['header_page'] = 'Chỉnh sửa bài viết';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả bài viết', 'admin/posts');
		$this->breadcrumbs->push('Chỉnh sửa bài viết', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		$data['position'] = array(
			'' 		=> 'Không có vị trí',
			'home' 	=> 'Trang chủ'
		);

		/*Viết bài mới*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('title_post','tên sản phẩm','trim|required');
			if ($this->form_validation->run() == true) {
				$param = array(
					'ID_Cat_Post'			=> $this->input->post('category_post'),
					'Title_Post'			=> $this->input->post('title_post'),
					'Title_Slug_Post'		=> to_slug($this->input->post('title_post')),
					'Short_Content_Post'	=> $this->input->post('short_post'),
					'Full_Content_Post'		=> $this->input->post('full_post'),
					'Thumbnail_Post'		=> $this->input->post('thumbnail_post'),
					'Position_Post'			=> $this->input->post('position_post'),
					'Public_Post'			=> $this->input->post('public_post'),
					'Views_Post'			=> 0,
					'Keywords_Post'			=> $this->input->post('keyword_post'),
					'Description_Post'		=> $this->input->post('description_post'),
					'Modify_Post'			=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Post_BE->update_post($id,$param);
				if ($query == true) {
					$message = '<p class="text-success text-center">Chỉnh sửa bài viết thành công</p>';
					$this->session->set_flashdata('message',$message);
					header('Location: '.base_url('/admin/posts/edit/'.$id));
				}else{
					$message = '<p class="text-warning text-center">Chỉnh sửa bài viết thất bại</p>';
					$this->session->set_flashdata('message',$message);
					header('Location: '.base_url('/admin/posts/edit/'.$id));
				}
			}
		}

		$data['category'] = $this->LP_Post_BE->get_list_category();
		$data['detail_post'] = $this->LP_Post_BE->get_detail_post($id);
		$this->load->template('backend','post/edit',isset($data)?$data:null);
	}
	public function category()
	{
		$data['header_page'] = 'Danh mục';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả bài viết', 'admin/posts');
		$this->breadcrumbs->push('Danh mục', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		$data['position'] = array(
			'' 		=> 'Không có vị trí',
			'home' 	=> 'Trang chủ'
		);

		/*Viết bài mới*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_catpost','tên danh mục','trim|required');
			if ($this->form_validation->run() == true) {
				$param = array(
					'Name_CPost'			=> $this->input->post('name_catpost'),
					'Name_Slug_CPost'		=> to_slug($this->input->post('name_catpost')),
					'Thumbnail_CPost'		=> $this->input->post('thumbnail_catpost'),
					'Keywords_CPost'		=> $this->input->post('keywords_catpost'),
					'Description_CPost'		=> $this->input->post('description_catpost'),
					'Sort_CPost'			=> $this->input->post('sort_catpost'),
					'Public_CPost'			=> $this->input->post('public_catpost'),
					'Createday_CPost'		=> date('Y-m-d H:i:s'),
					'Modify_CPost'			=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Post_BE->add_category($param);
				if ($query == true) {
					$message = '<p class="text-success">Thêm danh mục mới thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning">Thêm danh mục mới thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}
		}

		/*Xóa danh mục*/
		if (isset($_POST['btnDelete'])) {
			$this->form_validation->set_rules('id_catpost','id danh mục','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$id_catpost = $this->input->post('id_catpost');
				$query = $this->LP_Post_BE->delete_category($id_catpost);
				if ($query == true) {
					$message = '<p class="text-success">Xóa danh mục thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning">Xóa danh mục thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}	
		}

		$data['category'] = $this->LP_Post_BE->get_list_category_modify();
		$this->load->template('backend','category-post/list',isset($data)?$data:null);
	}
	public function editcategory($id)
	{
		$data['header_page'] = 'Chỉnh sửa danh mục';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả bài viết', 'admin/posts');
		$this->breadcrumbs->push('Danh mục', 'admin/posts/category');
		$this->breadcrumbs->push('Chỉnh sửa danh mục', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Chỉnh sửa danh mục*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_catpost','tên danh mục','trim|required');
			if ($this->form_validation->run() == true) {
				$param = array(
					'Name_CPost'			=> $this->input->post('name_catpost'),
					'Name_Slug_CPost'		=> to_slug($this->input->post('name_catpost')),
					'Thumbnail_CPost'		=> $this->input->post('thumbnail_catpost'),
					'Keywords_CPost'		=> $this->input->post('keywords_catpost'),
					'Description_CPost'		=> $this->input->post('description_catpost'),
					'Sort_CPost'			=> $this->input->post('sort_catpost'),
					'Public_CPost'			=> $this->input->post('public_catpost'),
					'Modify_CPost'			=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Post_BE->update_category($id,$param);
				if ($query == true) {
					$message = '<p class="text-success">Chỉnh sửa danh mục thành công</p>';
					$this->session->set_flashdata('message',$message);
					header('Location: '.base_url('/admin/posts/category'));
				}else{
					$message = '<p class="text-warning">Chỉnh sửa danh mục thất bại</p>';
					$this->session->set_flashdata('message',$message);
					header('Location: '.base_url('/admin/posts/editcategory/'.$id));
				}
			}
		}

		$data['detail_category'] = $this->LP_Post_BE->get_category($id);
		$this->load->template('backend','category-post/edit',isset($data)?$data:null);
	}
	public function ajax_post()
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
			$draw = intval($this->input->get("draw"));
	      	$start = intval($this->input->get("start"));
	      	$length = intval($this->input->get("length"));

	      	$query = $this->db->get("Posts");

	      	$data = [];

	      	foreach($query->result() as $row) {
	      		$category = $this->LP_Post_BE->get_category($row->ID_Cat_Post);
	           	$data[] = array(
	                '<a href="'.base_url('/admin/posts/edit/'.$row->ID_Post).'">'.$row->Title_Post.'</a>',
	                $category['Name_CPost'],
	                public_item($row->Public_Post), 
	                $row->Views_Post,  
	                $row->Modify_Post,       
	                '<a href="'.base_url('/admin/posts/edit/'.$row->ID_Post).'" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
	                <a href="" class="btn btn-danger btn-sm" onclick="get_id('.$row->ID_Post.')" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
	           	);
	      	}

	      	$result = array(
	           	"draw" => $draw,
	            "recordsTotal" => $query->num_rows(),
	            "recordsFiltered" => $query->num_rows(),
	            "data" => $data
	        );

	      	echo json_encode($result);
	      	exit();
	    }else{
	    	echo "Trang không tồn tại";
	    }
	}
}