<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Product extends CI_Controller
{
	
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		$check_login = $this->session->userdata('LoginSuccess');
		if (!isset($check_login)) {
			redirect(base_url('/admin/login'));
		}
	}
	public function index()
	{
		$data['header_page'] = 'Tất cả sản phẩm';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả sản phẩm', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		if (isset($_POST['btnDelete'])) {
			$this->form_validation->set_rules('id_product','id sản phẩm','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$id = $this->input->post('id_product');
				$query = $this->LP_Product_BE->delete_product($id);
				$query_thumb = $this->LP_Product_BE->delete_thumbnail($id);
				if ($query == true && $query_thumb == true) {
					$message = '<p class="text-success">Xóa sản phẩm thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning">Xóa sản phẩm thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}
		}

		$data['get_product'] = $this->LP_Product_BE->get_product();

		$this->load->template('backend','product/list',isset($data)?$data:null);
	}
	public function add()
	{
		$data['header_page'] = 'Thêm sản phẩm mới';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', '/admin/home');
		$this->breadcrumbs->push('Tất cả sản phẩm', '/admin/product');
		$this->breadcrumbs->push('Thêm sản phẩm mới', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Thêm sản phẩm mới*/
		if (isset($_POST['btnSave'])) {
			$tags_array = array();
			$this->form_validation->set_rules('name_product','tên sản phẩm','trim|required');
			$this->form_validation->set_rules('category_product','danh mục','required');
			if ($this->form_validation->run() == true) {
				$color = $this->input->post('color_product');
				if ($color != null) {
					$inputColor = implode(',',$this->input->post('color_product'));
				}else{
					$inputColor = null;
				}
				$gia_mettoi = implode('',explode(',', $this->input->post('price_02_product')));
				$gia_metvuong = implode('',explode(',', $this->input->post('price_product')));
				$param = array(
					'Name_P'			=> $this->input->post('name_product'),
					'Name_Slug_P'		=> to_slug($this->input->post('name_product')),
					'Price_P'			=> $gia_metvuong,
					'Price_2_P'			=> $gia_mettoi,
					'Code_P'			=> $this->input->post('code_product'),
					'Category_P'		=> $this->input->post('category_product'),
					'Color_P'			=> $inputColor,
					'Thumbnail_P'		=> $this->input->post('thumbnail_product'),
					'Short_Content_P'	=> $this->input->post('short_product'),
					'Full_Content_P'	=> $this->input->post('full_product'),
					'Keywords_P'		=> $this->input->post('keyword_product'),
					'Description_P'		=> $this->input->post('description_product'),
					'Public_P'			=> $this->input->post('public_product'),
					'View_P'			=> 0,
					'Status_P'			=> $this->input->post('status_product'),
					'Createday_P'		=> date('Y-m-d H:i:s'),
					'Modify_P'			=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Product_BE->add_product($param);
				if ($query == true) {
					$album = $this->input->post('albumThumbnail');
					if(!empty($album)){	
						foreach ($album as $value) {
							$param_thumb = array(
								'Thumbnail_link_Tn'	=> $value,
								'Product_Tn'		=> $query,
								'Createday_Tn'		=> date('Y-m-d H:i:s'),
								'Modify_Tn'			=> date('Y-m-d H:i:s')
							);
							$this->LP_Product_BE->add_thumbnail($param_thumb);
						}
					}
					// Thêm tag
					$tags = $this->input->post('tag_product');
					if ($tags != null) {
						foreach (explode(',', $tags) as $value) {
							$check_tag = $this->LP_Product_BE->check_tag($value);
							if ($check_tag >= 1) {
								$get_tag = $this->LP_Product_BE->get_info_tag($value);
								foreach ($get_tag as $value2) {
									$tags_array[] = $value2['ID_Tag'];
								}
							}elseif($check_tag == 0){
								$param_tag = array(
									'Name_Tag'			=> $value,
									'Name_Slug_Tag'		=> to_slug($value),
									'Createday_Tag'		=> date('Y-m-d H:i:s'),
									'Modify_Tag'		=> date('Y-m-d H:i:s')
								);
								$insert_tag = $this->LP_Product_BE->insert_tag($param_tag);
								$tags_array[] = $insert_tag;
							}
						}
						$data_insert_tag = implode(',', $tags_array);
					}else{
						$data_insert_tag = null;
					}
					$param_02 = array('Tags_P' => $data_insert_tag);
					$query_02 = $this->LP_Product_BE->update_product($query,$param_02);
					$message = '<p class="text-success text-center">Thêm sản phẩm mới thành công</p>';
					$this->session->set_flashdata('message',$message);
					header('Location: '.base_url('/admin/product/edit/'.$query));
				}else{
					$message = '<p class="text-warning text-center">Thêm sản phẩm mới thất bại</p>';
					$this->session->set_flashdata('message',$message);
					header('Location: '.base_url('/admin/product/add'));
				}
			}
		}

		$data['get_list_category'] = $this->LP_Product_BE->get_list_category();
		$data['get_list_color'] = $this->LP_Product_BE->get_list_color();
		$this->load->template('backend','product/add',isset($data)?$data:null);
	}
	public function edit($id)
	{
		$data['header_page'] = 'Chỉnh sửa sản phẩm';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', '/admin/home');
		$this->breadcrumbs->push('Tất cả sản phẩm', '/admin/product');
		$this->breadcrumbs->push('Chỉnh sửa sản phẩm', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		

		/*Thêm sản phẩm mới*/
		if (isset($_POST['btnSave'])) {
			$tags_array = array();
			$this->form_validation->set_rules('name_product','tên sản phẩm','trim|required');
			$this->form_validation->set_rules('category_product','danh mục','required');
			if ($this->form_validation->run() == true) {
				$color = $this->input->post('color_product');
				if ($color != null) {
					$inputColor = implode(',',$this->input->post('color_product'));
				}else{
					$inputColor = null;
				}
				$gia_mettoi = implode('',explode(',', $this->input->post('price_02_product')));
				$gia_metvuong = implode('',explode(',', $this->input->post('price_product')));
				$param = array(
					'Name_P'			=> $this->input->post('name_product'),
					'Name_Slug_P'		=> to_slug($this->input->post('name_product')),
					'Price_P'			=> $gia_metvuong,
					'Price_2_P'			=> $gia_mettoi,
					'Code_P'			=> $this->input->post('code_product'),
					'Category_P'		=> $this->input->post('category_product'),
					'Color_P'			=> $inputColor,
					'Thumbnail_P'		=> $this->input->post('thumbnail_product'),
					'Short_Content_P'	=> $this->input->post('short_product'),
					'Full_Content_P'	=> $this->input->post('full_product'),
					'Keywords_P'		=> $this->input->post('keyword_product'),
					'Description_P'		=> $this->input->post('description_product'),
					'Public_P'			=> $this->input->post('public_product'),
					'Status_P'			=> $this->input->post('status_product'),
					'Modify_P'			=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Product_BE->update_product($id,$param);
				if ($query == true) {
					$album = $this->input->post('albumThumbnail');
					if(!empty($album)){	
						foreach ($album as $value) {
							$check_thumbnail = $this->LP_Product_BE->check_thumbnail($value);
							if ($check_thumbnail == 0) {
								$param_thumb = array(
									'Thumbnail_link_Tn'	=> $value,
									'Product_Tn'		=> $id,
									'Createday_Tn'		=> date('Y-m-d H:i:s'),
									'Modify_Tn'			=> date('Y-m-d H:i:s')
								);
								$this->LP_Product_BE->add_thumbnail($param_thumb);
							}
						}
					}
					// Thêm tag
					$tags = $this->input->post('tag_product');
					if ($tags != null) {
						foreach (explode(',', $tags) as $value) {
							$check_tag = $this->LP_Product_BE->check_tag($value);
							if ($check_tag >= 1) {
								$get_tag = $this->LP_Product_BE->get_info_tag($value);
								foreach ($get_tag as $value2) {
									$tags_array[] = $value2['ID_Tag'];
								}
							}elseif($check_tag == 0){
								$param_tag = array(
									'Name_Tag'			=> $value,
									'Name_Slug_Tag'		=> to_slug($value),
									'Createday_Tag'		=> date('Y-m-d H:i:s'),
									'Modify_Tag'		=> date('Y-m-d H:i:s')
								);
								$insert_tag = $this->LP_Product_BE->insert_tag($param_tag);
								$tags_array[] = $insert_tag;
							}
						}
						$data_insert_tag = implode(',', $tags_array);
					}else{
						$data_insert_tag = null;
					}
					$param_02 = array('Tags_P' => $data_insert_tag);
					$query_02 = $this->LP_Product_BE->update_product($id,$param_02);
					$message = '<p class="text-success text-center">Chỉnh sửa sản phẩm thành công</p>';
					$this->session->set_flashdata('message',$message);
					// header('Location: '.base_url('/admin/product'));
				}else{
					$message = '<p class="text-warning text-center">Chỉnh sửa sản phẩm thất bại</p>';
					$this->session->set_flashdata('message',$message);
					// header('Location: '.base_url('/admin/product'));
				}
			}
		}
		$data['detail_product'] = $this->LP_Product_BE->get_detail_product($id);
		if(!empty($data['detail_product']['Tags_P'])){
			foreach (explode(',', $data['detail_product']['Tags_P']) as $item) {
				$get_name_tag = $this->LP_Product_BE->get_id_tag($item);
				foreach ($get_name_tag as $item2) {
					$data_name_tag[] =  $item2['Name_Tag'];
				}
			}
			$data['data_name_tag'] = implode(',', $data_name_tag);
		}else{
			$data['data_name_tag'] = null;
		}
		
		$data['color_check'] = explode(',', $data['detail_product']['Color_P']);
		$data['get_thumbnail'] = $this->LP_Product_BE->get_thumbnails($id);
		$data['get_list_category'] = $this->LP_Product_BE->get_list_category();
		$data['get_list_color'] = $this->LP_Product_BE->get_list_color();
		$this->load->template('backend','product/edit',isset($data)?$data:null);
	}
	public function category()
	{
		$data['header_page'] = 'Danh mục sản phẩm';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', '/admin/home');
		$this->breadcrumbs->push('Tất cả sản phẩm', '/admin/product');
		$this->breadcrumbs->push('Danh mục sản phẩm', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		$data['position'] = array(
			'' 				=> 'Không có vị trí',
			'home_bottom' 	=> 'Phần dưới Trang chủ',
			'home_footer'	=> 'Chân website'
		);
		/*Thêm danh mục*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_catproduct','tên danh mục','trim|required');
			if ($this->form_validation->run() == true) {
				$param = array(
					'Name_CatP'			=> $this->input->post('name_catproduct'),
					'Name_Slug_CatP'	=> to_slug($this->input->post('name_catproduct')),
					'Thumbnail_CatP'	=> $this->input->post('singleThumbnail'),
					'Keywords_CatP'		=> $this->input->post('keywords_catproduct'),
					'Description_CatP'	=> $this->input->post('description_catproduct'),
					'Position_CatP'		=> $this->input->post('position_catproduct'),
					'Sort_CatP'			=> $this->input->post('sort_catproduct'),
					'Createday_CatP'	=> date('Y-m-d H:i:s'),
					'Modify_CatP'		=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Product_BE->add_category($param);
				if ($query == true) {
					$data['message'] = '<p class="text-success">Thêm danh mục mới thành công</p>';
				}else{
					$data['message'] = '<p class="text-warning">Thêm danh mục mới thất bại</p>';
				}
			}
		}
		/*Xóa danh mục*/
		if (isset($_POST['btnDelete'])) {
			$this->form_validation->set_rules('id_cat','id danh mục','trim|required');
			if ($this->form_validation->run() == true) {
				$query = $this->LP_Product_BE->delete_category($this->input->post('id_cat'));
				if ($query == true) {
					$data['message'] = '<p class="text-success">Xóa danh mục thành công</p>';
				}else{
					$data['message'] = '<p class="text-warning">Xóa danh mục thất bại</p>';
				}
			}
		}

		$data['get_list_category'] = $this->LP_Product_BE->get_list_category();
		$this->load->template('backend','category-product/list',isset($data)?$data:null);
	}
	public function editcategory($id = null)
	{
		$data['header_page'] = 'Sửa danh mục';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', '/admin/home');
		$this->breadcrumbs->push('Tất cả sản phẩm', '/admin/product');
		$this->breadcrumbs->push('Danh mục', '/admin/product/category');
		$this->breadcrumbs->push('Sửa danh mục', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		$data['position'] = array(
			'' 				=> 'Không có vị trí',
			'home_bottom' 	=> 'Phần dưới Trang chủ',
			'home_footer'	=> 'Chân website'
		);

		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_catproduct','tên danh mục','trim|required');
			if ($this->form_validation->run() == true) {
				$param = array(
					'Name_CatP'			=> $this->input->post('name_catproduct'),
					'Name_Slug_CatP'	=> to_slug($this->input->post('name_catproduct')),
					'Thumbnail_CatP'	=> $this->input->post('singleThumbnail'),
					'Keywords_CatP'		=> $this->input->post('keywords_catproduct'),
					'Description_CatP'	=> $this->input->post('description_catproduct'),
					'Position_CatP'		=> $this->input->post('position_catproduct'),
					'Sort_CatP'			=> $this->input->post('sort_catproduct'),
					'Modify_CatP'		=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Product_BE->update_category($id,$param);
				if ($query == true) {
					$data['message'] = '<p class="text-success">Cập nhật danh mục thành công</p>';
				}else{
					$data['message'] = '<p class="text-warning">Cập nhật danh mục thất bại</p>';
				}
			}
		}

		if (isset($id)) {
			$data['detail_category'] = $this->LP_Product_BE->get_detail_category($id);
		}else{
			redirect(base_url('admin/home/error'));
		}
		
		$this->load->template('backend','category-product/edit',isset($data)?$data:null);
	}
	
	public function color()
	{
		$data['header_page'] = 'Màu sắc';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', '/admin/home');
		$this->breadcrumbs->push('Tất cả sản phẩm', '/admin/product');
		$this->breadcrumbs->push('Màu sắc', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Thêm màu sắc*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_color','tên màu','trim|required');
			$this->form_validation->set_rules('code_color','mã màu','trim|required');
			if ($this->form_validation->run() == true) {
				$param = array(
					'Name_Color'			=> $this->input->post('name_color'),
					'Name_Slug_Color'		=> to_slug($this->input->post('name_color')),
					'Code_Color'			=> $this->input->post('code_color'),
					'Keywords_Color'		=> $this->input->post('keywords_color'),
					'Description_Color'		=> $this->input->post('description_color'),
					'Createday_Color'		=> date('Y-m-d H:i:s'),
					'Modify_Color'			=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Product_BE->add_color($param);
				if ($query == true) {
					$data['message'] = '<p class="text-success">Thêm màu sắc thành công</p>';
				}else{
					$data['message'] = '<p class="text-warning">Thêm màu sắc thất bại</p>';
				}
			}
		}
		/*Xóa màu*/
		if (isset($_POST['btnDelete'])) {
			$this->form_validation->set_rules('id_color','id màu','trim|required');
			if ($this->form_validation->run() == true) {
				$query = $this->LP_Product_BE->delete_color($this->input->post('id_color'));
				if ($query == true) {
					$data['message'] = '<p class="text-success">Xóa danh mục thành công</p>';
				}else{
					$data['message'] = '<p class="text-warning">Xóa danh mục thất bại</p>';
				}
			}
		}
		$data['get_list_color'] = $this->LP_Product_BE->get_list_color();
		$this->load->template('backend','color/list',isset($data)?$data:null);
	}
	public function editcolor($id)
	{
		$data['header_page'] = 'Chỉnh sửa màu';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', '/admin/home');
		$this->breadcrumbs->push('Tất cả sản phẩm', '/admin/product');
		$this->breadcrumbs->push('Màu sắc', '/admin/product/color');
		$this->breadcrumbs->push('Chỉnh sửa màu', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_color','tên màu','trim|required');
			$this->form_validation->set_rules('code_color','mã màu','trim|required');
			if ($this->form_validation->run() == true) {
				$param = array(
					'Name_Color'			=> $this->input->post('name_color'),
					'Name_Slug_Color'		=> to_slug($this->input->post('name_color')),
					'Code_Color'			=> $this->input->post('code_color'),
					'Keywords_Color'		=> $this->input->post('keywords_color'),
					'Description_Color'		=> $this->input->post('description_color'),
					'Modify_Color'			=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Product_BE->update_color($id,$param);
				if ($query == true) {
					$data['message'] = '<p class="text-success">Cập nhật màu sắc thành công</p>';
				}else{
					$data['message'] = '<p class="text-warning">Cập nhật màu sắc thất bại</p>';
				}
			}
		}

		$data['detail_color'] = $this->LP_Product_BE->get_detail_color($id);
		$this->load->template('backend','color/edit',isset($data)?$data:null);
	}
	public function ajax_product()
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
			$draw = intval($this->input->get("draw"));
	      	$start = intval($this->input->get("start"));
	      	$length = intval($this->input->get("length"));

	      	$query = $this->db->get("Products");

	      	$data = [];

	      	foreach($query->result() as $row) {
	      		$category = $this->LP_Product_BE->get_category($row->Category_P);
	           	$data[] = array(
	                $row->ID_P,
	                '<a href="'.base_url('/admin/product/edit/'.$row->ID_P).'"><img src="'.thumbnail_product($row->Thumbnail_P).'" class="img-fluid" width="100" height="100"></a>',
	                '<a href="'.base_url('/admin/product/edit/'.$row->ID_P).'">'.$row->Name_P.'</a>',
	                $category['Name_CatP'],
	                price($row->Price_P,$this->setting[13],$this->setting[14]),
	                $row->Code_P,
	                check_product($row->Status_P),
	                public_item($row->Public_P), 
	                $row->View_P,         
	                '<a href="'.base_url('/admin/product/edit/'.$row->ID_P).'" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
	                <a href="" class="btn btn-danger btn-sm"  onclick="get_id('.$row->ID_P.')" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
	           	);
	      	}

	      	$result = array(
	           	"draw" => $draw,
	            "recordsTotal" => $query->num_rows(),
	            "recordsFiltered" => $query->num_rows(),
	            "data" => $data
	        );

	      	echo json_encode($result);
	      	exit();
	    }else{
	    	echo "Trang không tồn tại";
	    }
	}
	public function ajax_live_tag()
	{
		$tag = $this->input->get('tag_product');
		
	}
	public function tags_json()
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
			$this->db->select('Name_Tag');
			$query = $this->db->get('Tags')->result_array();
			$data = array();
			foreach ($query as $item) {
				$data[] = $item['Name_Tag'];
			}
			echo json_encode($data);
			die();
		}else{
	    	echo "Trang không tồn tại";
	    }
	}
	public function remove_thumbnail($id_product,$id_thumb)
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
			if (isset($id_product) && isset($id_thumb)) {
				$this->db->where('Product_Tn',$id_product);
				$this->db->where('ID_Tn',$id_thumb);
				$query = $this->db->delete('Thumbnails');
				if ($query == true) {
					echo "OK";
				}
			}
		}else{
	    	echo "Trang không tồn tại";
	    	die();
	    }
	}
}