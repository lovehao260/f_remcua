<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Ads extends CI_Controller
{
	
	public $setting;
	public $position;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		$check_login = $this->session->userdata('LoginSuccess');
		if (!isset($check_login)) {
			redirect(base_url('/admin/login'));
		}
		$this->position = array(
			'category_product'		=> 'category_product',
			'category_post'			=> 'category_post',
			'category_product_top'	=> 'category_product_top'
		);
	}
	public function index()
	{
		$data['header_page'] = 'Quảng cáo';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Quảng cáo', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		if (isset($_POST['btnDelete'])) {
			$this->form_validation->set_rules('id_ads','id quảng cáo','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$id_ads = $this->input->post('id_ads');
				$query = $this->LP_Ads_BE->delete_ads($id_ads);
				if ($query == true) {
					$message = '<p class="text-success">Xóa quảng cáo thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning">Xóa quảng cáo thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}
		}

		$data['get_ads'] = $this->LP_Ads_BE->get_ads();
		$this->load->template('backend','ads/list',isset($data)?$data:null);
	}
	public function add()
	{
		$data['header_page'] = 'Quảng cáo';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Quảng cáo', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_ads','tên quảng cáo','trim|required');
			$this->form_validation->set_rules('linkads_ads','liên kết tĩnh','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$param = array(
					'Name_Ads'		=> $this->input->post('name_ads'),
					'Image_Ads'		=> $this->input->post('image_ads'),
					'Link_Ads'		=> $this->input->post('linkads_ads'),
					'Position_Ads'	=> $this->input->post('position_ads'),
					'Click_Ads'		=> 0,
					'Sort_Ads'		=> $this->input->post('sort_ads'),
					'Public_Ads'	=> $this->input->post('public_ads'),
					'Createday_Ads'	=> date('Y-m-d H:i:s'),
					'Modify_Ads'	=> date('Y-m-d H:i:s')
				);	
				$query = $this->LP_Ads_BE->add_ads($param);
				if ($query == true) {
					$message = '<p class="text-success">Tạo quảng cáo thành công</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/ads'));
				}else{
					$message = '<p class="text-warning">Tạo quảng cáo thất bại</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/ads/add'));
				}
			}
		}

		$this->load->template('backend','ads/add',isset($data)?$data:null);
	}
	public function edit($id)
	{
		$data['header_page'] = 'Chỉnh sửa quảng cáo';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Quảng cáo', 'admin/ads');
		$this->breadcrumbs->push('Chỉnh sửa quảng cáo', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_ads','tên quảng cáo','trim|required');
			$this->form_validation->set_rules('linkads_ads','liên kết tĩnh','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$param = array(
					'Name_Ads'		=> $this->input->post('name_ads'),
					'Image_Ads'		=> $this->input->post('image_ads'),
					'Link_Ads'		=> $this->input->post('linkads_ads'),
					'Position_Ads'	=> $this->input->post('position_ads'),
					'Sort_Ads'		=> $this->input->post('sort_ads'),
					'Public_Ads'	=> $this->input->post('public_ads'),
					'Modify_Ads'	=> date('Y-m-d H:i:s')
				);	
				$query = $this->LP_Ads_BE->update_ads($id,$param);
				if ($query == true) {
					$message = '<p class="text-success">Chỉnh sửa quảng cáo thành công</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/ads'));
				}else{
					$message = '<p class="text-warning">Chỉnh sửa quảng cáo thất bại</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/ads/edit/'.$id));
				}
			}
		}
		$data['detail_ads'] = $this->LP_Ads_BE->get_detail_ads($id);
		$this->load->template('backend','ads/edit',isset($data)?$data:null);
	}
}