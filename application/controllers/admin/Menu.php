<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Menu extends CI_Controller
{
	
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		$check_login = $this->session->userdata('LoginSuccess');
		if (!isset($check_login)) {
			redirect(base_url('/admin/login'));
		}
	}
	public function index()
	{
		$data['header_page'] = 'Menu';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Menu', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		$data['title_card'] = 'Thêm menu mới';

		/*Thêm menu*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_menu','tên menu','trim|required');
			if ($this->form_validation->run() == true) {
				$parent = $this->input->post('parent_menu');
				if (empty($parent)) {
					$parent = NULL;
				}
				$param = array(
					'Name_Menu'			=> $this->input->post('name_menu'),
					'Link_Menu'			=> $this->input->post('link_cus_menu'),
					'Icon_Menu'			=> $this->input->post('icon_menu'),
					'Parent_Menu'		=> $parent,
					'Sort_Menu'			=> $this->input->post('sort_menu'),
					'Public_Menu'		=> $this->input->post('public_menu'),
					'Createday_Menu'	=> date('Y-m-d H:i:s'),
					'Modify_Menu'		=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Menu_BE->add_menu($param);
				if ($query == true) {
					$message = '<p class="text-success">Thêm menu thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning">Thêm menu thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}
		}
		/*Xóa menu*/
		if (isset($_POST['btnDelete'])) {
			$this->form_validation->set_rules('id_menu','id menu','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$id_menu = $this->input->post('id_menu');
				$check = $this->LP_Menu_BE->check_menu($id_menu);
				// print_r($check); die();
				if ($check > 1) {
					$message = '<p class="text-warning">Bạn chưa xóa menu con</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$query = $this->LP_Menu_BE->delete_menu($id_menu);
					if ($query == true) {
						$message = '<p class="text-success">Xóa menu thành công</p>';
						$this->session->set_flashdata('message',$message);
					}else{
						$message = '<p class="text-warning">Xóa menu thất bại</p>';
						$this->session->set_flashdata('message',$message);
					}
				}
				
			}	
		}

		$data['get_cat_product'] = $this->LP_Menu_BE->get_cat_product();
		$data['get_cat_post'] = $this->LP_Menu_BE->get_cat_post();
		$data['get_page'] = $this->LP_Menu_BE->get_page();

		$menu = $this->LP_Menu_BE->get_menu();
		$data['menu_dropdown'] = $this->my_lib->dequy_menu(null,$menu,'');
		$data['menu_list'] = $this->my_lib->dequy_menu(null,$menu,'','view');
		$this->load->template('backend','menu/list',isset($data)?$data:null);
	}
	public function edit($id)
	{
		$data['header_page'] = 'Chỉnh sửa menu';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Menu', 'admin/menu');
		$this->breadcrumbs->push('Chỉnh sửa menu', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		$data['title_card'] = 'Chỉnh sửa menu';

		/*Sửa menu*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_menu','tên menu','trim|required');
			if ($this->form_validation->run() == true) {
				$parent = $this->input->post('parent_menu');
				if ($parent == null) {
					$parent = NULL;
				}
				$param = array(
					'Name_Menu'			=> $this->input->post('name_menu'),
					'Link_Menu'			=> $this->input->post('link_cus_menu'),
					'Icon_Menu'			=> $this->input->post('icon_menu'),
					'Parent_Menu'		=> $parent,
					'Sort_Menu'			=> $this->input->post('sort_menu'),
					'Public_Menu'		=> $this->input->post('public_menu'),
					'Modify_Menu'		=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Menu_BE->update_menu($id,$param);
				if ($query == true) {
					$message = '<p class="text-success">Chỉnh sửa menu thành công</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/menu'));
				}else{
					$message = '<p class="text-warning">Chỉnh sửa menu thất bại</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/menu'));
				}
			}
		}

		$data['get_cat_product'] = $this->LP_Menu_BE->get_cat_product();
		$data['get_cat_post'] = $this->LP_Menu_BE->get_cat_post();
		$data['get_page'] = $this->LP_Menu_BE->get_page();
		$data['detail_menu'] = $this->LP_Menu_BE->get_detail_menu($id);

		$menu = $this->LP_Menu_BE->get_menu();
		$data['menu_dropdown'] = $this->my_lib->dequy_menu(null,$menu,'');
		$data['menu_list'] = $this->my_lib->dequy_menu(null,$menu,'','view');
		$this->load->template('backend','menu/edit',isset($data)?$data:null);
	}
	
}