<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Users extends CI_Controller
{
	
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		$check_login = $this->session->userdata('LoginSuccess');
		if (!isset($check_login)) {
			redirect(base_url('/admin/login'));
		}
	}
	public function index()
	{
		$data['header_page'] = 'Tất cả thành viên';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả thành viên', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		/*Thêm liên hệ*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_user','họ và tên','trim|required');
			$this->form_validation->set_rules('username_user','tên đăng nhập','trim|required');
			$this->form_validation->set_rules('password_user','mật khẩu','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$username = $this->input->post('username_user');
				$check = $this->LP_Users_BE->check_username($username);
				if ($check >= 1) {
					$message = '<p class="text-warning">Tên đăng nhập đã tồn tại</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$param = array(
						'Name_U'		=> $this->input->post('name_user'),
						'Username_U'	=> $username,
						'Password_U'	=> hash_password($this->input->post('password_user')),
						'Email_U'		=> $this->input->post('email_user'),
						'Phone_U'		=> $this->input->post('phone_user'),
						'Active_U'		=> $this->input->post('public_user'),
						'Createday_U'	=> date('Y-m-d H:i:s'),
						'Lastlogin_U'	=> date('Y-m-d H:i:s'),
						'Modify_U'		=> date('Y-m-d H:i:s')
					);
					$query = $this->LP_Users_BE->add_user($param);
					if ($query == true) {
						$message = '<p class="text-success">Thêm thành viên thành công</p>';
						$this->session->set_flashdata('message',$message);
					}else{
						$message = '<p class="text-warning">Thêm thành viên thất bại</p>';
						$this->session->set_flashdata('message',$message);
					}
				}
			}
		}

		/*Xóa liên hệ*/
		if (isset($_POST['btnDelete'])) {
			$this->form_validation->set_rules('id_user','id thành viên','trim|required');
			if ($this->form_validation->run() == TRUE) {
				$id_user = $this->input->post('id_user');
				$query = $this->LP_Users_BE->delete_user($id_user);
				if ($query == true) {
					$message = '<p class="text-success">Xóa thành viên thành công</p>';
					$this->session->set_flashdata('message',$message);
				}else{
					$message = '<p class="text-warning">Xóa thành viên thất bại</p>';
					$this->session->set_flashdata('message',$message);
				}
			}	
		}

		$data['get_user'] = $this->LP_Users_BE->get_user();
		$this->load->template('backend','users/list',isset($data)?$data:null);
	}
	
	public function edit($id)
	{
		$data['header_page'] = 'Chỉnh sửa thành viên';
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0],$data['header_page'].' - Admin Control');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		/*Breadcrumbs*/
		$this->breadcrumbs->push('Bảng điều khiển', 'admin/home');
		$this->breadcrumbs->push('Tất cả thành viên', 'admin/users');
		$this->breadcrumbs->push('Chỉnh sửa thành viên', '/');
		$data['breadcrumb'] = $this->breadcrumbs->show();

		$data['title_card'] = 'Chỉnh sửa thành viên';

		/*Chỉnh sửa thành viên*/
		if (isset($_POST['btnSave'])) {
			$this->form_validation->set_rules('name_user','họ và tên','trim|required');
			if ($this->form_validation->run() == true) {
				$password = $this->input->post('password_user');
				if ($password != '') {
					$hash_password = hash_password($password);
					$changepass = $this->LP_Users_BE->update_user($id,array('Password_U' => $hash_password));
				}
				$param = array(
					'Name_U'		=> $this->input->post('name_user'),
					'Email_U'		=> $this->input->post('email_user'),
					'Phone_U'		=> $this->input->post('phone_user'),
					'Active_U'		=> $this->input->post('public_user'),
					'Modify_U'		=> date('Y-m-d H:i:s')
				);
				$query = $this->LP_Users_BE->update_user($id,$param);
				if ($query == true) {
					$message = '<p class="text-success">Chỉnh sửa thành viên thành công</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/users'));
				}else{
					$message = '<p class="text-warning">Chỉnh sửa thành viên thất bại</p>';
					$this->session->set_flashdata('message',$message);
					redirect(base_url('admin/users/edit/'.$id));
				}
			}
		}

		$data['detail_user'] = $this->LP_Users_BE->get_detail_user($id);

		$this->load->template('backend','users/edit',isset($data)?$data:null);
	}
}