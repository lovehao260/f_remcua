<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Page extends CI_Controller
{
	
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		/*Menu*/
		$items = $this->LP_Menu_FE->get_menu();
		$this->multi_menu->set_items($items);
	}
	public function index()
	{
		/*Pagination*/
		$config['base_url'] = base_url('page');
		$config['total_rows'] = $this->LP_Page_FE->total_rows();
		$config['per_page'] = 6;
		$config['num_links'] = 2;
		$config['uri_segment'] = 2;
		$offset = $this->uri->segment(2);
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		$data['get_page'] = $this->LP_Page_FE->get_page($config['per_page'],$offset);

		$data['meta_title'] = meta_title($this->setting[0],'Trang');
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		$this->load->template('frontend','all-page',isset($data)?$data:null);
	}
	public function view($id)
	{
		$data['detail_page'] = $this->LP_Page_FE->detail_page($id);

		$data['meta_title'] = meta_title($this->setting[0],$data['detail_page']['Title_Page']);
		$data['meta_keywords'] = meta_keywords($data['detail_page']['Keywords_Page']);
		$data['meta_description'] = meta_description($data['detail_page']['Description_Page']);
		$data['meta_author'] = meta_author($this->setting[3]);
		$this->LP_Page_FE->set_view($id);
		$this->load->template('frontend','page',isset($data)?$data:null);
	}
}

