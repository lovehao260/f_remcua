<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**

* 

*/

class Home extends CI_Controller

{

	public $setting;

	public function __construct()

	{

		parent::__construct();

		$this->setting = $this->LP_Setting_FE->get_setting();

		/*Menu*/

		$items = $this->LP_Menu_FE->get_menu();

		$this->multi_menu->set_items($items);

	}

	public function index()

	{

		/*Meta tags*/

		$data['meta_title'] = meta_title($this->setting[0]);

		$data['meta_keywords'] = meta_keywords($this->setting[1]);

		$data['meta_description'] = meta_description($this->setting[2]);

		$data['meta_author'] = meta_author($this->setting[3]);



		$data['carousel'] = $this->LP_Carousel_FE->get_list();

		$data['min_carousel'] = $this->LP_Carousel_FE->min_sort_col();

		$data['get_product'] = $this->LP_Product_FE->get_product_home(8);

		$data['get_post_home'] = $this->LP_Post_FE->get_post_home('home',9);

		$data['get_category_position'] = $this->LP_Product_FE->get_category_position('home_bottom');

		$data['get_page_position'] = $this->LP_Page_FE->get_page_position('home',4);

		$data['get_page_position_big'] = $this->LP_Page_FE->get_page_position('home_big',1);



		$this->load->template('frontend','home',isset($data)?$data:null);

	}

	public function error()

	{

		/*Meta tags*/

		$data['meta_title'] = meta_title($this->setting[0],'Trang lỗi');

		$data['meta_keywords'] = meta_keywords($this->setting[1]);

		$data['meta_description'] = meta_description($this->setting[2]);

		$data['meta_author'] = meta_author($this->setting[3]);



		$this->load->template('frontend','error',isset($data)?$data:null);

	}

	public function tinhgia($pheptinh = 'metvuong')

	{

		if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {

			echo "Trang không tồn tại";

			die();

		}

		$chieucao = $this->input->post('chieucao');

		$chieurong = $this->input->post('chieurong');

		$gia = $this->input->post('gia_thuc');
		$gia2 = str_replace(',','',$this->input->post('dongia'));

		$tinhtoan = congthuc($chieucao,$chieurong,$gia2,$pheptinh);

		echo price($tinhtoan,$this->setting[13]);

	}

	public function click_ads($id)

	{

		if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {

			echo "Trang không tồn tại";

			die();

		}

		if (isset($id)) {

			$query = $this->LP_Ads_FE->click_ads($id);

			if ($query == true) {

				echo "OK";

			}

		}

		

	}

}



