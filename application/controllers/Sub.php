<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Sub extends CI_Controller
{
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
	}
	public function index()
	{
		echo $this->agent->browser().' '.$this->agent->version();
	}
	public function add()
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
			$email = $this->input->post('email-sub');
			if($this->LP_Sub_FE->check_sub($email) >= 1){
				echo "Địa chỉ email của bạn đã đăng ký trước đây";
			}else{
				$param = array(
					'Email_Sub'		=>	$email,
					'Session_Sub'	=>	session_id(),
					'UserAgent_Sub'	=>	$this->agent->browser().' '.$this->agent->version(),
					'Platform_Sub'	=> 	$this->agent->platform(),
					'Createday_Sub'	=>	date('Y-m-d H:i:s'),
					'Modify_Sub'	=>	date('Y-m-d H:i:s')
				);
				$query = $this->LP_Sub_FE->add_sub($param);
				if ($query == true) {
					echo '<p>Đăng ký email thành công</p>';
				}else{
					echo '<p>Thất bại, liên hệ: '.$this->setting[4].'</p>';
				}
			}
		}else{
			echo "Trang không tồn tại!";
		}
	}
}