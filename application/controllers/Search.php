<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Search extends CI_Controller
{
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->setting = $this->LP_Setting_FE->get_setting();
		/*Menu*/
		$items = $this->LP_Menu_FE->get_menu();
		$this->multi_menu->set_items($items);
	}
	public function index()
	{
		/*Meta tags*/
		$data['meta_title'] = meta_title($this->setting[0]);
		$data['meta_keywords'] = meta_keywords($this->setting[1]);
		$data['meta_description'] = meta_description($this->setting[2]);
		$data['meta_author'] = meta_author($this->setting[3]);

		$data['key'] = $this->input->get('tu-khoa');
		if (isset($data['key'])) {
			$count = $this->LP_Search_FE->count_product_search($data['key']);
			/*Pagination*/
			$config['base_url'] = base_url('tim-kiem');
			$config['total_rows'] = $count;
			$config['per_page'] = 12;
			$config['num_links'] = 2;
			$config['uri_segment'] = 2;
			$config['reuse_query_string'] = TRUE;
			$offset = $this->uri->segment(2);
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['get_product'] = $this->LP_Search_FE->search_product($data['key'],$config['per_page'],$offset);
		}else{
			$data['get_product'] = null;
		}
		// echo md5(sha1(md5('admin')));
		$this->load->template('frontend','search',isset($data)?$data:null);
	}
	public function error()
	{
		$this->load->template('frontend','error',isset($data)?$data:null);
	}
}

