<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Setting_FE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_setting()
	{
		$temp = array();
		$query = $this->db->get('Settings')->result_array();
		foreach ($query as $key => $value) {
			$temp[] = $value['Value_Setting'];
		}
		return $temp;
	}
}