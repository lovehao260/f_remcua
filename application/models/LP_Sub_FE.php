<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Sub_FE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function add_sub($param = array())
	{
		if (isset($param)) {
			return $this->db->insert('Subscribers',$param);
		}
		return null;
	}
	function check_sub($email)
	{
		if (isset($email)) {
			$this->db->where('Email_Sub',$email);
			return $this->db->count_all_results('Subscribers');
		}
		return null;
	}
}