<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Login_BE extends CI_Model
{
	
	function total($param_where = array())
	{
		$this->db->from('Users');
		if (isset($param_where) && count($param_where)) {
			$this->db->where($param_where);
		}
		return $this->db->count_all_results();
	}
	function get($select = 'Username_U', $param_where = null)
	{
		$this->db->select($select)->from('Users');
		if (isset($param_where) && count($param_where)) {
			$this->db->where($param_where);
		}
		return $this->db->get()->row_array();
	}
	function update_lastlogin($id,$param = array())
	{
		if (isset($id) && count($id)) {
			$this->db->where('ID_U',$id);
			return $this->db->update('Users',$param);
		}
	}
}
