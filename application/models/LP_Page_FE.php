<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Page_FE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_page($perpage,$offset)
	{
			$this->db->where('Public_Page','Y');
			$this->db->limit($perpage,$offset);
			return $this->db->get('Pages')->result_array();
	}
	function total_rows()
	{
		$this->db->where('Public_Page','Y');
		return $this->db->get('Pages')->num_rows();
	}
	function detail_page($id)
	{
		if (isset($id)) {
			$this->db->where('Public_Page','Y');
			$this->db->where('ID_Page',$id);
			$this->db->limit(1);
			return $this->db->get('Pages')->row_array();
		}
		return null;
	}
	function get_page_position($position,$limit)
	{
		if (isset($position)) {
			$this->db->where('Public_Page','Y');
			$this->db->where('Position_Page',$position);
			$this->db->order_by('Sort_Page','ASC');
			$this->db->limit($limit);
			return $this->db->get('Pages')->result_array();
		}
		return null;
	}
	function set_view($id)
	{
		$query = 'UPDATE Pages SET Views_Page = Views_Page + 1 WHERE ID_Page ='.$id;
		return $this->db->query($query);
	}
}