<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Search_FE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function search_product($key,$perpage,$offset)
	{
		if (isset($key)) {
			$this->db->like('Name_P',$key);
			$this->db->or_like('Name_Slug_P',$key);
			$this->db->or_like('Code_P',$key);
			$this->db->or_like('Short_Content_P',$key);
			$this->db->or_like('Full_Content_P',$key);
			$this->db->or_like('Keywords_P',$key);
			$this->db->or_like('Description_P',$key);
			$this->db->limit($perpage,$offset);
			return $this->db->get('Products')->result_array();
		}
		return null;
	}
	function count_product_search($key)
	{
		if (isset($key)) {
			$this->db->like('Name_P',$key);
			$this->db->or_like('Name_Slug_P',$key);
			$this->db->or_like('Code_P',$key);
			$this->db->or_like('Short_Content_P',$key);
			$this->db->or_like('Full_Content_P',$key);
			$this->db->or_like('Keywords_P',$key);
			$this->db->or_like('Description_P',$key);
			return $this->db->count_all_results('Products');
		}
		return null;
	}
	function search_posts($key,$perpage,$offset)
	{
		if (isset($key)) {
			$this->db->like('Title_Post',$key);
			$this->db->or_like('Title_Slug_Post',$key);
			$this->db->or_like('Keywords_Post',$key);
			$this->db->or_like('Description_Post',$key);
			$this->db->limit($perpage,$offset);
			return $this->db->get('Posts')->result_array();
		}
		return null;
	}
	function count_posts_search($key)
	{
		if (isset($key)) {
			$this->db->like('Title_Post',$key);
			$this->db->or_like('Title_Slug_Post',$key);
			$this->db->or_like('Keywords_Post',$key);
			$this->db->or_like('Description_Post',$key);
			return $this->db->count_all_results('Posts');
		}
		return null;
	}
}