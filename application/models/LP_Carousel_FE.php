<?php
/**
* 
*/
class LP_Carousel_FE extends CI_Model
{
	
	function get_list()
	{
		$this->db->where('Public_Slide','Y');
		$this->db->order_by('Sort_Slide','ASC');
		return $this->db->get('Slider')->result_array();
	}
	function min_sort_col()
	{
		$this->db->select_min('Sort_Slide');
		return $this->db->get('Slider')->row_array();
	}
}