<?php
/**
* 
*/
class LP_Post_FE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_post($perpage,$offset)
	{
		$this->db->where('Public_Post','Y');
		$this->db->limit($perpage,$offset);
		$this->db->order_by('ID_Post','DESC');
		return $this->db->get('Posts')->result_array();
	}
	function total_rows()
	{
		$this->db->where('Public_Post','Y');
		return $this->db->get('Posts')->num_rows();
	}
	function category_post($id,$perpage,$offset)
	{
		if (isset($id)) {
			$this->db->where('Public_Post','Y');
			$this->db->where('ID_Cat_Post',$id);
			$this->db->order_by('ID_Post','DESC');
			$this->db->limit($perpage,$offset);
			return $this->db->get('Posts')->result_array();
		}
		return null;
	}
	function total_rows_category($id)
	{
		if (isset($id)) {
			$this->db->where('Public_Post','Y');
			$this->db->where('ID_Cat_Post',$id);
			return $this->db->get('Posts')->num_rows();
		}
		return null;
	}
	function get_post_limit($limit)
	{
		if (isset($limit)) {
			$this->db->where('Public_Post','Y');
			$this->db->order_by('ID_Post','DESC');
			$this->db->limit($limit);
			return $this->db->get('Posts')->result_array();
		}
		return null;
	}
	function detail_post($id)
	{
		if (isset($id)) {
			$this->db->where('Public_Post','Y');
			$this->db->where('ID_Post',$id);
			return $this->db->get('Posts')->row_array();
		}
		return null;
	}
	function related_post($id)
	{
		if (isset($id)) {
			$this->db->where('Public_Post','Y');
			$this->db->where('ID_Cat_Post',$id);
			$this->db->order_by('ID_Post','ASC');
			$this->db->limit(9);
			return $this->db->get('Posts')->result_array();
		}
		return null;
	}
	function get_category()
	{
		$this->db->where('Public_CPost','Y');
		$this->db->order_by('Sort_CPost','ASC');
		return $this->db->get('Categorys_Post')->result_array();
	}
	function detail_category($id)
	{
		if (isset($id)) {
			$this->db->where('ID_CPost',$id);
			return $this->db->get('Categorys_Post')->row_array();
		}
		return null;
	}
	function get_post_home($position,$limit)
	{
		if (isset($position)) {
			$this->db->where('Public_Post','Y');
			$this->db->where('Position_Post',$position);
			$this->db->order_by('ID_Post','DESC');
			$this->db->limit($limit);
			return $this->db->get('Posts')->result_array();
		}
		return null;
	}
	function set_view($id)
	{
		$query = 'UPDATE Posts SET Views_Post = Views_Post + 1 WHERE ID_Post ='.$id;
		return $this->db->query($query);
	}
}