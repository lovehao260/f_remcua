<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Menu_FE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_menu()
	{
		$this->db->where('Public_Menu','Y');
		$this->db->order_by('Sort_Menu','ASC');
		return $this->db->get('Menu')->result_array();
	}
	
}