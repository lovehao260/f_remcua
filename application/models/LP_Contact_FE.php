<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Contact_FE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function add_contact($param = array())
	{
		if (isset($param)) {
			return $this->db->insert('Contacts',$param);
		}
		return null;
	}
	
}