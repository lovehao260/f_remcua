<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Product_FE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_color()
	{
		$this->db->order_by('ID_Color','DESC');
		return $this->db->get('Color')->result_array();
	}
	function get_category()
	{
		$this->db->order_by('Sort_CatP','ASC');
		return $this->db->get('Categorys_Product')->result_array();
	}
	function get_all_product()
	{
		$this->db->where('Public_P','Y');
		$this->db->order_by('ID_P','DESC');
		$this->db->limit(12);
		return $this->db->get('Products')->result_array();
	}
	function set_view_product($id)
	{
		if (isset($id)) {
			$this->db->set('View_P','View_P + 1',FALSE);
			$this->db->where('ID_P',$id);
			$this->db->update('Products');
		}
	}
	function get_product_home($limit)
	{
		$this->db->where('Public_P','Y');
		$this->db->order_by('ID_P','DESC');
		$this->db->limit($limit);
		return $this->db->get('Products')->result_array();
	}
	function get_name_category($id)
	{
		if (isset($id)) {
			$this->db->where('ID_CatP',$id);
			return $this->db->get('Categorys_Product')->row_array();
		}
		return null;
	}
	function get_name_color($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Color',$id);
			return $this->db->get('Color')->row_array();
		}
		return null;
	}
	function detail_product($id_product)
	{
		if(isset($id_product)){
			$this->db->where('Public_P','Y');
			$this->db->where('ID_P',$id_product);
			$this->db->limit(1);
			return $this->db->get('Products')->row_array();
		}
	}
	function get_product($id_category = null,$id_color = null,$perpage,$offset)
	{
		if (isset($id_category)) {
			$this->db->where('Public_P','Y');
			$this->db->where('Category_P',$id_category);
			$this->db->limit($perpage,$offset);
			$this->db->order_by('ID_P','DESC');
			return $this->db->get('Products')->result_array();
		}elseif(isset($id_color)){
			$this->db->where('Public_P','Y');
			$this->db->like('Color_P',$id_color);
			$this->db->limit($perpage,$offset);
			$this->db->order_by('ID_P','DESC');
			return $this->db->get('Products')->result_array();
		}else{
			$this->db->where('Public_P','Y');
			$this->db->limit($perpage,$offset);
			$this->db->order_by('ID_P','DESC');
			return $this->db->get('Products')->result_array();
		}
	}
	function get_thumbnail($id)
	{
		if (isset($id)) {
			$this->db->where('Product_Tn',$id);
			return $this->db->get('Thumbnails')->result_array();
		}
	}
	function get_color_detail($color)
	{
		if (isset($color)) {
			$count = count($color);
			foreach ($color as $item) {
			 	if ($count >= 1) {
			 		$this->db->or_like('ID_Color',$item);
			 	}
			}
			return $this->db->get('Color')->result_array();
		}
		return null;
	}
	function related_product($id)
	{
		if (isset($id)) {
			$this->db->where('Public_P','Y');
			$this->db->where('Category_P',$id);
			$this->db->order_by('ID_P','ASC');
			$this->db->limit(9);
			return $this->db->get('Products')->result_array();
		}
	}
	function total_rows()
	{
		return $this->db->get('Products')->num_rows();
	}
	function total_rows_category($id)
	{
		if (isset($id)) {
			$this->db->where('Category_P',$id);
			return $this->db->get('Products')->num_rows();
		}
	}
	function total_rows_color($id)
	{
		if (isset($id)) {
			$this->db->like('Color_P',$id);
			return $this->db->get('Products')->num_rows();
		}
	}
	function get_category_position($position)
	{
		if (isset($position)) {
			$this->db->where('Position_CatP',$position);
			$this->db->order_by('Sort_CatP','ASC');
			$this->db->limit(6);
			return $this->db->get('Categorys_Product')->result_array();
		}
	}
	function get_tags()
	{
		return $this->db->get('Tags')->result_array();
	}
	function get_detail_tags($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Tag',$id);
			return $this->db->get('Tags')->row_array();
		}
	}
	function count_product_tag($id_tag)
	{
		if (isset($id_tag)) {
			$this->db->where('Public_P','Y');
			$this->db->like('Tags_P',$id_tag);
			return $this->db->get('Products')->num_rows();
		}
	}
	function get_product_tag($id_tag,$perpage,$offset)
	{
		if (isset($id_tag)) {
			$this->db->where('Public_P','Y');
			$this->db->like('Tags_P',$id_tag);
			$this->db->limit($perpage,$offset);
			$this->db->order_by('ID_P','DESC');
			return $this->db->get('Products')->result_array();
		}
	}
	function get_tag_detail($id)
	{
		if (isset($id)) {
			$count = count($id);
			if ($count >= 1) {
				$this->db->where_in('ID_Tag',$id);
				return $this->db->get('Tags')->result_array();
			}
			return null;
		}
		return null;
	}
	function set_view($id)
	{
		$query = 'UPDATE Products SET View_P = View_P + 1 WHERE ID_P ='.$id;
		return $this->db->query($query);
	}
}
