<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Setting_BE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function update_setting($where,$value)
	{
		if (isset($where) && isset($value)) {
			$this->db->where('Key_Setting',$where);
			return $this->db->update('Settings',array('Value_Setting' => $value));
		}
	}
}