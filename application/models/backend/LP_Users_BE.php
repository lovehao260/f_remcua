<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Users_BE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_user()
	{
		return $this->db->get('Users')->result_array();
	}
	function get_detail_user($id)
	{
		if (isset($id)) {
			$this->db->where('ID_U',$id);
			return $this->db->get('Users')->row_array();
		}
	}
	function check_username($username)
	{
		if (isset($username)) {
			$this->db->where('Username_U',$username);
			return $this->db->count_all_results('Users');
		}
	}
	function add_user($param = array())
	{
		if (isset($param)) {
			return $this->db->insert('Users',$param);
		}
	}
	function update_user($id,$param = array())
	{
		if (isset($param) && isset($id)) {
			$this->db->where('ID_U',$id);
			return $this->db->update('Users',$param);
		}
	}
	function delete_user($id)
	{
		if (isset($id)) {
			$this->db->where('ID_U',$id);
			return $this->db->delete('Users');
		}
	}
}