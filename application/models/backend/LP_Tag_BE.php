<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Tag_BE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_detail_tag($id)
	{
		$this->db->where('ID_Tag',$id);
		return $this->db->get('Tags')->row_array();
	}
	function add_tag($param = array())
	{
		if (isset($param)) {
			return $this->db->insert('Tags',$param);
		}
	}
	function update_tag($id,$param = array())
	{
		if (isset($id) && isset($param)) {
			$this->db->where('ID_Tag',$id);
			return $this->db->update('Tags',$param);
		}
	}
	function delete_tag($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Tag',$id);
			return $this->db->delete('Tags');
		}
	}
}