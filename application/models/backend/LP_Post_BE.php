<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Post_BE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_post()
	{
		$this->db->select('ID_Post, Title_Post, ID_Cat_Post, Public_Post, Views_Post');
		$this->db->from('Posts');
		$this->db->order_by('ID_Post','DESC');
		return $this->db->get()->result_array();
	}
	function get_detail_post($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Post',$id);
			return $this->db->get('Posts')->row_array();
		}
	}
	function get_category($id)
	{
		if (isset($id)) {
			$this->db->where('ID_CPost',$id);
			return $this->db->get('Categorys_Post')->row_array();
		}
	}
	function get_list_category()
	{
		$this->db->order_by('ID_CPost','ASC');
		return $this->db->get('Categorys_Post')->result_array();
	}
	function get_list_category_modify()
	{
		$this->db->order_by('Modify_CPost','DESC');
		return $this->db->get('Categorys_Post')->result_array();
	}
	function add_post($param = array())
	{
		if (isset($param)) {
			$this->db->insert('Posts',$param);
			return $this->db->insert_id();
		}
	}
	function update_post($id,$param = array())
	{
		if (isset($id) && isset($param)) {
			$this->db->where('ID_Post',$id);
			return $this->db->update('Posts',$param);
		}
	}
	function delete_post($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Post',$id);
			return $this->db->delete('Posts');
		}
	}
	function add_category($param = array())
	{
		if (isset($param)) {
			return $this->db->insert('Categorys_Post',$param);
		}
	}
	function update_category($id,$param = array())
	{
		if (isset($id) && isset($param)) {
			$this->db->where('ID_CPost',$id);
			return $this->db->update('Categorys_Post',$param);
		}
	}
	function delete_category($id)
	{
		if (isset($id)) {
			$this->db->where('ID_CPost',$id);
			return $this->db->delete('Categorys_Post');
		}
	}
}