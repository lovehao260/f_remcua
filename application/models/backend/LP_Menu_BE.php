<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Menu_BE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_menu()
	{
		$this->db->order_by('Sort_Menu','ASC');
		return $this->db->get('Menu')->result_array();
	}
	function get_cat_product()
	{
		return $this->db->get('Categorys_Product')->result_array();
	}
	function get_cat_post()
	{
		return $this->db->get('Categorys_Post')->result_array();
	}
	function get_page()
	{
		return $this->db->get('Pages')->result_array();
	}
	function get_detail_menu($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Menu',$id);
			return $this->db->get('Menu')->row_array();
		}
	}
	function add_menu($param = array())
	{
		if (isset($param)) {
			return $this->db->insert('Menu',$param);
		}
	}
	function update_menu($id,$param = array())
	{
		if (isset($param) && isset($id)) {
			$this->db->where('ID_Menu',$id);
			return $this->db->update('Menu',$param);
		}
	}
	function delete_menu($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Menu',$id);
			return $this->db->delete('Menu');
		}
	}
	function check_menu($id)
	{
		if (isset($id)) {
			$this->db->where('Parent_Menu',$id);
			return $this->db->count_all_results('Menu');
		}
	}
	
}