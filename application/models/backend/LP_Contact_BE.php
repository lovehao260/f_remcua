<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Contact_BE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_contact()
	{
		$this->db->order_by('Createday_Contact','DESC');
		return $this->db->get('Contacts')->result_array();
	}
	function get_contact_home($limit)
	{
		$this->db->order_by('Createday_Contact','DESC');
		$this->db->limit($limit);
		return $this->db->get('Contacts')->result_array();
	}
	function get_contact_notification()
	{
		$this->db->order_by('Createday_Contact','DESC');
		$this->db->limit(5);
		return $this->db->get('Contacts')->result_array();
	}
	function get_detail_contact($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Contact',$id);
			return $this->db->get('Contacts')->row_array();
		}
	}
	function count_contact_new()
	{
		$this->db->where('Read_Contact','N');
		return $this->db->count_all_results('Contacts');
	}
	function set_read_contact($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Contact',$id);
			$query = $this->db->get('Contacts')->row_array();
			if ($query['Read_Contact'] == 'N') {
				$this->db->where('ID_Contact',$id);
				return $this->db->update('Contacts',array('Read_Contact' => 'Y'));
			}
			return null;
		}
		return false;
	}
	function add_contact($param = array())
	{
		if (isset($param)) {
			$this->db->insert('Contacts',$param);
			return $this->db->insert_id();
		}
	}
	function update_contact($id,$param = array())
	{
		if (isset($param) && isset($id)) {
			$this->db->where('ID_Contact',$id);
			return $this->db->update('Contacts',$param);
		}
	}
	function delete_contact($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Contact',$id);
			return $this->db->delete('Contacts');
		}
	}
	function get_subscribe()
	{
		return $this->db->get('Subscribers')->result_array();
	}
	function get_detail_subscribe($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Sub',$id);
			return $this->db->get('Subscribers')->row_array();
		}
	}
	function update_subscribe($id,$param = array())
	{
		if (isset($param) && isset($id)) {
			$this->db->where('ID_Sub',$id);
			return $this->db->update('Subscribers',$param);
		}
	}
	function delete_subscribe($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Sub',$id);
			return $this->db->delete('Subscribers');
		}
	}
}