<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Product_BE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_product()
	{
		$this->db->select('ID_P, Name_P, Price_P, Code_P, Category_P, Public_P, Status_P');
		$this->db->from('Products');
		$this->db->order_by('ID_P','DESC');
		return $this->db->get()->result_array();
	}
	function get_detail_product($id)
	{
		if (isset($id)) {
			$this->db->where('ID_P',$id);
			return $this->db->get('Products')->row_array();
		}
	}
	function get_category($id = null)
	{
		if (isset($id)) {
			$this->db->where('ID_CatP',$id);
			return $this->db->get('Categorys_Product')->row_array();
		}
	}
	function get_list_category()
	{
		$this->db->order_by('Modify_CatP','DESC');
		return $this->db->get('Categorys_Product')->result_array();
	}
	function get_list_color()
	{
		$this->db->order_by('Modify_Color','DESC');
		return $this->db->get('Color')->result_array();
	}
	function get_detail_color($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Color',$id);
			return $this->db->get('Color')->row_array();
		}
	}
	function get_detail_category($id)
	{
		if (isset($id)) {
			$this->db->where('ID_CatP',$id);
			return $this->db->get('Categorys_Product')->row_array();
		}
	}
	function add_product($param = array())
	{
		if (isset($param)) {
			$this->db->insert('Products',$param);
			return $this->db->insert_id();
		}
	}
	function update_product($id,$param = array())
	{
		if (isset($param) && isset($id)) {
			$this->db->where('ID_P',$id);
			return $this->db->update('Products',$param);
		}
	}
	function delete_product($id)
	{
		if (isset($id)) {
			$this->db->where('ID_P',$id);
			return $this->db->delete('Products');
		}
	}
	function delete_thumbnail($id)
	{
		if (isset($id)) {
			$this->db->where('Product_Tn',$id);
			return $this->db->delete('Thumbnails');
		}
	}
	function add_thumbnail($param = array())
	{
		if (isset($param)) {
			return $this->db->insert('Thumbnails',$param);
		}
	}
	function update_thumbnail($id,$param = array())
	{
		if (isset($param)) {
			$this->db->where('ID_Tn',$id);
			return $this->db->update('Thumbnails',$param);
		}
	}
	function add_category($param = array())
	{
		if (isset($param)) {
			return $this->db->insert('Categorys_Product',$param);
		}
	}
	function update_category($id,$param = array())
	{
		if (isset($id) && isset($param)) {
			$this->db->where('ID_CatP',$id);
			return $this->db->update('Categorys_Product',$param);
		}
	}
	function delete_category($id)
	{
		if (isset($id)) {
			$this->db->where('ID_CatP',$id);
			return $this->db->delete('Categorys_Product');
		}
	}
	function add_color($param = array())
	{
		if (isset($param)) {
			return $this->db->insert('Color',$param);
		}
	}
	function update_color($id,$param = array())
	{
		if (isset($id) && isset($param)) {
			$this->db->where('ID_Color',$id);
			return $this->db->update('Color',$param);
		}
	}
	function delete_color($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Color',$id);
			return $this->db->delete('Color');
		}
	}
	function get_tag($tag)
	{
		if (isset($tag)) {
			$this->db->like('Name_Tag',$tag);
			return $this->db->get('Tags')->result_array();
		}
	}
	function check_tag($tag)
	{
		if (isset($tag)) {
			$this->db->where('Name_Tag',$tag);
			return $this->db->count_all_results('Tags');
		}
	}
	function get_info_tag($tag)
	{
		if (isset($tag)) {
			$this->db->where('Name_Tag',$tag);
			return $this->db->get('Tags')->result_array();
		}
	}
	function get_id_tag($tag)
	{
		if (isset($tag)) {
			$this->db->where('ID_Tag',$tag);
			return $this->db->get('Tags')->result_array();
		}
	}
	function insert_tag($param = array())
	{
		if (isset($param)) {
			$this->db->insert('Tags',$param);
			return $this->db->insert_id();
		}
	}
	function get_thumbnails($id)
	{
		if (isset($id)) {
			$this->db->where('Product_Tn',$id);
			return $this->db->get('Thumbnails')->result_array();
		}
	}
	function check_thumbnail($link = array())
	{
		if (isset($link)) {
			$this->db->where('Thumbnail_link_Tn',$link);
			return $this->db->count_all_results('Thumbnails');
		}
	}
}