<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Ads_BE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_ads()
	{
		$this->db->order_by('Modify_Ads','DESC');
		return $this->db->get('Ads')->result_array();
	}
	function get_detail_ads($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Ads',$id);
			return $this->db->get('Ads')->row_array();
		}
	}
	function add_ads($param = array())
	{
		if (isset($param)) {
			return $this->db->insert('Ads',$param);
		}
	}
	function update_ads($id,$param = array())
	{
		if (isset($param) && isset($id)) {
			$this->db->where('ID_Ads',$id);
			return $this->db->update('Ads',$param);
		}
	}
	function delete_ads($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Ads',$id);
			return $this->db->delete('Ads');
		}
	}
	
}