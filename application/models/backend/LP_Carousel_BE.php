<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Carousel_BE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_carousel()
	{
		$this->db->order_by('Sort_Slide','ASC');
		return $this->db->get('Slider')->result_array();
	}
	function get_detail_slide($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Slide',$id);
			return $this->db->get('Slider')->row_array();
		}
	}
	
	function add_slide($param = array())
	{
		if (isset($param)) {
			return $this->db->insert('Slider',$param);
		}
	}
	function update_slide($id,$param = array())
	{
		if (isset($param) && isset($id)) {
			$this->db->where('ID_Slide',$id);
			return $this->db->update('Slider',$param);
		}
	}
	function delete_slide($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Slide',$id);
			return $this->db->delete('Slider');
		}
	}
	
}