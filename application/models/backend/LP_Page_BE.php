<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Page_BE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_page()
	{
		$this->db->order_by('ID_Page','DESC');
		return $this->db->get('Pages')->result_array();
	}
	function get_detail_page($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Page',$id);
			return $this->db->get('Pages')->row_array();
		}
	}
	function add_page($param = array())
	{
		if (isset($param)) {
			$this->db->insert('Pages',$param);
			return $this->db->insert_id();
		}
	}
	function update_page($id,$param = array())
	{
		if (isset($id) && isset($param)) {
			$this->db->where('ID_Page',$id);
			return $this->db->update('Pages',$param);
		}
	}
	function delete_page($id)
	{
		if (isset($id)) {
			$this->db->where('ID_Page',$id);
			return $this->db->delete('Pages');
		}
	}
}