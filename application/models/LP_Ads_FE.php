<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LP_Ads_FE extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_ads($position,$limit = null)
	{
		if (isset($position)) {
			$this->db->where('Public_Ads','Y');
			$this->db->where('Position_Ads',$position);
			$this->db->order_by('Sort_Ads','ASC');
			if ($limit != null) {
				$this->db->limit($limit);
			}
			return $this->db->get('Ads')->result_array();
		}
		return null;
	}
	function click_ads($id)
	{
		$query = 'UPDATE Ads SET Click_Ads = Click_Ads + 1 WHERE ID_Ads ='.$id;
		return $this->db->query($query);
	}
}