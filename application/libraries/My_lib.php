<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class My_lib
{
	
	public $dequy;
	function dequy_menu($parent_id = null, $data = null, $step = '', $type = 'dropdown')
	{
		if (isset($data) && is_array($data)) {
			foreach ($data as $key => $value) {
				if ($value['Parent_Menu'] == $parent_id) {
					if ($type == 'dropdown') {
						$this->dequy[$value['ID_Menu']] = $step.' '.$value['Name_Menu'];
					}else if($type == 'view'){
						$value['Name_Menu'] = $step.' '.$value['Name_Menu'];
						$this->dequy[$value['ID_Menu']] = $value;
					}
					$this->dequy_menu($value['ID_Menu'],$data,$step.'-- ',$type);
				}
			}
		}
		return $this->dequy;
	}
}