<?php defined('BASEPATH') OR exit('No direct script access allowed');
 /**
 * 
 */
class MY_Loader extends CI_Loader
{
 	
 	public function template($folder_name,$file_name, $vars = array(), $return = false)
 	{
 		if ($return) {
 			$content  = $this->view($folder_name.'/header', $vars, $return);
 			$content .= $this->view($folder_name.'/'.$file_name, $vars, $return);
 			$content .= $this->view($folder_name.'/footer', $vars, $return);
 			return $content;
 		}else {
 			$this->view($folder_name.'/header', $vars);
 			$this->view($folder_name.'/'.$file_name, $vars);
 			$this->view($folder_name.'/footer', $vars);
 		}
 	}
}

?>