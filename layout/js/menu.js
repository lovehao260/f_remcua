$(document).ready(function() {

    let active_sidebar = $('.site-sidebar .widget-area', '#primary'),
        site_main      = $('.site-main#main', '#primary'),
        site_content   = $('.site-content#content', '#body'),
        site_footer    = $('footer#main-footer'),
        footer_margin  = 72;

    if (active_sidebar.length > 0) {
        site_main.css('min-height',active_sidebar.outerHeight()+80);
    }

    if (site_content.length > 0) {
        site_content.css('min-height','calc(100vh - '+footer_margin+'px - '+site_footer.outerHeight()+'px)');
    }



    $('.featured_portfolio_next').on('click', function() {
        let the_list = $('.featured_portfolio_list'),
            list_pos = Number(the_list.attr('display'));

        if (list_pos<3) {
            the_list.attr('display',(list_pos+1));
            $('.featured_portfolio_pag a').removeClass('active');
            $('.featured_portfolio_pag a[data-folio="'+(list_pos+1)+'"]').addClass('active');
        }
        return false;
    });

    $('.featured_portfolio_prev').on('click', function() {
        let the_list = $('.featured_portfolio_list'),
            list_pos = Number(the_list.attr('display'));

        if (list_pos>1) {
            the_list.attr('display',(list_pos-1));
            $('.featured_portfolio_pag a').removeClass('active');
            $('.featured_portfolio_pag a[data-folio="'+(list_pos-1)+'"]').addClass('active');
        }
        return false;
    });

    $('.featured_portfolio_pag a').on('click', function() {
        let the_list   = $('.featured_portfolio_list'),
            list_pos   = Number(the_list.attr('display')),
            target_pos = $(this).attr('data-folio');

        $('.featured_portfolio_pag a').not(this).removeClass('active');
        $(this).addClass('active');
        the_list.attr('display',target_pos);
        return false;
    });
    $('.sub-menu').each(function() {
        $(this).prev('a').addClass('has-sub');
    });

    $('.main-navigation.header_menu').before('<a href="javascript:;" class="nav_toggler"><span></span></a>');

    $(document).on('click', '.nav_toggler', function() {
        $('body').toggleClass('toggle_nav');
        $('a.active-sub').removeClass('active-sub');
        return false;
    });

    $(document).on('click', 'a.has-sub:not(.active-sub)', function() {
        $('a.active-sub').removeClass('active-sub');
        if ($(window).width() <= 1024) {
            $(this).addClass('active-sub');
            return false;
        }
    });

    $('.search-field').on('focus', function() {
        $(this).parent().parent('.search_form_wrapper').addClass('input_focused');
    });

    $('.search-field').on('blur', function() {
        $(this).parent().parent('.search_form_wrapper').removeClass('input_focused');
    });

    $('.compose_search_form').on('click', function() {
        $('.form_search').show(500);
        $('.site-header').addClass('show-search');
        $('.menu-overlay').show(500);

        return false;
    });
    $('.menu-overlay').on('click', function() {
        $('.form_search').hide(500);
        $('.site-header').removeClass('show-search');

        $('.menu-overlay').toggle();
        return false;
    });
    $('.cancel_search').on('click', function() {
        $('.compose_search_form').removeClass('form_active');
        return false;
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // escape key maps to keycode `27`
            $('.compose_search_form').removeClass('form_active');
        }
    });

    $(document).on('hover', '*', function() {
        let $this = $(this);
        if ($(window).width() < 768) {
            $this.click();
        }
    });

    function height_track(tracker,tracking) {
        tracker.css('height',$(tracking).outerHeight());
    }
    function width_track(tracker,tracking) {
        tracker.css('width',$(tracking).outerWidth());
    }

    $('[height-track]').each(function() {
        let tracking = $(this).attr('height-track');
        height_track($(this),tracking);
    });

    $('[width-track]').each(function() {
        let tracking = $(this).attr('width-track');
        width_track($(this),tracking);
    });

    $(window).resize(function() {

        $('a.active-sub').removeClass('active-sub');

        $('[height-track]').each(function() {
            let tracking = $(this).attr('height-track');
            height_track($(this),tracking);
        });

        $('[width-track]').each(function() {
            let tracking = $(this).attr('width-track');
            width_track($(this),tracking);
        });

    });

    $(window).scroll(function() {
        let scrTop         = $(window).scrollTop(),
            hero_boundary  = $(window).height();

        if ($(window).width() < 1024 && $(window).width() >= 768) {
            hero_boundary = $(window).height()/2;
        }

        if (scrTop > (hero_boundary - 200)) {
            $('body.home').addClass('over_banner');
        } else {
            $('body.home').removeClass('over_banner');
        }

        if (active_sidebar.length > 0) {
            let sidebar_height = active_sidebar.outerHeight(),
                sidebar_wrap   = active_sidebar.parent('.site-sidebar'),
                wrap_offset    = sidebar_wrap.offset().top,
                wrap_boundary  = sidebar_wrap.outerHeight();
            sidebar_offset = active_sidebar.offset().top,
                alter_offset   = scrTop + wrap_offset + sidebar_wrap.outerHeight() - sidebar_wrap.height() + active_sidebar.outerHeight() - wrap_offset,
                sidebar_Pos    = sidebar_offset+sidebar_height-wrap_offset;

            if ( alter_offset >= wrap_boundary ) {
                active_sidebar.addClass('unfixed');
            } else {
                active_sidebar.removeClass('unfixed');
            }

            //active_sidebar.attr('sidebar_offset',sidebar_offset+sidebar_height-wrap_offset).attr('alter_offset',alter_offset).attr('wrap_height',sidebar_wrap.outerHeight());
        }
    });

    $('.product-category.product').each(function(i){
        let $this = $(this),
            theIMG = $this.find('img').attr('src');

        if (theIMG) {
            //$this.css('background-image','url(' + theIMG + ')');
            $(this).children('a').prepend('<span class="product-cat-thumb" style="background-image: url(' + theIMG + ')"></span>');
        } else {
            $(this).children('a').prepend('<span class="product-cat-thumb pct-placeholder"></span>');
        }

        if (i == 0) {
            $this.parent('.products').addClass('product-catview');
        }

    });


});
