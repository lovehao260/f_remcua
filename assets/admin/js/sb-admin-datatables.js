// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTable').DataTable({
  	"pageLength": 10,
    "order": [[ 0, 'desc' ]],
  	"columnDefs": [ {
	  "targets": -1,"orderable": false,
	} ],
  	"language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Vietnamese.json"
    }
  });

  $('#dataTableProduct').DataTable({
  	"pageLength": 10,
  	"order": [[ 0, 'desc' ]],
  	"columnDefs": [ {
	"targets": -1,"orderable": false,
	} ],
  	"language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Vietnamese.json"
    },
    "ajax": {
    	"url": "/admin/product/ajax_product",
    	"type": "GET"
    }
  });

  $('#dataTablePosts').DataTable({
    "pageLength": 10,
    "order": [ 4, 'desc' ],
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Vietnamese.json"
    },
    "ajax": {
      "url": "/admin/posts/ajax_post",
      "type": "GET"
    }
  });

  $('#dataTablePage').DataTable({
    "pageLength": 10,
    "order": [ 6, 'desc' ],
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Vietnamese.json"
    },
    "ajax": {
      "url": "/admin/page/ajax_page",
      "type": "GET"
    }
  });

  $('#dataTableContact').DataTable({
    "pageLength": 10,
    "order": [ 3, 'desc' ],
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Vietnamese.json"
    },
    "ajax": {
      "url": "/admin/contact/ajax_contact",
      "type": "GET"
    }
  });

  $('#dataTableSubscribe').DataTable({
    "pageLength": 10,
    "order": [ 4, 'desc' ],
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Vietnamese.json"
    },
    "ajax": {
      "url": "/admin/contact/ajax_subscribe",
      "type": "GET"
    }
  });

  $('#dataTableTag').DataTable({
    "pageLength": 10,
    "order": [ 2, 'desc' ],
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Vietnamese.json"
    },
    "ajax": {
      "url": "/admin/tags/ajax_tag",
      "type": "GET"
    }
  });
});