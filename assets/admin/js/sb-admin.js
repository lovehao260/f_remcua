(function($) {
  "use strict"; // Start of use strict
  // Configure tooltips for collapsed side navigation
  $('.navbar-sidenav [data-toggle="tooltip"]').tooltip({
    template: '<div class="tooltip navbar-sidenav-tooltip" role="tooltip" style="pointer-events: none;"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
  })
  // Toggle the side navigation
  $("#sidenavToggler").click(function(e) {
    e.preventDefault();
    $("body").toggleClass("sidenav-toggled");
    $(".navbar-sidenav .nav-link-collapse").addClass("collapsed");
    $(".navbar-sidenav .sidenav-second-level, .navbar-sidenav .sidenav-third-level").removeClass("show");
  });
  // Force the toggled class to be removed when a collapsible nav link is clicked
  $(".navbar-sidenav .nav-link-collapse").click(function(e) {
    e.preventDefault();
    $("body").removeClass("sidenav-toggled");
  });
  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .navbar-sidenav, body.fixed-nav .sidenav-toggler, body.fixed-nav .navbar-collapse').on('mousewheel DOMMouseScroll', function(e) {
    var e0 = e.originalEvent,
      delta = e0.wheelDelta || -e0.detail;
    this.scrollTop += (delta < 0 ? 1 : -1) * 30;
    e.preventDefault();
  });
  // Scroll to top button appear
  $(document).scroll(function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });
  // Configure tooltips globally
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="select"]').selectpicker();
  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    event.preventDefault();
  });


  // $("#tag_product").tagsinput({
  //   typeahead: {
  //     source: function(query) {
  //       return $.getJSON('/admin/product/tags_json');
  //     }
  //   }
  // });
  $('#keyword_product').tagsinput();
  $('#keywords_catproduct').tagsinput();
  $('#keywords_color').tagsinput();
  
  $('.price_product').priceFormat({
    prefix: '',
    centsLimit: 0
  });
  // $('#price_02_product').change(function(){
  //   $('#gia_mettoi').val($('#price_02_product').unmask());
  // });
  // $('#price_product').change(function(){
  //   $('#gia_metvuong').val($('#price_product').unmask());
  // });
  $('#code_color').colorpicker();

  $('#formProduct').validate({
    rules: {
      name_product: 'required',
      category_product: 'required'
    },
    messages: {
      name_product: 'Tên sản phẩm không bỏ trống',
      category_product: 'Vui lòng chọn danh mục sản phẩm'
    },
    errorClass: 'text-danger'
  });

})(jQuery); // End of use strict
function login()
{
  $('#formLogin').validate({
    rules: {
      tendangnhap: 'required',
      matkhau: 'required'
    },
    messages: {
      tendangnhap: 'Tên đăng nhập không bỏ trống',
      matkhau: 'Mật khẩu không bỏ trống'
    },
    errorClass: 'text-danger'
  });
}
$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {
    var so = 0;
        if (input.files) {
            var filesAmount = input.files.length;
            // var tag_img = $.parseHTML('<img class="col-3 img-fluid" id="imageAlbum'+ so++ +'">');
            var tag_open_div = $.parseHTML('<div class="col-3">');
            var tag_close_div = $.parseHTML('</div>');
            // var inputTag = $.parseHTML('<input type="hidden" name="albumThumbnail" id="albumThumbnail">');

            for (i = 0; i < filesAmount; i++) {
                var tag_img = $.parseHTML('<img class="col-3 img-fluid" id="imageAlbum'+ i +'">');
                var reader = new FileReader();

                reader.onload = function(event) {
                    $(tag_img).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    // $(inputTag).attr('value', event.target.result).appendTo(placeToInsertImagePreview);

                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#fileupload').on('change', function() {
        imagesPreview(this, '#showThumbnail');
    });
});
