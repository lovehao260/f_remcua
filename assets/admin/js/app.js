$(document).ready(function(){
    $('#dataTableColor').DataTable({
      "pageLength": 10,
      "order": [[ 3, 'desc' ]],
      "columnDefs": [ {
      "targets": -1,"orderable": false,
    } ],
      "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Vietnamese.json"
      }
    });

    /*selectpicker*/
    // $('#link_menu').selectpicker();
});
