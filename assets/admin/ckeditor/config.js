/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';      
	config.filebrowserBrowseUrl = 'http://remsaigon.vn/admin/ckfinder/html';
	config.filebrowserImageBrowseUrl = 'http://remsaigon.vn/admin/ckfinder/html?type=Images';
	config.filebrowserUploadUrl = 'http://remsaigon.vn/admin/ckfinder/connector?command=QuickUpload&type=Images';
};
