$(document).ready(function(){
    /*$('body').append('<div id="toTop" class="btn" data-tooltip="true" title="Back to Top"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
    $(window).scroll(function () {
	    if ($(this).scrollTop() != 0) {
	      $('#toTop').fadeIn();
	    } else {
	      $('#toTop').fadeOut();
	    }
	}); */

	$('#don_gia_met_vuong').priceFormat({
	    prefix: '',
	    centsLimit: 0
	});
	$('#don_gia_met_vuong').change(function(){
	    $('#gia_thuc').val($('#don_gia_met_vuong').unmask());
	});

	$('#dongia_toi').priceFormat({
	    prefix: '',
	    centsLimit: 0
	});
	$('#dongia_toi').change(function(){
	    $('#gia_thuc_toi').val($('#dongia_toi').unmask());
	});
	
  	$('#toTop').click(function(){
      	$("html, body").animate({ scrollTop: 0 }, 600);
      	return false;
  	});
  	$('.thumbnail-product-item').click(function(){
		$('#showThumbnail').attr('src',$(this).data('src'));
	});
	$('#thumbnail-product').carousel('pause');
	$('#showThumbnail').click(function(){
		$('#modal_image_thumb').attr('src',$('#showThumbnail').attr('src'));
		$('#title-p-modal').html($('#title-p').html());
		
		$('#showLagreThumb').modal('show');
	});
	$('#album_footer').click(function(){
		$('#modal_image_thumb').attr('src',$('#showThumbnail').attr('src'));
		$('#showLagreThumb').modal('show');
	});
  	$('#formContact').validate({
	  	rules: {
	  		hovaten		: 'required',
	  		dienthoai	: 'required',
	  		noidung		: 'required'
	  	},
	  	messages: {
	  		hovaten		: 'Bạn chưa nhập họ và tên',
	  		dienthoai	: 'Bạn chưa nhập số điện thoại',
	  		noidung		: 'Bạn chưa nhập nội dung'
	  	},
	  	errorClass: 'text-danger'
  	});  

  	// $('.modal-content').resizable({
   //    //alsoResize: ".modal-dialog",
   //    minHeight: 300,
   //    minWidth: 300
   //  });
    // $('.modal-dialog').draggable();

    $('#showLagreThumb').on('show.bs.modal', function() {
      $(this).find('.modal-body').css({
        'max-height': '100%'
      });
    });
  	
});

	
function add_sub()
{
	$('#formSub').validate({
		rules: {
			'email-sub': 'required'
		},
		messages: {
			'email-sub': 'Bạn chưa nhập email!'
		},
		submitHandler: function() {
			$.ajax({
				type: 'POST',
				url: '/sub/add',
				data: $('#formSub').serialize(),
				beforeSend: function(){
					$('#btn-sub').html('Đang xử lý <i class="fas fa-spinner fa-spin"></i>');
					$('#btn-sub').attr('disabled',true);
				},
				success: function(result){
					$('#btn-sub').html('Đăng ký');
					$('#btn-sub').attr('disabled',false);
					$('#result').html(result);
				}
			});
		}
	});
}
function tinh_met_vuong()
{
	var chieucao = $('#chieucao').val();
	var chieurong = $('#chieurong').val();
	var gia = $('#don_gia_met_vuong').val();
	if (chieurong.length <= 0){
		// alert('Bạn chưa nhập chiều rộng');
		$('#ketquaTinhMetVuong').html('<label class="error text-danger">Bạn chưa nhập chiều rộng</label>');
	}else if(chieucao.length <= 0){
		// alert('Bạn chưa nhập chiều cao');
		$('#ketquaTinhMetVuong').html('<label class="error text-danger">Bạn chưa nhập chiều cao</label>');
	}else if(gia.length <= 0){
		alert('Bạn chưa nhập giá');
	}else {
		/*Hàm ajax*/
		$.ajax({
			type: 'POST',
			url: '/home/tinhgia/metvuong',
			data: $('#formMetVuong').serialize(),
			success: function(result){
				$('#ketquaTinhMetVuong').html('<h3>Kết quả: '+result+'</h3>');
			}
		});
	}
}
function tinh_met_toi()
{
	var chieurong = $('#chieurong_toi').val();
	var gia = $('#dongia_toi').val();
	if (chieurong.length <= 0){
		// alert('Bạn chưa nhập chiều rộng');
		$('#ketquaTinhMetToi').html('<label class="error text-danger">Bạn chưa nhập chiều rộng</label>');
	}else if(gia.length <= 0){
		$('#ketquaTinhMetToi').html('<label class="error text-danger">Bạn chưa nhập giá</label>');
	}else {
		/*Hàm ajax*/
		$.ajax({
			type: 'POST',
			url: '/home/tinhgia/mettoi',
			data: $('#formMetToi').serialize(),
			success: function(result){
				$('#ketquaTinhMetToi').html('<h3>Kết quả: '+result+'</h3>');
			}
		});
	}
}
function click_ads(id)
{
	$.ajax({
		type: 'POST',
		url: '/home/click_ads/'+id,
		cache: false
	});
}